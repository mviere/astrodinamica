"""
\authors María Eugenia Viere

\brief Simulador de modelos de la astrodinámica

\version 0

\date 02/05/2022
"""
# %%
import numpy as np
from datetime import datetime, timedelta

def ae2pratios(a, e, theta = None, h = None, mu = 398600.441, in_angle = 'Rad'):
    '''
    Semi-major axis and eccentricity to ratio, apoapsis ratio, periapsis ratio and semilatus rectum

    Args:
        a (float): semi-major axis [km]
        
        e (float): eccentricity [-]

        theta (float): true anomaly [rad] (add for eliptical and hyperbolic case)

        h (float): angular momentum [km^2/s] (add for parabolic case)

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of theta, 'Rad' or 'Deg'
    
    Outputs:
        r (float): ratio [km]

        r_periapsis (float): periapsis ratio [km]

        r_apoapsis (float): apoapsis ratio [km]

        p (float): semilatus rectum [m]
    '''
    if in_angle != 'Rad' and theta != None:
        theta = np.radians(theta)
    if theta == None:                           # circular / parabolic
        r_apoapsis = a
    else:                                       # eliptical / hyperbolic
        r_apoapsis = a*(1+e)
    if theta == None and h == None:              # circular
        p, r, r_periapsis = a
    if theta == None and h != None:             # parabolic
        p = -h**2/mu
        r = (p/2)*(1+(mu*e)**2)
        r_periapsis = p/2
    else:                                       # eliptical / hyperbolic
        p = a*(1-e**2)
        r = p/(1+e*np.cos(theta))
        r_periapsis = a*(1-e)
    return r, r_apoapsis, r_periapsis, p

def raEN2v(r, a = None, EN = None, mu = 398600.441):
    '''
    Ratio and energy to velocity tangential to orbit

    Args:
        r (float): ratio [km]

        a (float / string): semi-major axis [km] (add for eliptical and hyperbolic case instead of EN) or 'Inf' (add for parabolas)

        EN (float): orbit energy [km^2/s^2] (add for eliptical and hyperbolic case instead of a)
        
        mu (float): central body gravitational parameter [km^3/s^2]
            
    Outputs:
        v (float): velocity [km/s]
    '''
    if a == None and EN == None:            # circular
        v = np.sqrt(mu/r)
    if type(a) == str:                      # parabolic
        v = np.sqrt(2*mu/r)
    if a != None:                           # eliptical / hyperbolic
        v = np.sqrt((2*mu/r)-(mu/a))
    elif EN != None:                        # eliptical / hyperbolic
        v = np.sqrt((2*mu/r)-2*EN)
    return v

def ae2ENh(a, e, r_periapsis = None, mu = 398600.441):
    '''
    Semi-major axis and eccentricity to energy and angular momentum

    Args:
        a (float / string): semi-major axis [km]
        
        e (float): eccentricity [-]
        
        mu (float): central body gravitational parameter [km^3/s^2]
        
        r_periapsis (float): radius of periapsis [km] (add for parabolic case)
    
    Outputs:
        EN (float): orbit energy [km^2/s^2]
        
        h (float): angular momentum [km^2/s]
    '''
    if type(a) == float or type(a) == int:  # circular / elliptical / hyperbolic
        EN = -mu/(2*a)
        p = a*(1-e**2)
    else:                                   # parabolic case
        EN = 0
        e = 1
        p = 2*r_periapsis
    h = np.sqrt(p*mu)
    return EN, h

def Tap2n(T = None, a = None, p = None, mu = 398600.441):
    '''
    Period or semi-major or semilatus rectum to mean motion

    Args:
        T (float): period [s] (add for circular, eliptical and hyperbolic case instead of a)

        a (float): semi-major axis [km] (add for circular, eliptical and hyperbolic case instead of T)

        p (float): semilatus rectum [km] (add for parabolic case)
        
        mu (float): central body gravitational parameter [km^3/s^2]
    
    Outputs:
        n (float): mean motion [rad/s]
    '''
    if p == None:                                   # circular / eliptical / hyperbolic
        if a != None:
            n = np.sqrt(mu/(a**3))
        elif T != None:
            n = 2*np.pi/T
        return n
    else:                                           # parabolic
        n = 2*np.sqrt(mu/(p**3))
        return n

def ENh2ae(EN, h, r_periapsis = None, mu = 3.986e14):
    '''
    Energy and angular momentum to semi-major axis and eccentricity

    Args:
        EN (float): orbit energy [km^2/s^2]
        
        h (float): angular momentum [km^2/s]

        r_periapsis (float): radius of periapsis [km] (add for parabolic case)
        
        mu (float): central body gravitational parameter [km^3/s^2]
    
    Outputs:
        a (float / string): semi-major axis [km] or 'Inf' for parabolas
        
        e (float): eccentricity [-]
    '''
    if EN == 0:                              # parabolic case
        a = 'Inf'
        p = 2*r_periapsis
        e = 1
    else:                                   # circular / elliptical / hyperbolic
        a = -mu/(2*EN)
        p = h**2/mu
        e = np.sqrt(1-(p/a))
    return a, e

def KepEqtnE(M, e, abstol = 1e-8, itlim = 1e8, in_angle = 'Rad', out_angle = 'Rad'):
    '''
    Kepler equation to convert mean anomaly (M) and eccentricity (e) to eccentric anomaly (E).
    
    Args:
        M (float): mean anomaly [rad]
        
        e (float): eccentricity [-]

        abstol (float): maximum tolerance for E value [rad]
        
        itlim (int): maximum iterations

        in_angle (string): angle unit of M and abstol, 'Rad' or 'Deg'

        out_angle (string): angle unit of E, 'Rad' or 'Deg'
    
    Outputs:
        E (float): eccentric anomaly [deg]
        
        convergence (int): 1 if calculation converged within itlim else 0
    '''
    if in_angle != 'Rad':
        M = np.radians(M)
        if abstol != 1e-8:
            abstol = np.radians(abstol)
    E = M - e if np.sin(M) < 0 else M + e
    E_ = E + 2*abstol
    i = 0
    convergence = 1
    while abs(E-E_) > abstol and i < itlim:
        E_ = E; i += 1
        if i >= itlim:
            print('Calulation stopped, iteration limit reached')
            convergence = 0
            break
        E = E_ + (M - E_ + e*np.sin(E_)) / (1 - e*np.cos(E_))
    if out_angle != 'Rad':
        E = np.degrees(E)
    return E, convergence

def KepEqtnP(dt, p, mu = 398600.441, out_angle = 'Rad'):
    '''
    Kepler equation to convert time of flight since periapsis (dt) to parabolic anomaly (B).
    
    Args:
        dt (float): time of flight since periapsis [s]
        
        p (float): semilatus rectum [km]

        mu (float): central body gravitational parameter [km^3/s^2]

        out_angle (string): angle unit of B, 'Rad' or 'Deg'
    
    Outputs:
        B (float): parabolic anomaly [rad]
    '''
    s = (np.arctan(1/(3*(np.sqrt(mu/(p**3)))*dt)))/2
    w = np.arctan(np.cbrt(np.tan(s)))
    B = 2*(1/np.tan(2*w))
    if out_angle != 'Rad':
        B = np.degrees(B)
    return B

def KepEqtnH(M, e, abstol = 1e-8, itlim = 1e8, in_angle = 'Rad', out_angle = 'Rad'):
    '''
    Kepler equation to convert mean anomaly (M) and eccentricity (e) to hyperbolic anomaly (H).
    
    Args:
        M (float): mean anomaly [rad] 
        
        e (float): eccentricity [-]

        abstol (float): maximum tolerance for H value [rad]
        
        itlim (int): maximum iterations

        in_angle (string): angle unit of M and abstol, 'Rad' or 'Deg'

        out_angle (string): angle unit of H, 'Rad' or 'Deg'
    
    Outputs:
        H (float): hyperbolic anomaly [rad]
        
        convergence (int): 1 if calculation converged within itlim else 0
    '''
    if in_angle != 'Rad':
        M = np.radians(M)
        if abstol != 1e-8:
            abstol = np.radians(abstol)
    if abstol != 1e-8:
        abstol = abstol*np.pi/180
    if e < 1.6:
        if -np.pi<M<0 or M>np.pi:
            H = M - e
        else:
            H = M + e
    else:
        if e<3.6 and np.absolute(M)>np.pi:
            H = M - np.sign(M)*e
        else:
            H = M/(e-1)
    H_ = H + 2*abstol
    i = 0
    convergence = 1
    while abs(H-H_) > abstol and i < itlim:
        H_ = H; i += 1
        if i >= itlim:
            print('Calulation stopped, iteration limit reached')
            convergence = 0
            break
        H = H_ + (M + H_ - e*np.sinh(H_)) / (e*np.cosh(H_)-1)
    if out_angle != 'Rad':
        H = np.degrees(H)
    return H, convergence

def theta2EBH(theta, e, in_angle = 'Rad', out_angle = 'Rad'):
    '''
    True anomaly (theta) to eccentric anomaly (E) or parabolic anomaly (B) or hyperbolic anomaly (H).
    
    Args:
        theta (float): true anomaly [rad]

        e (float): eccentricity [-]

        in_angle (string): angle unit of theta, 'Rad' or 'Deg'

        out_angle (string): angle unit of E, B or H, 'Rad' or 'Deg'

    Outputs:
        E (float): eccentric anomaly [rad]

        B (float): parabolic anomaly [rad]

        H (float): hyperbolic anomaly [rad]
    '''
    if in_angle != 'Rad':
        theta = np.radians(theta)
    if e < 1.0:
        sin_E = (np.sin(theta)*np.sqrt(1-e**2))/(1+e*np.cos(theta))
        cos_E = (np.cos(theta)+e)/(1+e*np.cos(theta))
        E = np.arctan2(sin_E, cos_E)
        if E < 0:
            E += 2*np.pi # nooooos ee
        if out_angle != 'Rad':
            E = np.degrees(E)
        return E
    if e == 1:
        B = np.tan(theta/2)
        if out_angle != 'Rad':
            B = np.degrees(B)
        return B
    if e > 1.0:
        sinh_H = (np.sin(theta)*np.sqrt(e**2-1))/(1+e*np.cos(theta))    
        cosh_H = (e+np.cos(theta))/(1+e*np.cos(theta))
        H = np.arctanh(sinh_H/cosh_H)
        if out_angle != 'Rad':
            H = np.degrees(H)
        return H

def EBH2theta(e, E = None, B = None, H = None, p = None, r = None, in_angle = 'Rad', out_angle = 'Rad'):
    '''
    Eccentric anomaly or parabolic anomaly or hyperbolic anomaly to true anomaly
    
    Args:
        e (float): eccentricity [-]

        E (float): eccentric anomaly [rad]

        B (float): parabolic anomaly [rad]

        H (float): hyperbolic anomaly [rad]

        p (float): semilatus rectum [km] (add for parabolic case)

        r (float): ratio [km] (add for parabolic case)

        in_angle (string): angle unit of E, B or H, 'Rad' or 'Deg'

        out_angle (string): angle unit of theta, 'Rad' or 'Deg'

    Outputs:
        theta (float): true anomaly [rad]
    '''
    if e < 1.0:
        if in_angle != 'Rad':
            E = np.radians(E)
        sin_theta = (np.sin(E)*np.sqrt(1-e**2))/(1-e*np.cos(E))
        cos_theta = (np.cos(E)-e)/(1-e*np.cos(E))
        theta = np.arctan2(sin_theta, cos_theta)
        if theta < 0:
            theta += 2*np.pi # no seeee
    if e == 1:
        if in_angle != 'Rad':
            B = np.radians(B)
        sin_theta = (p*B/r)
        cos_theta = ((p-r)/r)
        theta = np.arctan2(sin_theta, cos_theta)
    if e > 1.0:
        if in_angle != 'Rad':
            H = np.radians(H)
        sin_theta = (-np.sinh(H)*np.sqrt(e**2-1))/(1-e*np.cosh(H))
        cos_theta = (np.cosh(H)-e)/(1-e*np.cosh(H))
        theta = np.arctan2(sin_theta, cos_theta)
    if out_angle != 'Rad':
        theta = np.degrees(theta)
    return theta

def M2theta(M, e, p = None, r = None, abstol = 1e-8, itlim = 1e8, in_angle = 'Rad', out_angle = 'Rad'):
    '''
    Mean anomaly (M) to true anomaly (theta).
    
    Args:
        M (float): mean anomaly [rad]

        e (float): eccentricity [-]

        p (float): semilatus rectum [km] (add for parabolic case)

        r (float): ratio [km] (add for parabolic case)

        abstol (float): maximum tolerance for E or H value [rad]
        
        itlim (int): maximum iterations

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of M, 'Rad' or 'Deg'

        out_angle (string): angle unit of theta, 'Rad' or 'Deg'

    Outputs:
        theta (float): true anomaly [rad]
    '''
    if in_angle != 'Rad':
        M = np.radians(M)
    if e < 1.0:
        E, convergence = KepEqtnE(M, e, abstol, itlim)
        theta = EBH2theta(e, E)
        if theta < 0:
           theta += 2*np.pi
    if e == 1.0:
        s = (np.pi/2 - np.arctan(3*M/2))/2
        w = np.arctan(np.cbrt(np.tan(s)))
        B_ = 2*(1/np.tan(2*w))
        p_ = p
        r_ = r
        theta = EBH2theta(e, B = B_, p = p_, r = r_)
    if e > 1.0:
        H_, convergence = KepEqtnH(M, e, abstol, itlim)
        theta = EBH2theta(e, H = H_)
    if out_angle != 'Rad':
        theta = np.degrees(theta)
    return theta

def theta2M(theta, e, in_angle = 'Rad', out_angle = 'Rad'):
    '''
    True anomaly (theta) to mean anomaly (M).
    
    Args:
        theta (float): true anomaly [rad]

        e (float): eccentricity [-]

        in_angle (string): angle unit of theta, 'Rad' or 'Deg'

        out_angle (string): angle unit of M, 'Rad' or 'Deg'

    Outputs:
        M (float): mean anomaly [rad]
    '''
    if in_angle != 'Rad':
        theta = np.radians(theta)
    if e < 1.0:
        sin_E = (np.sin(theta)*np.sqrt(1-e**2))/(1+e*np.cos(theta))
        cos_E = (np.cos(theta)+e)/(1+e*np.cos(theta))
        E = np.arctan2(sin_E, cos_E)
        if E < 0:
            E += 2*np.pi
        M = E - e*np.sin(E)
    if e == 1:
        B = np.tan(theta/2)
        M = B + (B**3)/3
    if e > 1.0:
        H = np.arcsinh((np.sin(theta)*np.sqrt(e**2-1))/(1+e*np.cos(theta)))
        M = e*np.sinh(H) - H
    if out_angle != 'Rad':
        M = np.degrees(M)
    return M

def findTOF(thetai, thetaf, e, a, p = None, mu = 398600.441, in_angle = 'Rad'):
    '''
    Time of flight (TOF) between two true anomalies (thetai, thetaf). 
    
    Args:
        thetai (float): initial true anomaly [rad]

        thetaf (float): final true anomaly [rad]

        e (float): eccentricity [-]

        a (float): semi-major axis [km]

        p (float): semilatus rectum [km] (add for parabolic case)

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of thetai and thetaf, 'Rad' or 'Deg'

    Outputs:
        TOF (float): time of flight [s]
    '''
    if in_angle != 'Rad':
        thetai = np.radians(thetai)
        thetaf = np.radians(thetaf)
    Ei = theta2EBH(thetai, e)
    Ef = theta2EBH(thetaf, e)
    if e < 1.0:
        n = np.sqrt(mu/a**3)
        Mi = Ei - e*np.sin(Ei)
        Mf = Ef - e*np.sin(Ef)
    if e == 1.0:
        n = 2*np.sqrt(mu/p**3)
        Mi = Ei + (Ei**3)/3
        Mf = Ef + (Ef**3)/3
    if e > 1.0:
        n = np.sqrt(mu/a**3)
        Mi = e*np.sinh(Ei) - Ei
        Mf = e*np.sinh(Ef) - Ef
    TOF = abs(Mf-Mi)/n
    return TOF

def thetaiTOF2thetaf(thetai, TOF, e, a, p = None, r = None, mu = 398600.441, abstol = 1e-8, itlim = 1e8, in_angle = 'Rad', out_angle = 'Rad'):
    '''
    Initial true anomaly (thetai) and time of flight (TOF) to final true anomaly (thetaf).
    
    Args:
        thetai (float): initial true anomaly [rad]

        TOF (float): time of flight [s]

        e (float): eccentricity [-]

        a (float): semi-major axis [m] or 'Inf' for parabolas

        mu (float): central body gravitational parameter [m^3/s^2]

    Outputs:
        thetaf (float): final true anomaly [rad]
    '''
    if in_angle != 'Rad':
        thetai = np.radians(thetai)
    Mi = theta2M(thetai, e)
    if e != 1:
        Mf = np.sqrt(mu/a**3)*TOF + Mi
        abstol_ = abstol
        itlim_ = itlim
        thetaf = M2theta(Mf, e, abstol = abstol_, itlim = itlim_)
    if e == 1:
        Mf = 2*np.sqrt(mu/p**3)*TOF + Mi
        p_ = p
        r_ = r
        thetaf = M2theta(Mf, e, p = p_, r = r_)
    if out_angle != 'Rad':
        thetaf = np.degrees(thetaf)
    return thetaf

def ROT1(a, positive = True): # por default crece positivamente
    if positive:
        ROT1 = np.array([[1, 0, 0], [0, np.cos(a), np.sin(a)], [0, -np.sin(a), np.cos(a)]])
    else:
        ROT1 = np.array([[1, 0, 0], [0, np.cos(a), -np.sin(a)], [0, np.sin(a), np.cos(a)]])
    return ROT1

def ROT2(a, positive = True): # por default crece positivamente
    if positive:
        ROT2 = np.array([[np.cos(a), 0, -np.sin(a)], [0, 1, 0], [np.sin(a), 0, np.cos(a)]])
    else:
        ROT2 = np.array([[np.cos(a), 0, np.sin(a)], [0, 1, 0], [-np.sin(a), 0, np.cos(a)]])
    return ROT2

def ROT3(a, positive = True): # por default crece positivamente
    if positive:
        ROT3 = np.array([[np.cos(a), np.sin(a), 0], [-np.sin(a), np.cos(a), 0], [0, 0, 1]])
    else:
        ROT3 = np.array([[np.cos(a), -np.sin(a), 0], [np.sin(a), np.cos(a), 0], [0, 0, 1]])
    return ROT3

def COE2RV(p, e, i, raan = None, w = None, theta = None, u = None, lt = None, wt = None, mu = 398600.441, in_angle = 'Deg'):
    '''
    Some orbital elements (e, i, raan, w, theta, u, lt, wt) and semilatus rectum (p)
    to position vector (r_ECI) and velocity vector (v_ECI), both in ECI.
    
    Args:
        p (float): semilatus rectum [km]

        e (float): eccentricity [-]

        i (float): inclination [deg]

        raan (float): right ascension of ascending node [deg]

        w (float): argument of perigee [deg]

        theta (float): true anomaly [deg]

        u (float): argument of latitud [deg]

        lt (float): true longitud [deg]

        wt (float): true longitud of periapsis [deg]

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of i, raan, w, theta, u, lt and wt, 'Rad' or 'Deg'

    Outputs:
        r_ECI (vector): position vector [km]

        v_ECI (vector): velocity vector [km/s]
    '''
    if in_angle == 'Deg':
        i = np.radians(i)
        if raan:
            raan = np.radians(raan)
        if w:
            w = np.radians(w)
        if theta:
            theta = np.radians(theta)
        if u:
            u = np.radians(u)
        if lt:
            lt = np.radians(lt)
        if wt:
            wt = np.radians(wt)
    if i == 0.0 and e == 0.0:
        raan = 0.0
        w = 0.0
        theta = lt
    if i != 0.0 and e == 0.0:
        w = 0.0
        theta = u
    if i == 0.0 and e < 1:
        raan = 0.0
        w = wt

    r_PQW_i = (p*np.cos(theta))/(1+e*np.cos(theta))
    r_PQW_j = (p*np.sin(theta))/(1+e*np.cos(theta))
    r_PQW = np.array([r_PQW_i, r_PQW_j, 0])

    v_PQW_i = -np.sqrt(mu/p)*np.sin(theta)
    v_PQW_j = np.sqrt(mu/p)*(e+np.cos(theta))
    v_PQW = np.array([v_PQW_i, v_PQW_j, 0])
    
    PQW2ECI = np.dot(np.dot(ROT3(raan, False), ROT1(i, False)),ROT3(w, False))
    r_ECI = np.dot(PQW2ECI, r_PQW)
    v_ECI = np.dot(PQW2ECI, v_PQW)

    return r_ECI, v_ECI

def RV2COE(r_ECI, v_ECI, mu = 398600.441, out_angle = 'Deg'):
    '''
    Position vector (r_ECI) and velocity vector (v_ECI), both in ECI,
    to orbital elements (a, e, i, raan, w, theta, u, lt, wt) and semilatus rectum (p).

    Args:
        r_ECI (vector): position vector [km]

        v_ECI (vector): velocity vector [km/s]

        mu (float): central body gravitational parameter [km^3/s^2]

        out_angle (string): angle unit of i, raan, w, theta, u, lt and wt, 'Rad' or 'Deg'

    Outputs:
        p (float): semilatus rectum [km]

        a (float): semi-major axis [m] or 'Inf' for parabolas

        e (float): eccentricity [-]

        i (float): inclination [deg]

        raan (float): right ascension of ascending node [deg]

        w (float): argument of perigee [deg]

        theta (float): true anomaly [deg]

        u (float): argument of latitud [deg]

        lt (float): true longitud [deg]

        wt (float): true longitud of periapsis [deg]
    '''
    r = np.linalg.norm(r_ECI)
    v = np.linalg.norm(v_ECI)

    h_vector = np.cross(r_ECI, v_ECI)
    h = np.linalg.norm(h_vector)
    
    n_vector = np.cross([0,0,1], h_vector)
    n = np.linalg.norm(n_vector)
    
    e_vector =(np.multiply((v**2-mu/r),r_ECI)-np.multiply(np.dot(r_ECI, v_ECI),v_ECI))/mu
    e = np.linalg.norm(e_vector)
    
    EN = v**2/2 - mu/r

    if e != 1.0:
        a = -mu/(2*EN)
        p = a*(1-e**2)
    else:
        p = h**2/mu
        a = 'Inf'
    
    i = np.arccos(h_vector[2]/h)
    raan = np.arccos(n_vector[0]/n)
    
    if n_vector[1] < 0:
        raan = 2*np.pi - raan
    
    w = np.arccos(np.dot(n_vector, e_vector)/(n*e))
    if e_vector[2] < 0:
        w = 2*np.pi - w
    
    theta = np.arccos(np.dot(e_vector, r_ECI)/(e*r))
    if np.dot(r_ECI, v_ECI) < 0:
        theta = 2*np.pi - theta
    
    if i == 0 and e < 1.0:
        wt = np.arccos(e_vector[0]/e)
        if e_vector[1] < 0:
            wt = 2*np.pi - wt
    else:
        wt = None
    
    if i != 0 and e == 0:
        u = np.arccos(np.dot(n_vector, r_ECI)/(n*r))
        if r_ECI[2] < 0:
            u = 2*np.pi - u
    else:
        u = None
    
    if i == 0 and e == 0:
        lt = np.arccos(r_ECI[0]/r)
        if r_ECI[1] < 0:
            lt = 2*np.pi - lt
    else:
        lt = None
    
    if out_angle == 'Deg':
        i = np.degrees(i)
        raan = np.degrees(raan)
        w = np.degrees(w)
        theta = np.degrees(theta)
        if u != None:    
            u = np.degrees(u)
        if lt != None:
            lt = np.degrees(lt)
        if wt != None:
            wt = np.degrees(wt)

    return p, a, e, i, raan, w, theta, u, lt, wt

def propagate(r_ECI_0, v_ECI_0, dt, mu = 398600.441):

    p, a, e, i, raan, w, theta, u, lt, wt = RV2COE(r_ECI_0, v_ECI_0, mu, out_angle = 'Rad')
    
    if e < 1.0:
        n = np.sqrt(mu/a**3)
        Ei = theta2EBH(theta, e)                                               
        Mi = Ei - e*np.sin(Ei)
        Mf = Mi + n*dt
        Ef, convergence = KepEqtnE(Mf, e)
        theta = EBH2theta(e, Ef)
    
    if e == 1.0:
        n = np.sqrt(mu/p**3)
        Bi = theta2EBH(theta, e)
        r = np.linalg.norm(r_ECI_0)                                           
        Mi = Bi + Bi**3/3
        Bf = KepEqtnP(dt, p, mu)
        theta = EBH2theta(e, 0, Bf, 0, p, r)

    if e > 1.0:
        n = np.sqrt(mu/a**3)
        Hi = theta2EBH(theta, e)                                             
        Mi = e*np.sinh(Hi) - Hi
        Mf = Mi + n*dt
        Hf, convergence = KepEqtnH(Mf, e)
        theta = EBH2theta(e, 0, 0, Hf)

    r_ECI, v_ECI = COE2RV(p, e, i, raan, w, theta, u, lt, wt, mu, in_angle = 'Rad')

    return r_ECI, v_ECI

def simular(r_ECCI_0, v_ECCI_0, t, mu = 398600.441, coe = True):
    
    N = len(t)
    X = np.zeros([6,N])
    Y = np.zeros([9,N])
    
    X[:,0] = np.concatenate([r_ECCI_0, v_ECCI_0])
    
    p, a, e, i, raan, w, theta, u, lt, wt = RV2COE(r_ECCI_0, v_ECCI_0, mu)
    Y[:,0] = np.array([a, e, i, raan, w, theta, u, lt, wt])
    
    for c in range(1, N):
        r_ECCI, v_ECCI =  propagate(r_ECCI_0, v_ECCI_0, t[c])
        X[:,c] = np.concatenate([r_ECCI, v_ECCI])
        p, a, e, i, raan, w, theta, u, lt, wt = RV2COE(r_ECCI, v_ECCI, mu)
        Y[:,c] = np.array([a, e, i, raan, w, theta, u, lt, wt])
    
    if coe:
        return Y
    else:
        return X

def geoc2geod(phi_gc, e = 0.081819221456):
    phi_gc = np.radians(phi_gc)
    phi_gd = np.arctan((np.tan(phi_gc))/(1-e**2))
    phi_gd = np.degrees(phi_gd)
    return phi_gd

def geod2geoc(phi_gd, e = 0.081819221456):
    phi_gd = np.radians(phi_gd)
    phi_gc = np.arctan((1-e**2)*np.tan(phi_gd))
    phi_gc = np.degrees(phi_gc)
    return phi_gc

def ECEF2LLH(r_ECEF, R = 6378.1363, e = 0.081819221456, abstol = 1e-8, itlim = 1e8, out_angle = 'Rad'):
    '''
    Transformación de ECEF a LLH (latitud geodesica, longitud y altura sobre el esferoide).

    Args:
        r_ECEF (vector): position vector [km]

        R (float): radius of central body [km]

        e (float): eccentricity [-]

        abstol (float): maximum tolerance for E value [rad]
        
        itlim (int): maximum iterations

        out_angle (string): angle unit of phi_gd and lamb, 'Rad' or 'Deg'
    
    Outputs:
        phi_gd (float): geodetic latitud [rad]
        
        lamb (float): longitud [rad]
        
        h_ellp (float): ellipsoidal height [km] 
        
        convergence (int): 1 if calculation converged within itlim else 0
    '''
    r_delsat = np.sqrt((r_ECEF[0]**2)+(r_ECEF[1]**2))
    sin_alf = r_ECEF[1]/r_delsat
    cos_alf = r_ECEF[0]/r_delsat
    lamb = np.arctan2(sin_alf, cos_alf)
    phi_gd = np.arctan(r_ECEF[2]/r_delsat)
    r_delta = r_delsat
    phi_gd_ = phi_gd + 2*abstol
    i = 0
    convergence = 1
    while abs(phi_gd-phi_gd_) > abstol and i < itlim:
        phi_gd_ = phi_gd
        i += 1
        if i >= itlim:
            print('Calulation stopped, iteration limit reached')
            convergence = 0
            break
        C = R/np.sqrt(1-(e**2)*(np.sin(phi_gd_)**2))
        phi_gd = np.arctan((r_ECEF[2]+C*(e**2)*np.sin(phi_gd_))/r_delta)

    if np.abs(np.degrees(phi_gd)) < 89:                                 # Fuera de los polos
        C = R/(np.sqrt(1-(e**2)*(np.sin(phi_gd_)**2)))
        h_ellp = r_delta/np.cos(phi_gd)-C
    else:                                                               # En los polos
        S = R*(1-e**2)/np.sqrt(1-(e*np.sin(phi_gd))**2)
        h_ellp = r_ECEF[2]/np.sin(phi_gd)-S
    if out_angle == 'Deg':
        lamb = np.degrees(lamb)
        phi_gd = np.degrees(phi_gd)
    return phi_gd, lamb, h_ellp, convergence

def LLH2ECEF(phi_gd, lamb, h_ellp, R = 6378.1363, e = 0.081819221456, in_angle = 'Rad'):
    '''
    Transformación de LLH (latitud geodesica, longitud y altura sobre el esferoide) a ECEF.

    Args:
        phi_gd (float): geodetic latitud [rad]
        
        lamb (float): longitud [rad]
        
        h_ellp (float): ellipsoidal height [km]

        R (float): radius of central body [km]

        e (float): eccentricity of central body [-]

        in_angle (string): angle unit of phi_gd and lamb, 'Rad' or 'Deg'

    Outputs:
        r_ECEF (vector): position vector [km]
    '''
    if in_angle != 'Rad':
        lamb = np.radians(lamb)
        phi_gd = np.radians(phi_gd)
    if np.abs(np.degrees(phi_gd)) < 89:
        C = R/(np.sqrt(1-(e**2)*(np.sin(phi_gd)**2)))
        r_delsat= (h_ellp+C)*np.cos(phi_gd)
        r_ECEF_k = np.tan(phi_gd)*r_delsat-C*(e**2)*np.sin(phi_gd)
    else:
        S = R*(1-e**2)/np.sqrt(1-(e*np.sin(phi_gd))**2)
        r_ECEF_k = (h_ellp+S)*np.sin(phi_gd)
        r_delsat = (r_ECEF_k+C*(e**2)*np.sin(phi_gd))/np.tan(phi_gd)
    r_ECEF_i = np.cos(lamb)*r_delsat
    r_ECEF_j = np.sin(lamb)*r_delsat
    r_ECEF = np.array([r_ECEF_i, r_ECEF_j, r_ECEF_k])
    return r_ECEF

def time2seconds(h, min, s):

    UTs = h*3600 + min*60 + s

    return UTs

def seconds2time(UTs):

    i = UTs / 3600.0
    h  = np.floor(i)
    min = np.floor((i - h)*60)
    s = (i - h - min/60 )*3600
    
    return h, min, s

def DMS2Rad(deg, amin, aseg):
    '''
    Devuelve radianes ingresando los grados, arcominutos y arcosegundos.

    Args:
        deg (int): degrees [deg] 0 < deg < 359

        amin (int): arcminutes [deg] 0 < amin < 59

        asec (float): arcseconds [deg] 0 < asec < 59.9
        
    Outputs:
        rad (float): radians [rad]
    '''
    rad = np.radians(deg+amin/60+aseg/3600)
    return rad

def HMS2Rad(h, min, s):
    '''
    Devuelve radianes ingresando horas, minutos y segundos.

    Args:
        h (int): hours [h] 0 < h < 23

        min (int): minutes [min] 0 < min < 59

        s (float): seconds [s] 0 < s < 59.9
        
    Outputs:
        rad (float): radians [rad]
    '''
    rad = np.radians(15*(h+min/60+s/3600))
    return rad

def JulianDate(yr, mo, d, h, min, s):
    '''
    Transformación de fecha y tiempo Gregoriano a fecha Juliana (basada en UT1).

    Args:
        yr (int): years

        mo (int): months

        d (int): days

        h (int): hours

        min (int): minutes

        s (float): seconds
    
    Outputs:
        JD (float): Julian date
    '''
    JD = 367*yr-int(7*(yr+int(((mo+9)/12)))/4)+int(275*mo/9)+d+1721013.5+(((s/60+min)/60)+h)/24
    return JD

def ConvTime(yr, mo, d, h, min, s, DAT):
    '''
    Tiempo UTC en fecha Juliana (J2000).

    Args:
        yr (int): year

        mo (int): month

        d (int): day

        h (int): hours

        min (int): minutes

        s (float): seconds

        DAT (float): TAI - UTC [s]

    Outputs:
        TTT (float): Julian date in centuries (J2000)
    '''
    UTC = datetime(yr, mo, d, h, min, s)
    DAT = timedelta(seconds = DAT)
    TAI = UTC + DAT
    TT = TAI + timedelta(seconds = 32.184)
    JDTT = JulianDate(TT.year, TT.month, TT.day, TT.hour, TT.minute, TT.second)
    TTT = (JDTT-2451545)/36525
    return TTT

def thetaGMST(yr, mo, d, h, min, s, DUT1, out_angle = 'Deg'):
    '''
    Devuelve el thetaGMST (Greenwich Mean Sidereal Time) dado una fecha y tiempo UTC. 

    Args:
        yr (int): year

        mo (int): month

        d (int): day

        h (int): hours

        min (int): minutes

        s (float): seconds

        DUT1 (float): UT1 - UTC [s]

        out_angle (string): angle unit of thetaGMST, 'Rad' or 'Deg'

    Outputs:
        thetaGMST (float): GMST angle [deg]
    '''
    UTC = datetime(yr, mo, d, h, min, s)
    DUT1 = timedelta(seconds = DUT1)
    UT1 = UTC + DUT1
    UT1_seconds = timedelta(hours = UT1.hour, minutes = UT1.minute, seconds = UT1.second).total_seconds()

    JDUT1 = JulianDate(UT1.year, UT1.month, UT1.day, UT1.hour, UT1.minute, UT1.second)
    TUT1 = (JDUT1 - 2451545)/36525
    
    thetaGMST = 100.4606184 + 36000.77005361*TUT1 + 0.00038793*(TUT1**2) - (2.6e-8)*(TUT1**3)
    w_terrestre = (1.002737909350795*360)/(24*3600)
    thetaGMST = thetaGMST + w_terrestre*UT1_seconds
    # Cantidad de grados totales dividido 360, me da un resto, eso es el thetaGMST
    thetaGMST = np.mod(thetaGMST, 360)
    
    if out_angle != 'Deg':
        thetaGMST = np.radians(thetaGMST)
    
    return thetaGMST

def r_RSW2r_ECI(r_objeto_RSW, a, e, i, raan, w, theta):
    '''
    Toma la posición del objeto en RSW (RSW tiene origen en el site).
    Devuelve la posición del objeto en ECI.

    Args:
        r_objeto_RSW (vector): position vector of object

        a (float): semi-major axis [m] or 'Inf' for parabolas

        e (float): eccentricity [-]

        i (float): inclination [deg]

        raan (float): right ascension of ascending node [deg]

        w (float): argument of perigee [deg]

        theta (float): true anomaly [deg]

    Outputs:
        r_objeto_ECI (vector): position vector of object
    '''                             
    p = a*(1-e**2)
    r_site_ECI, v_site_ECI = COE2RV(p, e, i, raan, w, theta, in_angle = 'Deg')    
    RSW2PQW = ROT3(theta, False)
    r_objeto_PQW = np.dot(RSW2PQW, r_objeto_RSW)
    PQW2ECI = np.dot(np.dot(ROT3(raan, False), ROT1(i, False)),ROT3(w, False))
    r_objeto_ECI = np.dot(PQW2ECI, r_objeto_PQW) + r_site_ECI
    return r_objeto_ECI

def Hohmann(radioi, radiof, mu = 398600.441):
    '''
    Devuelve el delta de velocidad (delta_v) total y el tiempo de vuelo (t_trans) en una transferencia de Hohmann
    entre dos orbitas circulares con radio inicial (radioi) y radio final (radiof).

    Args:
        radioi (float): initial radius [km]

        radiof (float): final radius [km]

        mu (float): central body gravitational parameter [km^3/s^2]

    Outputs:
        delta_v (float): total change in velocity [km/s]

        t_trans (float): time on trasfer orbit [s]
    '''
    a_trans = (radioi+radiof)/2
    
    vi = np.sqrt(mu/radioi)
    v_transa = np.sqrt((2*mu/radioi)-mu/a_trans)
    
    vf = np.sqrt(mu/radiof)
    v_transb = np.sqrt((2*mu/radiof)-mu/a_trans)

    delta_va = v_transa-vi
    delta_vb = vf-v_transb
    delta_v = abs(delta_va)+abs(delta_vb)
    
    t_trans = np.pi*np.sqrt((a_trans**3)/mu)
    
    return delta_v, t_trans

def HohmannElliptic(r_peri_i = None, r_apo_i = None, r_peri_f = None, r_apo_f = None, apoi = True, apof = True, mu = 398600.441):

    # Momento angular orbita inicial
    h_i = np.sqrt(2*mu*(r_apo_i*r_peri_i/(r_apo_i+r_peri_i)))
    # Momento angular orbita final
    h_f = np.sqrt(2*mu*(r_apo_f*r_peri_f/(r_apo_f+r_peri_f)))

    if apoi:
        if apof: # APO - APO
            h_apo_apo = np.sqrt(2*mu*(r_apo_i*r_apo_f/(r_apo_i+r_apo_f)))
            v_A_i = h_i/r_apo_i
            v_A_f = h_apo_apo/r_apo_i
            delta_v_A = abs(v_A_f-v_A_i)
            v_B_i = h_apo_apo/r_apo_f
            v_B_f = h_f/r_apo_f
            delta_v_B = abs(v_B_f-v_B_i)
            delta_v = delta_v_A + delta_v_B

        if not apof: # APO - PERI
            h_apo_peri = np.sqrt(2*mu*(r_apo_i*r_peri_f/(r_apo_i+r_peri_f)))
            v_A_i = h_i/r_apo_i
            v_A_f = h_apo_peri/r_apo_i
            delta_v_A = abs(v_A_f-v_A_i)
            v_B_i = h_apo_peri/r_peri_f
            v_B_f = h_f/r_peri_f
            delta_v_B = abs(v_B_f-v_B_i)
            delta_v = delta_v_A + delta_v_B

    if not apoi:
        if not apof: # PERI - PERI
            h_peri_peri = np.sqrt(2*mu*(r_peri_i*r_peri_f/(r_peri_i+r_peri_f)))
            v_A_i = h_i/r_peri_i
            v_A_f = h_peri_peri/r_peri_i
            delta_v_A = abs(v_A_f-v_A_i)
            v_B_i = h_peri_peri/r_peri_f
            v_B_f = h_f/r_peri_f
            delta_v_B = abs(v_B_f-v_B_i)
            delta_v = delta_v_A + delta_v_B

        if apof: # PERI - APO
            h_peri_apo = np.sqrt(2*mu*(r_peri_i*r_apo_f/(r_peri_i+r_apo_f)))
            v_A_i = h_i/r_peri_i
            v_A_f = h_peri_apo/r_peri_i
            delta_v_A = abs(v_A_f-v_A_i)
            v_B_i = h_peri_apo/r_apo_f
            v_B_f = h_f/r_apo_f
            delta_v_B = abs(v_B_f-v_B_i)
            delta_v = delta_v_A + delta_v_B

    return delta_v

def Bi_elliptic(radioi, radiob, radiof, mu = 398600.441):
    '''    
    Devuelve el delta de velocidad (delta_v) total y el tiempo de vuelo (t_trans) en una transferencia Bi-eliptica
    entre dos orbitas circulares con el radio inicial (radioi), el radio final (radiof) y
    el radio en el punto b de la orbita de transferencia (radiob).

    Args:
        radioi (float): initial radius [km]

        radiob (float): radius at point b [km]

        radiof (float): final radius [km]

        mu (float): central body gravitational parameter [km^3/s^2]

    Outputs:
        delta_v (float): total change in velocity [km/s]

        t_trans (float): time on trasfer orbit [s]
    '''
    a_trans1 = (radioi+radiob)/2
    a_trans2 = (radiob+radiof)/2

    vi = np.sqrt(mu/radioi)
    v_trans1a = np.sqrt((2*mu/radioi)-mu/a_trans1)
    v_trans1b = np.sqrt((2*mu/radiob)-mu/a_trans1)
    v_trans2b = np.sqrt((2*mu/radiob)-mu/a_trans2)
    v_trans2c = np.sqrt((2*mu/radiof)-mu/a_trans2)
    vf = np.sqrt(mu/radiof)

    delta_va = v_trans1a - vi
    delta_vb = v_trans2b - v_trans1b
    delta_vc = vf - v_trans2c
    delta_v = abs(delta_va)+abs(delta_vb)+abs(delta_vc)

    t_trans = np.pi*(np.sqrt((a_trans1**3)/mu)+np.sqrt((a_trans2**3)/mu))

    return delta_v, t_trans

def OneTangentBurn(radioi, radiof, theta_transb, k = 0, mu = 398600.441, periapsis = True, in_angle = 'Rad'):
    '''
    Devuelve el delta de velocidad (delta_v) total y el tiempo de vuelo (t_trans) en una transferencia One-Tangent Burn
    de transferencia eliptica entre dos orbitas circulares con el radio inicial (radioi), el radio final (radiof),
    el radio en el punto b de la orbita de transferencia (radiob) y su anomalía verdadera (theta_transb).
    
    Args:
        radioi (float): initial radius [km]

        radiob (float): radius at point b [km]

        radiof (float): final radius [km]

        theta_transb (float): true anomaly at point b [rad]

        k (int): revolutions on transfer orbit [-] 

        mu (float): central body gravitational parameter [km^3/s^2]

        periapsis (boolean): True if the maneuver starts at the periapsis of the transfer orbit, False for apoapsis

        in_angle (string): angle unit of theta_transb, 'Rad' or 'Deg'

    Outputs:
        delta_v (float): total change in velocity [km/s]

        t_trans (float): time on trasfer orbit [s]
    '''
    if in_angle != 'Rad':
        theta_transb = np.radians(theta_transb)
    
    ratio_ = radioi/radiof

    if periapsis:
        e_trans = (ratio_-1)/(np.cos(theta_transb)-ratio_)
        a_trans = radioi/(1-e_trans)
    else:
        e_trans = (ratio_-1)/(np.cos(theta_transb)+ratio_)
        a_trans = radioi/(1+e_trans)

    vi = np.sqrt(mu/radioi)
    v_transa = np.sqrt((2*mu/radioi) - mu/a_trans)
    vf = np.sqrt(mu/radiof)
    v_transb = np.sqrt((2*mu/radiof) - mu/a_trans)
    delta_va = v_transa-vi
    phi_transb = np.arctan(e_trans*np.sin(theta_transb)/(1+e_trans*np.cos(theta_transb)))
    delta_vb = np.sqrt(v_transb**2+vf**2-2*v_transb*vf*np.cos(phi_transb))
    delta_v = abs(delta_va)+abs(delta_vb)

    E = np.arccos((e_trans+np.cos(theta_transb))/(1+e_trans*np.cos(theta_transb)))
    if periapsis:
        t_trans = np.sqrt((a_trans**3)/mu)*(2*k*np.pi+(E-e_trans*np.sin(E)))
    else: 
        t_trans = np.sqrt((a_trans**3)/mu)*(2*k*np.pi+(E-e_trans*np.sin(E))-(np.pi-e_trans*np.sin(np.pi)))

    return delta_v, t_trans

def CCPhasingSameOrbit(a_tgt, theta, k_tgt = 1, k_int = 1, mu = 398600.441, in_angle = 'Rad'):
    '''
    Circular Coplanar Phasing on Same Orbit
    Devuelve el tiempo en la orbita de fase (t_phase), el delta de velocidad (delta_v)
    y el semi-eje mayor de la órbita de fase (a_phase).

    Args:
        a_tgt (float): semimayor axis of target orbit [km]

        theta (float): phase angle [rad] (positive if interceptor leads target, negative if target leads interceptor)

        k_tgt (int): revolutions on target orbit [-]

        k_int (int): revolutions on phase orbit [-]

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): unit angle of theta, 'Rad' or 'Deg'

    Outputs:
        delta_v (float): total change in velocity [km/s]

        t_phase (float): time on phasing orbit [s]

        a_phase (float): semimayor axis of phase orbit [km]
    '''
    if in_angle != 'Rad':
        theta = np.radians(theta)

    w_tgt = np.sqrt(mu/a_tgt**3)
    t_phase = (2*np.pi*k_tgt+theta)/w_tgt
    a_phase = (mu*((t_phase/(2*np.pi*k_int))**2))**(1/3)
    if a_phase < a_tgt:
        r_peri = 2*a_phase - a_tgt
        if r_peri <= 6378.1363:
            print('¡Choca con la Tierra!')
    delta_v = 2*abs(np.sqrt(2*mu/a_tgt-mu/a_phase)-np.sqrt(mu/a_tgt))

    return delta_v, t_phase, a_phase

def CCPhasingDifOrbit(a_tgt, a_int, thetai, k = 1, mu = 398600.441, in_angle = 'Rad'):
    '''
    Circular Coplanar Phasing on Different Orbits
    Devuelve el tiempo en la orbita de transferencia (t_trans), el delta de velocidad (delta_v),
    el semi-eje mayor de la órbita de transferencia (a_trans), el tiempo de espera hasta el inicio de transferencia (t_wait)
    y el periodo en el cuál se repite la geometría.

    Args:
        a_tgt (float): semimayor axis of target orbit [km]

        a_int (float): semimayor axis of interceptor orbit [km]

        thetai (float): initial phase angle [rad] (positive if interceptor leads target, negative if target leads interceptor)

        k (int): revolutions in which the geometry is repeated [-]

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): unit angle of thetai, 'Rad' or 'Deg'

    Outputs:
        delta_v (float): total change in velocity [km/s]

        t_trans (float): time on transfer orbit [s]

        a_trans (float): semimayor axis of transfer orbit [km]

        t_wait (float): time until the interceptor and target are in the correct positions [s]

        synodic (float): synodic period [s]
    '''
    if in_angle != 'Rad':
        thetai = np.radians(thetai)

    w_tgt = np.sqrt(mu/a_tgt**3)
    w_int = np.sqrt(mu/a_int**3)
    a_trans = (a_int+a_tgt)/2
    t_trans = np.pi*np.sqrt(a_trans**3/mu)
    theta = np.pi-w_tgt*t_trans
    t_wait = ((theta-thetai)+2*np.pi*k)/(w_int-w_tgt)
    synodic = 2*np.pi*k/(w_int-w_tgt)
    delta_v = abs(np.sqrt(2*mu/a_int-mu/a_trans)-np.sqrt(mu/a_int))+abs(np.sqrt(2*mu/a_tgt-mu/a_trans)-np.sqrt(mu/a_tgt))
    theta = np.degrees(theta)
    return delta_v, t_trans, a_trans, t_wait, synodic, theta

def MarteTierra(v_sonda_Sol):
    '''
    Una sonda hace una transferencia de Hohmann de una orbita terrestre a una orbita marciana (circulares).
    Visto desde la Tierra, la sonda sale en una orbita hyperbolica. Visto desde el Sol, la orbita es eliptica.
    '''
    radio_orbita_terrestre = 149597828.68
    mu_Sol = 1.32712440018e11
    mu_Tierra = 398600.441
    Re = 6378.1363
    h = 250 # Altura orbita terrestre

    # Velocidad de la sonda respecto al Sol, velocidad final en el punto inicial de la orbita de transferencia.
    # Obtener con función Hohmann() la v_transa
    # v_sonda_Sol = 32.73

    # Ecuación vis-viva v = np.sqrt(mu*(2/r-1/a))

    # La Tierra orbita circularmente al Sol. La velocidad de la Tierra respecto al Sol es,
    v_terrestre_Sol = np.sqrt(mu_Sol/radio_orbita_terrestre)
   
    # La sonda orbita circularmente a la Tierra. Su velocidad inicial (pre-manioibra),
    vi_sonda_Tierra = np.sqrt(mu_Tierra/(Re+h))
    
    # Cuando la sonda sale del dominio terrestre, desde la Tierra lo hace en una orbita hyperbolica
    # Esta velocidad remanente con la que va a seguir es vinf de la sonda, que es la velocidad de la sonda despecto a la Tierra
    # v_sonda_Tierra = vinf_sonda
    # v_sonda_Tierra + v_terrestre_Sol = v_sonda_Sol
    vinf_sonda = v_sonda_Sol - v_terrestre_Sol
    
    # De la escuación vis-viva vinf_sonda = np.sqrt(mu_Tierra/(-a)) donde a < 0 (hyperbolico)
    a = mu_Tierra/vinf_sonda**2
    
    # La sonda orbita hyperbolicamente a la Tierra. Su velocidad final (post-manioibra),
    vf_sonda_Tierra = np.sqrt(mu_Tierra*(2/(Re+h)-1/a))
    
    delta_v = vf_sonda_Tierra - vi_sonda_Tierra
    return delta_v

def constelacion(radio, sep, mu = 398604.441):
    '''
    Dado un radio de orbita circular, devuelve su periodo y el mean motion.
    '''
    T = 2*np.pi*np.sqrt(radio**3/mu)
    n = np.floor(T/sep)

    return T, n

def LaunchWindow(yr, mo, d, h, min, s, DUT1, i, raan, rango, phi_gc, lamb, in_angle ='Deg'):

    thetaGMST_0 = thetaGMST(yr, mo, d, h, min, s, DUT1)

    if in_angle == 'Deg':
        i = np.radians(i)
        phi_gc = np.radians(phi_gc)
    
    if in_angle != 'Deg':
        raan = np.degrees(raan)
        rango = np.degrees(rango)
        lamb = np.degrees(lamb)
    
    beta = np.arcsin(np.cos(i)/np.cos(phi_gc))  # azimut de lanzamiento [rad]

    lamb_u = np.degrees(np.arccos(np.cos(beta)/np.sin(i)))  # angulo auxiliar [deg]

    thetaGMST_1 = raan - rango + lamb_u - lamb    
    thetaGMST_2 = raan + rango + lamb_u - lamb 

    w_Tierra = 0.00417807462229498 # velocidad angular de la tierra en grados/seg

    UT_1 = (thetaGMST_1 - thetaGMST_0)/w_Tierra
    UT_2 = (thetaGMST_2 - thetaGMST_0)/w_Tierra

    h1, min1, s1 = seconds2time(UT_1)
    h2, min2, s2 = seconds2time(UT_2)

    return h1, min1, s1, h2, min2, s2

def InclinationOnly(delta_i, e, a = None, p = None, theta = None, mu = 398600.441, in_angle = 'Deg'):
    '''
    Devuelve el delta de velocidad (delta_v) en la maniobra Inclination Only entre orbitas circulares o elipticas.

    Args:
        delta_i (float): inclination change [deg]

        e (float): eccentricity [-]

        a (float): semimayor axis [km] (obligatory for circular case. for eliptical case, enter a instead of p)

        p (float): semilatus rectum [km] (for eliptical case, enter a instead of p)

        theta (float): true anomaly at node [deg] (for eliptical case)

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of delta_i and theta, 'Rad' or 'Deg'

    Outputs:
        delta_v (float): total change in velocity [km/s]

        u (int): node of manuver InclinationOnly [deg]
    '''
    if in_angle == 'Deg' and theta != None:
        theta = np.radians(theta)
    
    if in_angle == 'Deg':
        delta_i = np.radians(delta_i)

    if e == 0.0:
        vi = np.sqrt(mu/a)
        delta_v = 2*vi*np.sin(delta_i/2)
        u = None
    else:
        if p == None and a != None:
            p = a*(1-e**2)
        if p != None and a == None:
            a = p/(1-e**2)
        
        r1 = p/(1+e*np.cos(theta))
        vi1 = np.sqrt((2*mu/r1)-mu/a)
        phi_fpa1 = np.arctan(e*np.sin(theta)/(1+e*np.cos(theta)))
        delta_v1 = 2*np.cos(phi_fpa1)*vi1*np.sin(delta_i/2)
        
        r2 = p/(1+e*np.cos(abs(theta-np.pi)))
        vi2 = np.sqrt((2*mu/r2)-mu/a)
        phi_fpa2 = np.arctan(e*np.sin(theta-np.pi)/(1+e*np.cos(abs(theta-np.pi))))
        delta_v2 = 2*np.cos(phi_fpa2)*vi2*np.sin(delta_i/2)

        if delta_v1 <= delta_v2:
            delta_v = delta_v1
            u = np.degrees(theta)
        else:
            delta_v = delta_v2
            u = np.degrees(abs(theta - np.pi))
    delta_v = abs(delta_v)
    return delta_v, u

def RAANOnly(i_initial, delta_raan, a, mu = 398600.441, in_angle = 'Deg', out_angle = 'Deg'):
    '''
    Devuelve el delta de velocidad (delta_v) y los argumentos de latitud final e inicial
    en la maniobra RAAN Only entre orbitas circulares.

    Args:
        i_initial (float): initial inclination [deg]

        delta_raan (float): delta RAAN [deg]

        a (float): semimayor axis [km]

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of i_initial and delta_raan, 'Rad' or 'Deg'

        out_angle (string): angle unit of u_initial and u_final, 'Rad' or 'Deg'

    Outputs:
        delta_v (float): total change in velocity [km/s]

        u_initial (float): initial argument of latitud [deg]

        u_final (float): final argument of latitud [deg]
    '''
    if in_angle == 'Deg':
        i_initial = np.radians(i_initial)
        delta_raan = np.radians(delta_raan)
    vi = np.sqrt(mu/a)
    theta = np.arccos((np.cos(i_initial)**2)+(np.sin(i_initial)**2)*np.cos(delta_raan))
    delta_v = 2*vi*np.sin(theta/2)
    # u_initial es el u donde iniciar la maniobra
    u_initial = np.arccos(np.tan(i_initial)*(np.cos(delta_raan)-np.cos(theta))/np.sin(theta))
    # u_final es el u que queda cuando ya se realizo la maniobra, toma un tiempo la maniobra, va a estar en otro u
    u_final = np.arccos(np.cos(i_initial)*np.sin(i_initial)*((1-np.cos(delta_raan))/np.sin(theta)))
    if out_angle == 'Deg':
        u_initial = np.degrees(u_initial)
        u_final = np.degrees(u_final)
    delta_v = abs(delta_v)
    return delta_v, u_initial, u_final

def Combinada(i_initial, i_final, delta_raan, a, mu = 398600.441, in_angle = 'Deg', out_angle = 'Deg'):
    '''
    Devuelve el delta de velocidad (delta_v) y los argumentos de latitud final e inicial
    en la maniobra combinada (Incination and RAAN) entre orbitas circulares.

    Args:
        i_initial (float): initial inclination [deg]

        i_final (float): final inclination [deg]

        delta_raan (float): ascending node change [deg]

        a (float): semimayor axis [km]

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of i_initial, i_final and delta_raan, 'Rad' or 'Deg'

        out_angle (string): angle unit of u_initial and u_final, 'Rad' or 'Deg'

    Outputs:
        delta_v (float): total change in velocity [km/s]

        u_initial (float): initial argument of latitud [deg]

        u_final (float): final argument of latitud [deg]
    '''
    if in_angle == 'Deg':
        i_initial = np.radians(i_initial)
        i_final = np.radians(i_final)
        delta_raan = np.radians(delta_raan)
    vi = np.sqrt(mu/a)
    theta = np.arccos(np.cos(i_initial)*np.cos(i_final)+np.sin(i_initial)*np.sin(i_final)*np.cos(delta_raan))
    delta_v = 2*vi*np.sin(theta/2)
    u_initial = np.arccos((np.sin(i_final)*np.cos(delta_raan)-np.cos(theta)*np.sin(i_initial))/(np.sin(theta)*np.cos(i_initial)))
    u_final = np.arccos((np.cos(i_initial)*np.sin(i_final)-np.sin(i_initial)*np.cos(i_final)*np.cos(delta_raan))/np.sin(theta))
    if out_angle == 'Deg':
        u_initial = np.degrees(u_initial)
        u_final = np.degrees(u_final)
    return delta_v, u_initial, u_final

def InclinationRAAN(i_initial, i_final, delta_raan, a, mu = 398600.441, in_angle = 'Deg', out_angle = 'Deg'):
    '''
    Devuelve el delta de velocidad (delta_v) y los argumentos de latitud inicial, donde iniciar el RAAN,
    y final, de la maniobra Inclination, luego, RAAN entre orbitas circulares.

    Args:
        i_initial (float): initial inclination [deg]

        i_final (float): final inclination [deg]

        delta_raan (float): ascending node change [deg]

        a (float): semimayor axis [km]

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of i_initial, i_final and delta_raan, 'Rad' or 'Deg'

        out_angle (string): angle unit of u_initial and u_final, 'Rad' or 'Deg'

    Outputs:
        delta_v (float): total change in velocity [km/s]

        u (int): node of manuver InclinationOnly [deg]

        u_initial (float): initial argument of latitud [deg]

        u_final (float): final argument of latitud [deg]
    '''
    if in_angle == 'Deg':
        i_initial = np.radians(i_initial)
        i_final = np.radians(i_final)
        delta_raan = np.radians(delta_raan)
    
    delta_i = i_final-i_initial
    delta_vI, u = InclinationOnly(delta_i, 0, a, 0, 0, mu, in_angle = 'Rad')
    delta_vRAAN, u_initial, u_final = RAANOnly(i_final, delta_raan, a, mu, in_angle = 'Rad')
    delta_v = delta_vI + delta_vRAAN
    if out_angle != 'Deg':
        u_initial = np.radians(u_initial)
        u_final = np.radians(u_final)
    return delta_v, u, u_initial, u_final

def RAANInclination(i_initial, i_final, delta_raan, a, mu = 398600.441, in_angle = 'Deg', out_angle = 'Deg'):
    '''
    Devuelve el delta de velocidad (delta_v) y los argumentos de latitud inicial, donde iniciar el RAAN,
    y final, de la maniobra RAAN, luego, Inclination entre orbitas circulares.

    Args:
        i_initial (float): initial inclination [deg]

        i_final (float): final inclination [deg]

        delta_raan (float): ascending node change [deg]

        a (float): semimayor axis [km]

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of i_initial, i_final and delta_raan, 'Rad' or 'Deg'

        out_angle (string): angle unit of u_initial and u_final, 'Rad' or 'Deg'

    Outputs:
        delta_v (float): total change in velocity [km/s]

        u (int): node of manuver InclinationOnly [deg]

        u_initial (float): initial argument of latitud [deg]

        u_final (float): final argument of latitud [deg]
    '''
    if in_angle == 'Deg':
        i_initial = np.radians(i_initial)
        i_final = np.radians(i_final)
        delta_raan = np.radians(delta_raan)
    
    delta_vRAAN, u_initial, u_final = RAANOnly(i_initial, delta_raan, a, mu, in_angle = 'Rad')
    delta_i = i_final-i_initial
    delta_vI, u = InclinationOnly(delta_i, 0, a, 0, 0, mu, in_angle = 'Rad')
    delta_v = delta_vI + delta_vRAAN
    if out_angle != 'Deg':
        u_initial = np.radians(u_initial)
        u_final = np.radians(u_final)
    return delta_v, u, u_initial, u_final

def MinimumCombinedPlaneChange(i_initial, i_final, r_initial, r_final, mu = 398600.441, in_angle = 'Deg', out_angle = 'Deg'):
    '''
    Maniobra combinada: transferencia de Hohmann entre orbitas circulares con dos cambios de inclinacion (puntos inicial y final).
    Optimiza el cambio de velocidad (delta_v) total realizando dos cambios de inclinación (delta_i_initial, delta_i_final) en lugar de uno.
    
    Args:
        i_initial (float): initial inclination [deg]

        i_final (float): final inclination [deg]

        r_initial (float): initial orbit ratio [km]

        r_final (float): final orbit ratio [km]

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of i_initial and i_final, 'Rad' or 'Deg'

        out_angle (string): angle unit of delta_i_initial and delta_i_final, 'Rad' or 'Deg'

    Outputs:
        delta_i_initial (float): change in inclination at initial point [deg]
        
        delta_i_final (float): change in inclination at final point [deg]
        
        delta_v (float): total change in velocity [km/s]
    '''
    a_trans = (r_initial+r_final)/2
    v_initial = np.sqrt(mu/r_initial)
    v_final = np.sqrt(mu/r_final)
    v_trans_a = np.sqrt((2*mu/r_initial)-mu/a_trans)
    v_trans_b = np.sqrt((2*mu/r_final)-mu/a_trans)
    
    if in_angle == 'Deg':
        i_initial = np.radians(i_initial)
        i_final = np.radians(i_final)
    delta_i = i_final - i_initial
    
    R = r_final/r_initial
    s = (1/delta_i)*np.arctan(np.sin(delta_i)/(R**(3/2)+np.cos(delta_i)))
    
    delta_i_initial = s*delta_i
    delta_i_final = (1-s)*delta_i
    
    delta_v_initial = np.sqrt((v_initial)**2+(v_trans_a)**2-2*v_initial*v_trans_a*np.cos(delta_i_initial))
    delta_v_final = np.sqrt((v_final)**2+(v_trans_b)**2-2*v_final*v_trans_b*np.cos(delta_i_final))
    delta_v = abs(delta_v_initial) + abs(delta_v_final)
    
    if out_angle == 'Deg':
        delta_i_initial = np.degrees(delta_i_initial)
        delta_i_final = np.degrees(delta_i_final)

    return delta_i_initial, delta_i_final, delta_v

def r_SEZ2belro(r_SEZ, out_angle = 'Deg'):
    
    ro = np.sqrt(r_SEZ[0]**2+r_SEZ[1]**2+r_SEZ[2]**2)
    phi = np.arccos(r_SEZ[2]/ro)
    theta = np.arctan(r_SEZ[1]/r_SEZ[0])
    el = np.pi/2 - phi
    b = np.pi - theta
    
    if out_angle == 'Deg':
        b = np.degrees(b)
        el = np.degrees(el)
    
    return b, el, ro

def belro2r_SEZv_SEZ(b, ro, el, b_, ro_, el_, in_angle = 'Deg'):
    
    if in_angle == 'Deg':
        b = np.radians(b)
        el = np.radians(el)
        b_ = np.radians(b_)
        el_ = np.radians(el_)

    x = -ro*np.cos(el)*np.cos(b)
    y = ro*np.cos(el)*np.sin(b)
    z = ro*np.sin(el)

    x_ = -ro_*np.cos(el)*np.cos(b) + ro*np.sin(el)*np.cos(b)*el_ + ro*np.cos(el)*np.sin(b)*b_
    y_ = ro_*np.cos(el)*np.sin(b) - ro*np.sin(el)*np.sin(b)*el_ + ro*np.cos(el)*np.cos(b)*b_
    z_ = ro_*np.sin(el) + ro*np.cos(el)*el_

    r = np.array([x, y, z])
    v = np.array([x_, y_, z_])
    return r, v

def GCRF2ECI(r, asc, decl):
    x = r*np.cos(decl)*np.cos(asc)
    y = r*np.cos(decl)*np.sin(asc)
    z = r*np.sin(decl)
    r_ECI = np.array([x, y, z])
    return r_ECI

def SiteTrackVallado(phi_gd, lamb, h_ellp, ro, b, el, ro_, b_, el_, yr, mo, d, h, min, s, DUT1, R = 6378.1363, e = 0.081819221456, in_angle = 'Deg'):
    if in_angle == 'Deg':
        phi_gd = np.radians(phi_gd)
        lamb = np.radians(lamb)
        b = np.radians(b)
        el = np.radians(el)
        b_ = np.radians(b_)
        el_ = np.radians(el_)
    C = R/np.sqrt(1-e**2*(np.sin(phi_gd))**2)
    S = C*(1-e**2)
    r_delta = (C+h_ellp)*np.cos(phi_gd)
    r_k = (S+h_ellp)*np.sin(phi_gd)
    r_site_ECEF = np.array([r_delta*np.cos(lamb), r_delta*np.sin(lamb), r_k])
    r_SEZ, v_SEZ = belro2r_SEZv_SEZ(b, ro, el, b_, ro_, el_, in_angle = 'Rad')
    SEZ2ECEF = np.dot(ROT2((np.pi/2 - phi_gd), positive = False), ROT3(lamb, positive = False))
    r_ECEF = np.dot(SEZ2ECEF, r_SEZ) + r_site_ECEF
    v_ECEF = np.dot(SEZ2ECEF, v_SEZ)
    dia = datetime(yr, mo, d, h, min, s)
    thetaGMST_ = thetaGMST(dia.year, dia.month, dia.day, dia.hour, dia.minute, dia.second, DUT1, out_angle = 'Deg')
    ECEF2ECI = ROT3(thetaGMST_, positive = False)
    r_ECI = np.dot(ECEF2ECI, r_ECEF)
    v_ECI = np.dot(ECEF2ECI, v_ECEF)
    return r_ECI, v_ECI

def SiteTrackKluever(phi_gd, lamb, h_ellp, ro, b, el, ro_, b_, el_, R = 6378.1363, e = 0.081819221456, in_angle = 'Deg'):
    
    if in_angle == 'Deg':
        phi_gd = np.radians(phi_gd)
        lamb = np.radians(lamb)
        b = np.radians(b)
        el = np.radians(el)
        b_ = np.radians(b_)
        el_ = np.radians(el_)

    w = [0, 0, 7.292115e-5]

    C = R/np.sqrt(1-(e**2)*(np.sin(phi_gd)**2))
    S = C*(1-e**2)
    r_site_ECI = np.array([(C+h_ellp)*np.cos(phi_gd)*np.cos(lamb), (C+h_ellp)*np.cos(phi_gd)*np.sin(lamb), (S+h_ellp)*np.sin(phi_gd)])
    
    r_SEZ, v_SEZ = belro2r_SEZv_SEZ(b, ro, el, b_, ro_, el_, in_angle = 'Rad')
    SEZ2ECI = np.dot(ROT3(lamb, positive = False), ROT2((np.pi/2 - phi_gd), positive = False))
    r_ECI = np.dot(SEZ2ECI, r_SEZ)
    v_ECI = np.dot(SEZ2ECI, v_SEZ)

    r_ECI = r_ECI + r_site_ECI
    v_ECI = v_ECI + np.cross(w, r_ECI)

    return r_ECI, v_ECI

def MetodoGibbs(r_1, r_2, r_3, vr, mu = 398600.441, out_angle = 'Deg'): 
    '''
    Devuelve posicion y velocidad del satelite (r_ECI, v_ECI) y los elementos orbitales de la orbita,
    dados tres vectores posiciones en ECI (r_1, r_2, r_3).

    Args:
        r_1 (float): first position vector [km]

        r_2 (float): second position vector [km]

        r_3 (float): third position vector [km]

        vr (string): positicion vector to use with v, 'r_1', 'r_2' or 'r_3'

        out_angle (string): angle unit of coe, 'Rad' or 'Deg'

    Outputs:
        r_ECI (float): position vector [km]

        v_ECI (float): velocity vector [km/s]

        coe (float): list of orbital elements
    '''
    r_12 = np.cross(r_1, r_2)
    r_23 = np.cross(r_2, r_3)
    r_31 = np.cross(r_3, r_1)
    D = r_12 + r_23 + r_31
    N = np.dot(r_23, np.linalg.norm(r_1)) + np.dot(r_31, np.linalg.norm(r_2)) + np.dot(r_12, np.linalg.norm(r_3)) 
    S = -(np.dot(r_2-r_3, np.linalg.norm(r_1)) + np.dot(r_3-r_1, np.linalg.norm(r_2)) + np.dot(r_1-r_2, np.linalg.norm(r_3)))

    if vr == 'r_1':
        v_1 = np.dot((1/np.linalg.norm(r_1))*np.sqrt(mu/(np.linalg.norm(N)*np.linalg.norm(D))), np.cross(D, r_1)) + np.dot(np.sqrt(mu/(np.linalg.norm(N)*np.linalg.norm(D))), S)
        p, a, e, i, raan, w, theta, u, lt, wt = RV2COE(r_1, v_1, mu)        
        r_ECI = r_1
        v_ECI = v_1
        coe = [a, e, i, raan, w, theta, u, lt, wt]

    if vr == 'r_2':
        v_2 = np.dot((1/np.linalg.norm(r_2))*np.sqrt(mu/(np.linalg.norm(N)*np.linalg.norm(D))), np.cross(D, r_2)) + np.dot(np.sqrt(mu/(np.linalg.norm(N)*np.linalg.norm(D))), S)
        p, a, e, i, raan, w, theta, u, lt, wt = RV2COE(r_2, v_2, mu)        
        r_ECI = r_2
        v_ECI = v_2
        coe = [a, e, i, raan, w, theta, u, lt, wt]

    if vr == 'r_3':
        v_3 = np.dot((1/np.linalg.norm(r_3))*np.sqrt(mu/(np.linalg.norm(N)*np.linalg.norm(D))), np.cross(D, r_3)) + np.dot(np.sqrt(mu/(np.linalg.norm(N)*np.linalg.norm(D))), S)
        p, a, e, i, raan, w, theta, u, lt, wt = RV2COE(r_3, v_3, mu)        
        r_ECI = r_3
        v_ECI = v_3
        coe = [a, e, i, raan, w, theta, u, lt, wt]

    return r_ECI, v_ECI, coe

'''
Datos
mu_terrestre = 398600.441 km^3/s^2 = 3.986e14 m^3/s^2
radio_terrestre = 6378.1363 km
radio_orbita_terrestre = 149597828.68 km
mu_solar = mu = 1.32712440018e11 km^3/s^2
DAT = 37
DUT1 = -0.2 o -0.1
w_Tierra = np.degrees(7.292e-5)

Position vector of the site (on earth's surface) given longitude and geocentric latitude
r_site = [r*np.cos(phi_gc)*np.cos(lamb), r*np.cos(phi_gc)*np.sin(lamb), r*np.sin(phi_gc)]

Position vector of the satellite (SEZ) with respec to the ground station given Azimuth (B), Elevation (el) and Range (Ra)
p_SEZ = [-Ra*np.cos(el)*np.cos(B), Ra*np.cos(el)*np.sin(B), Ra*np.sin(el)]

Velocity vector of the satellite (SEZ) with respec to the ground station given Azimuth (B, B_dot), Elevation (el, el_dot) and Range (Ra, Ra_dot)
comp1 = -Ra_dot*np.cos(el)*np.cos(B) + Ra*np.sin(el)*np.cos(B)*el_dot + Ra*np.cos(el)*np.sin(B)*B_dot
comp2 =  Ra_dot*np.cos(el)*np.sin(B) - Ra*np.sin(el)*np.sin(B)*el_dot + Ra*np.cos(el)*np.cos(B)*B_dot
comp3 =  Ra_dot*np.sin(el) + Ra*np.cos(el)*el_dot
p_dot_SEZ = [comp1, comp2, comp3]

Geocentric and Geodetic latitude relation
phi_gd = np.arctan(np.tan(phi_gc)/(1-(0.081819221456)**2)) 
phi_gc = np.arctan((1-(0.081819221456)**2)*np.tan(phi_gd))

Matriz de transformacion (SEZ / ECEF) - Revisar signos con lamb (421)
SEZ2ECEF = np.dot(rot(-lamb, 3), rot(-(np.pi/2 - phi_gd), 2))
ECEF2SEZ = np.dot(rot(np.pi/2 - phi_gd, 2), rot(-lamb, 3))

Matriz de transformacion (PQW / RSW)
PQW2RSW = rot(th, 3)
RSW2PQW = rot(-th, 3)

Matriz de transformacion (ECI / ECEF)
ECI2ECEF = rot(GMST, 3)
ECEF2ECI = rot(-GMST, 3)

Matriz de transformacion (ECI / PQW)
ECI2PQW = np.dot(np.dot(rot(aop, 3), rot(i, 1)), rot(raan, 3))
PQW2ECI = np.dot(np.dot(rot(-raan, 3), rot(-i, 1)), rot(-aop, 3))
'''