"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def r_RSW2r_ECI(r_objeto_RSW, a, e, i, raan, w, theta):
    '''
    Toma la posición del objeto en RSW (RSW tiene origen en el site).
    Devuelve la posición del objeto en ECI.

    Args:
        r_objeto_RSW (vector): position vector of object

        a (float): semi-major axis [m] or 'Inf' for parabolas

        e (float): eccentricity [-]

        i (float): inclination [deg]

        raan (float): right ascension of ascending node [deg]

        w (float): argument of perigee [deg]

        theta (float): true anomaly [deg]

    Outputs:
        r_objeto_ECI (vector): position vector of object
    '''                             
    p = a*(1-e**2)
    r_site_ECI, v_site_ECI = COE2RV(p, e, i, raan, w, theta, in_angle = 'Deg')    
    RSW2PQW = ROT3(theta, False)
    r_objeto_PQW = np.dot(RSW2PQW, r_objeto_RSW)
    PQW2ECI = np.dot(np.dot(ROT3(raan, False), ROT1(i, False)),ROT3(w, False))
    r_objeto_ECI = np.dot(PQW2ECI, r_objeto_PQW) + r_site_ECI
    return r_objeto_ECI