"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np
from datetime import datetime, timedelta

def JulianDate(yr, mo, d, h, min, s):

    """
    Finds the Julian date (based on UT1) from the entered date and time (gregorian)

    Args:
        yr (int): year

        mo (int): month

        d (int): day

        h (int): hours

        min (int): minutes

        s (float): seconds
    
    Outputs:
        JD (float): Julian date

    """

    JD = 367*(yr) - int(7*(yr + int(((mo + 9)/12)))/4) + int(275*mo/9) + d + 1721013.5 + (((s/60 + min)/60) + h)/24

    return JD

def ConvTime(yr, mo, d, h, min, s, DAT):

    """
    UTC time to the respective Julian date (J2000) 

    Args:
        yr (int): year

        mo (int): month

        d (int): day

        h (int): hours

        min (int): minutes

        s (float): seconds

        DUT1 (float): delta of UT1 - UTC [s]

        DAT (float): delta of TAI - UTC [s]

    Outputs:
        TTT (float): Julian date in centuries (J2000)

    """
    UTC = datetime(yr, mo, d, h, min, s)
    DAT = timedelta(seconds=DAT)
    TAI = UTC + DAT
    TT = TAI + timedelta(seconds=32.184)
    JDTT = JulianDate(TT.year, TT.month, TT.day, TT.hour, TT.minute, TT.second)
    TTT = (JDTT - 2451545)/36525

    return TTT

def thetaGMST(yr, mo, d, h, min, s, DUT1, out_angle = 'Deg'):

    """
    Finds the GMST (Greenwich Mean Sidereal Time) angle given the UTC time. 

    Args:
        yr (int): year

        mo (int): month

        d (int): day

        h (int): hours

        min (int): minutes

        s (float): seconds

        DUT1 (float): delta of UT1 - UTC [s]

    Outputs:
        th_GMST (float): GMST angle [deg][rad]

    """

    UTC = datetime(yr, mo, d, h, min, s)
    DUT1 = timedelta(seconds=DUT1)
    UT1 = UTC + DUT1
    UT1_seconds = timedelta(hours=UT1.hour, minutes=UT1.minute, seconds=UT1.second).total_seconds()

    JDUT1 = JulianDate(UT1.year, UT1.month, UT1.day, UT1.hour, UT1.minute, UT1.second)
    TUT1 = (JDUT1 - 2451545)/36525
    
    thetaGMST = 100.4606184 + 36000.77005361*TUT1  + 0.00038793*(TUT1**2) - (2.6e-8)*(TUT1**3)
    w = (1.002737909350795*360)/(24*3600)
    thetaGMST = thetaGMST + w*UT1_seconds
    thetaGMST = np.mod(thetaGMST, 360)

    if out_angle != 'Deg':
        thetaGMST = thetaGMST*np.pi/180
        
    return thetaGMST