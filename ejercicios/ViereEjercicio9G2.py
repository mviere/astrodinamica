"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""

import numpy as np
from matplotlib import pyplot as plt
import random

def KepEqtnE(M, e, abstol = 1e-8, itlim = 1e8):
    """
    Kepler equation to convert mean anomaly and eccentricity to eccentric anomaly
    
    Args:
        M (float): mean anomaly [deg]
        
        e (float): eccentricity [-]

        abstol (float): maximum tolerance for E value [deg]
        
        itlim (int): maximum iterations
    
    Outputs:
        E (float): eccentric anomaly [rad]
        
        convergence (int): 1 if calculation converged within itlim else 0
    """
    M = M*np.pi/180
    if abstol != 1e-8:
        abstol = abstol*np.pi/180
    E = M - e if np.sin(M) < 0 else M + e
    E_ = E + 2*abstol
    i = 0
    convergence = 1
    while abs(E-E_) > abstol and i < itlim:
        E_ = E; i += 1
        if i >= itlim:
            print('Calulation stopped, iteration limit reached')
            convergence = 0
            break
        E = E_ + (M - E_ + e*np.sin(E_)) / (1 - e*np.cos(E_))
    return E, convergence

def E_2_theta(E,e):
    '''
    Eccentric anomaly to true anomaly
    
    Args:
        E (float): eccentric anomaly [rad]

        e (float): eccentricity [-]

    Outputs:
        theta (float): true anomaly [deg]
    '''
    sin_theta = (np.sin(E)*np.sqrt(1-e**2))/(1-e*np.cos(E))
    cos_theta = (np.cos(E)-e)/(1-e*np.cos(E))
    theta = np.arctan2(sin_theta, cos_theta)
    theta = theta*180/np.pi
    return theta

e_list = []
theta_list = []
M_list = []

for k in range(10):
    e_list.append(random.uniform(0.0, 0.99))

plt.figure(figsize=(10, 10), dpi=80)                                 
plt.grid()
plt.xlabel("True Anomaly")
plt.ylabel("Mean Anomaly")
plt.xlim(-180, 180)
plt.ylim(-180, 180)
plt.xticks([-180, -150, -120, -90, -60, -30, 0, 30, 60, 90, 120, 150, 180],
           [r'$-180°$', r'$-150°$', r'$-120°$', r'$-90°$', r'$-60°$', r'$-30°$', r'$0°$', r'$30°$', r'$60°$', r'$90°$', r'$120°$', r'$150°$', r'$180°$'])
plt.yticks([-180, -150, -120, -90, -60, -30, 0, 30, 60, 90, 120, 150, 180],
           [r'$-180°$', r'$-150°$', r'$-120°$', r'$-90°$', r'$-60°$', r'$-30°$', r'$0°$', r'$30°$', r'$60°$', r'$90°$', r'$120°$', r'$150°$', r'$180°$'])

for e in e_list:
    for i in range(-180, 181, 1):
        M = i
        M_list.append(M)
        E, convergence = KepEqtnE(M, e)
        theta = E_2_theta(E,e)
        theta_list.append(theta)
        plt.plot(theta_list, M_list, linewidth=0.3, linestyle="-")

plt.show()
