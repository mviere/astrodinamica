"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def theta_2_M(theta, e):
    '''
    True anomaly to mean anomaly
    
    Args:
        theta (float): true anomaly [deg]
            - eliptical 0 < theta < 360
            - parabolic -180 < theta < 180
            - hyperbolic -180 < theta < 180 (aprox)

        e (float): eccentricity [-]

    Outputs:
        M (float): mean anomaly [deg]
            - eliptical 0 < M < 360
            - parabolic -180 < M < 180
            - hyperbolic -180 < M < 180 (aprox)
    '''
    theta = theta*np.pi/180
    if e < 1.0:
        sin_E = (np.sin(theta)*np.sqrt(1-e**2))/(1+e*np.cos(theta))
        cos_E = (np.cos(theta)+e)/(1+e*np.cos(theta))
        E = np.arctan2(sin_E, cos_E)
        if E < 0:
            E += 2*np.pi
        M = E - e*np.sin(E)
    if e == 1:
        B = np.tan(theta/2)
        M = B + (B**3)/3
    if e > 1.0:
        H = np.arcsinh((np.sin(theta)*np.sqrt(e**2-1))/(1+e*np.cos(theta)))
        M = e*np.sinh(H) - H
    M = M*180/np.pi
    return M

if __name__ == "__main__":
    print('-------------------------------------------------------------------------------------------')
    print('This is an example to verify the functionality of the script.')
    print('Given theta = 260 deg and e = 0.7411, mean anomaly for eliptical orbit should be 342.86 deg.')
    #print('Given theta = 52.7 deg and e = 1, mean anomaly for parabolic orbit should be 15.35 deg.')
    #print('Given theta = 131.28 deg and e = 1.4, mean anomaly for hyperbolic orbit should be 155.4 deg.')
    print('Let\'s see,')
    M_e = theta_2_M(260, 0.7411)
    #M_b = theta_2_M(52.7, 1)
    #M_h = theta_2_M(131.28, 1.4)
    print('>>> eliptical mean anomaly  (M): ', M_e)
    #print('>>> parabolic mean anomaly  (M): ', M_b)
    #print('>>> hyperbolic mean anomaly  (M): ', M_h)
    print('-------------------------------------------------------------------------------------------')