for i in range(N):
    r_vector = X[0:3,i]
    v_vector = X[3:6,i]
    p, a, e, i, o, w, theta, u, lt, wt = RV_2_COE(r_vector, v_vector)
    Y[:,i] = np.array([a, e, i, o, w, theta])