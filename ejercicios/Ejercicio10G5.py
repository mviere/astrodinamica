"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

def JulianDate(yr, mo, d, h, min, s):

    """
    Finds the Julian date (based on UT1) from the entered date and time (gregorian)

    Args:
        yr (int): year

        mo (int): month

        d (int): day

        h (int): hours

        min (int): minutes

        s (float): seconds
    
    Outputs:
        JD (float): Julian date

    """

    JD = 367*(yr) - int(7*(yr + int(((mo + 9)/12)))/4) + int(275*mo/9) + d + 1721013.5 + (((s/60 + min)/60) + h)/24

    return JD

def time2seconds(h, min, s):

    """
    Converts hours, minutes and seconds into seconds

    Args:
        h (int): hours

        min (int): minutes

        s (float): seconds
    
    Outputs:
        UTs (float): seconds from the beginning of the day

    """

    UTs = h*3600 + min*60 + s

    return UTs

def seconds2time(UTs):

    """
    Converts seconds into hours, minutes and seconds

    Args:
        UTs (float): seconds from the beginning of the day
        
    Outputs:
        h (int): hours

        min (int): minutes

        s (float): seconds
    
    """

    i = UTs / 3600.0
    h  = np.floor(i)
    min = np.floor((i - h)*60)
    s = (i - h - min/60 )*3600
    
    return h, min, s

'''
def thetaGMST(yr, mo, d, h, min, s, DUT1, out_angle = 'Deg'):

    """
    Finds the GMST (Greenwich Mean Sidereal Time) angle given the UTC time. 

    Args:
        yr (int): year

        mo (int): month

        d (int): day

        h (int): hours

        min (int): minutes

        s (float): seconds

        DUT1 (float): delta of UT1 - UTC [s]

    Outputs:
        th_GMST (float): GMST angle [deg][rad]

    """

    UTC = time2seconds(h, min, s)
    UT1 = UTC + DUT1
    h1, min1, s1 = seconds2time(UT1)

    JDUT1 = JulianDate(yr, mo, d, int(h1), int(min1), s1)

    TUT1 = (JDUT1 - 2451545.0)/36525.0

    th_GMST = 100.4606184 + 36000.77005361*TUT1  + 0.00038793*(TUT1**2) - (2.6e-8)*(TUT1**3)

    w = (1.002737909350795*360)/(24*3600)

    th_GMST = th_GMST + w*UT1

    th_GMST = np.mod(th_GMST, 360)

    if out_angle == 'Deg':

        return th_GMST

    else:
        
        return np.radians(th_GMST)
'''
'''
def thetaGMST(yr, mo, d, h, min, s, DUT1, out_angle = 'Deg'):

    """
    Finds the GMST (Greenwich Mean Sidereal Time) angle given the UTC time. 

    Args:
        yr (int): year

        mo (int): month

        d (int): day

        h (int): hours

        min (int): minutes

        s (float): seconds

        DUT1 (float): delta of UT1 - UTC [s]

    Outputs:
        th_GMST (float): GMST angle [deg][rad]

    """
    UTC = datetime(yr, mo, d, h, min, s)
    DUT1 = timedelta(seconds=DUT1)
    UT1 = UTC + DUT1
    UT1_seconds = timedelta(hours=UT1.hour, minutes=UT1.minute, seconds=UT1.second).total_seconds()

    JDUT1 = JulianDate(UT1.year, UT1.month, UT1.day, UT1.hour, UT1.minute, UT1.second)
    TUT1 = (JDUT1 - 2451545)/36525
    
    thetaGMST = 100.4606184 + 36000.77005361*TUT1  + 0.00038793*(TUT1**2) - (2.6e-8)*(TUT1**3)
    w = (1.002737909350795*360)/(24*3600)
    thetaGMST = thetaGMST + w*UT1_seconds
    thetaGMST = np.mod(thetaGMST, 360)

    if out_angle != 'Deg':
        thetaGMST = thetaGMST*np.pi/180
        
    return thetaGMST
'''

def thetaGMST(yr, mo, d, h, min, s, DUT1, out_angle = 'Deg'):
    '''
    Devuelve el thetaGMST (Greenwich Mean Sidereal Time) dado una fecha y tiempo UTC. 

    Args:
        yr (int): year

        mo (int): month

        d (int): day

        h (int): hours

        min (int): minutes

        s (float): seconds

        DUT1 (float): UT1 - UTC [s]

        out_angle (string): angle unit of thetaGMST, 'Rad' or 'Deg'

    Outputs:
        thetaGMST (float): GMST angle [deg]
    '''
    UTC = datetime(yr, mo, d, h, min, s)
    DUT1 = timedelta(seconds = DUT1)
    UT1 = UTC + DUT1
    UT1_seconds = timedelta(hours = UT1.hour, minutes = UT1.minute, seconds = UT1.second).total_seconds()

    JDUT1 = JulianDate(UT1.year, UT1.month, UT1.day, UT1.hour, UT1.minute, UT1.second)
    TUT1 = (JDUT1 - 2451545)/36525
    
    thetaGMST = 100.4606184 + 36000.77005361*TUT1 + 0.00038793*(TUT1**2) - (2.6e-8)*(TUT1**3)
    w_terrestre = (1.002737909350795*360)/(24*3600)
    thetaGMST = thetaGMST + w_terrestre*UT1_seconds
    # Cantidad de grados totales dividido 360, me da un resto, eso es el thetaGMST
    thetaGMST = np.mod(thetaGMST, 360)
    
    if out_angle != 'Deg':
        thetaGMST = np.radians(thetaGMST)
    
    return thetaGMST

def DMS2Rad(deg, amin, aseg):

    """
    Convert angles expressed in degree, arcminute and arcsecond (DMS) to radians

    Args:
        deg (int): degrees [0-359]

        amin (int): arcminutes [0-59]

        asec (float): arcseconds [0-59.9]
        
    Outputs:
        rad (float): radians [rad]

    """

    rad = (deg + amin/60 + aseg/3600)*np.pi/180

    return rad

def HMS2Rad(h, min, s):

    """
    Convert hours, minutes and seconds to radians

    Args:
        h (int): hours [0-23]

        min (int): minutes [0-59]

        s (float): seconds [0-59.9]
        
    Outputs:
        rad (float): radians [rad]

    """

    rad = 15*(h + min/60 + s/3600)*np.pi/180

    return rad

def ROT1(a, positive = True): # por default crece positivamente
    if positive:
        ROT1 = np.array([[1, 0, 0], [0, np.cos(a), np.sin(a)], [0, -np.sin(a), np.cos(a)]])
    else:
        ROT1 = np.array([[1, 0, 0], [0, np.cos(a), -np.sin(a)], [0, np.sin(a), np.cos(a)]])
    return ROT1

def ROT2(a, positive = True): # por default crece positivamente
    if positive:
        ROT2 = np.array([[np.cos(a), 0, -np.sin(a)], [0, 1, 0], [np.sin(a), 0, np.cos(a)]])
    else:
        ROT2 = np.array([[np.cos(a), 0, np.sin(a)], [0, 1, 0], [-np.sin(a), 0, np.cos(a)]])
    return ROT2

def ROT3(a, positive = True): # por default crece positivamente
    if positive:
        ROT3 = np.array([[np.cos(a), np.sin(a), 0], [-np.sin(a), np.cos(a), 0], [0, 0, 1]])
    else:
        ROT3 = np.array([[np.cos(a), -np.sin(a), 0], [np.sin(a), np.cos(a), 0], [0, 0, 1]])
    return ROT3

def GCRF2ECI(r, asc, decl):
    x = r*np.cos(decl)*np.cos(asc)
    y = r*np.cos(decl)*np.sin(asc)
    z = r*np.sin(decl)
    r_ECI = np.array([x, y, z])
    return r_ECI

def r_SEZ2belro(r_SEZ, out_angle = 'Deg'):
    
    ro = np.sqrt(r_SEZ[0]**2+r_SEZ[1]**2+r_SEZ[2]**2)
    phi = np.arccos(r_SEZ[2]/ro)
    theta = np.arctan(r_SEZ[1]/r_SEZ[0])
    el = np.pi/2 - phi
    b = np.pi - theta
    
    if out_angle == 'Deg':
        b = np.degrees(b)
        el = np.degrees(el)
    
    return b, el, ro

# Paso la posición de Sirio en GCRF a ECI 
r = 1
asc = HMS2Rad(6, 45, 8.9173)
decl = DMS2Rad(-16, 42, 58.017)
r_ECI = GCRF2ECI(r, asc, decl)

# Defino la matriz ECEF2SEZ, localizando el site del SEZ en Buenos Aires
phi_gc = np.radians(-34.61315)
lamb = np.radians(-58.37723)
phi_gd = np.arctan(np.tan(phi_gc)/(1-(0.081819221456)**2))
ECEF2SEZ = np.dot(ROT2(np.pi/2 - phi_gd), ROT3(lamb))
initial_date = datetime(2022, 2, 22, 3, 0, 0)

# Defino la matriz ECEF2SEZ, localizando el site del SEZ en Teofilo Tabanera
'''phi_gd = np.radians(-31.524075)
lamb = np.radians(-64.463522)
ECEF2SEZ = np.dot(ROT2(np.pi/2 - phi_gd), ROT3(lamb))
initial_date = datetime(2022, 2, 22, 3, 0, 0)'''


plt.figure(figsize=(10, 10), dpi=80)
plt.subplot(1, 1, 1)                                 
plt.grid()
plt.xlim(0, 86400)
plt.xticks([0, 3600, 7200, 10800, 14400, 18000, 21600, 25200, 28800, 32400, 36000, 39600, 43200, 46800, 50400, 54000, 57600, 61200, 64800, 68400, 72000, 75600, 79200, 82800, 86400],
           [r'$0$', r'$1$', r'$2$', r'$3$', r'$4$', r'$5$', r'$6$', r'$7$', r'$8$', r'$9$', r'$10$', r'$11$', r'$12$', r'$13$', r'$14$', r'$15$', r'$16$', r'$17$', r'$18$', r'$19$', r'$20$', r'$21$', r'$22$', r'$23$', r'$24$'])
plt.xlabel("Time [h]")
plt.ylabel("Elevation [deg]")
plt.suptitle("Elevation throught time of Buenos Aires-Sirio direction")

for k in range(86400):
    # Defino el thetaGMST, varía con el tiempo
    date = initial_date + timedelta(seconds=k)
    thetaGMST_ = thetaGMST(date.year, date.month, date.day, date.hour, date.minute, date.second, -0.2, out_angle = 'Rad')
    # Defino la matriz ECI2ECEF
    # SIRIO se mantiene constante en ECI y varía en ECEF con thetaGMST
    ECI2ECEF = ROT3(thetaGMST_)
    # Paso la posición de Sirio en ECI a ECEF
    r_ECEF = np.dot(ECI2ECEF, r_ECI)
    # Paso la posición de Sirio en ECEF a SEZ
    # SIRIO varía en ECEF y en SEZ con thetaGMST
    r_SEZ = np.dot(ECEF2SEZ, r_ECEF)
    # Obtengo b, el y ro para cada thetaGMST. ro no varía porque r = 1
    b, el, ro = r_SEZ2belro(r_SEZ, out_angle = 'Deg')
    if k > 36000 and 0 <= el < 0.1:
        print(el)
        print(k)
    plt.plot(k, el, marker = "o", markersize = 0.8, markeredgecolor = "pink", markerfacecolor = "pink")

plt.show()