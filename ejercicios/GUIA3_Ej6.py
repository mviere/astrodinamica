"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

'''
Funcion principal
'''
def th_GMST(yr, mo, d, h, min, s, DUT1, Angle = 'Deg'):

    """
    Finds the GMST (Greenwich Mean Sidereal Time) angle given the UTC time. 

    Args:
        yr (int): year

        mo (int): month

        d (int): day

        h (int): hours

        min (int): minutes

        s (float): seconds

        DUT1 (float): delta of UT1 - UTC [s]

    Outputs:
        th_GMST (float): GMST angle [deg][rad]

    """

    UTC = time2seconds(h, min, s)
    UT1 = UTC + DUT1
    print('seconds UT1:',UT1)
    h1, min1, s1 = seconds2time(UT1)

    JDUT1 = JulianDate(yr, mo, d, int(h1), int(min1), s1)
    print('JDUT1: ',JDUT1)
    TUT1 = (JDUT1 - 2451545.0)/36525.0
    print('TUT1: ',TUT1)

    th_GMST = 100.4606184 + 36000.77005361*TUT1  + 0.00038793*(TUT1**2) - (2.6e-8)*(TUT1**3)
    print('theta: ',th_GMST)
    w = (1.002737909350795*360)/(24*3600)

    th_GMST = th_GMST + w*UT1
    print('theta: ',th_GMST)
    th_GMST = np.mod(th_GMST, 360)

    if Angle == 'Deg':

        return th_GMST

    else:
        
        return deg2rad(th_GMST)

'''
Funciones secundarias
'''
def JulianDate(yr, mo, d, h, min, s):

    """
    Finds the Julian date (based on UT1) from the entered date and time (gregorian)

    Args:
        yr (int): year

        mo (int): month

        d (int): day

        h (int): hours

        min (int): minutes

        s (float): seconds
    
    Outputs:
        JD (float): Julian date

    """

    JD = 367*(yr) - int(7*(yr + int(((mo + 9)/12)))/4) + int(275*mo/9) + d + 1721013.5 + (((s/60 + min)/60) + h)/24

    return JD

def time2seconds(h, min, s):

    """
    Converts hours, minutes and seconds into seconds

    Args:
        h (int): hours

        min (int): minutes

        s (float): seconds
    
    Outputs:
        UTs (float): seconds from the beginning of the day

    """

    UTs = h*3600 + min*60 + s

    return UTs

def seconds2time(UTs):

    """
    Converts seconds into hours, minutes and seconds

    Args:
        UTs (float): seconds from the beginning of the day
        
    Outputs:
        h (int): hours

        min (int): minutes

        s (float): seconds
    
    """

    i = UTs / 3600.0
    h  = np.floor(i)
    min = np.floor((i - h)*60)
    s = (i - h - min/60 )*3600
    
    return h, min, s

def deg2rad(a):

    """
    Conversion from degrees to radians
    
    Args:
        a (float): angle [deg]
    
    Outputs:
        a (float): angle [rad]
        
    """
    
    a = a*np.pi/180
    return a
