"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def rv2coe(r, v, Angle = 'Deg', mu = 398600.441):

    """
    Convert state vector (ECI) to keplerian orbital elements (PQW). User can choose 
    radians (Angle = 'rad') or degrees (Angle = 'Deg') for the output angles.
    
    Args:
        r (array, float): position vector [km]
        
        v (array, float): velocity vevtor [km/s]
    
    Outputs:
        p (float): semi-parameter [km]

        a (float): semi-mayor axis [km]

        e (float): eccentricity [-]

        i (float): inclination [deg][rad]

        raan (float): longitude of ascending node [deg][rad]

        aop (float): argument of periapsis [deg][rad]

        th (float): true anomaly [deg][rad]

    """

    h = np.cross(r, v)

    n = np.cross([0,0,1], h)

    e = (np.multiply(r, norm(v)**2 - mu/norm(r)) - np.multiply(v, np.dot(r, v)))/mu

    E = (norm(v)**2)/2 - mu/norm(r)

    if norm(e) != 1:
        a = -mu/(2*E)
        p = a*(1 - norm(e)**2)

    else:
        a = 'Inf'
        p = (norm(h)**2)/mu

    i = np.arccos(h[2]/norm(h))

    if norm(n) == 0:                                                   # Equatorial
        if norm(e) == 0:                                               # Circular
            lamb = np.arccos(r[0]/norm(r))
            
            if r[1] < 0:
                lamb = 2*np.pi - lamb
            
            raan = 0
            aop = 0
            th = lamb

        else:                                    
            omt = np.arccos(e[0]/norm(e))

            if e[1] < 0:
                omt = 2*np.pi - omt

            raan = 0
            aop = omt
            th = np.arccos(np.dot(e, r)/(norm(e)*norm(r)))

    elif norm(e) == 0:                                                 # Circular
        raan = np.arccos(n[0]/norm(n))
        aop = 0
        u = np.arccos(np.dot(n, r)/(norm(n)*norm(r)))
        
        if r[2] < 0:
            u = 2*np.pi - u

        th = u

    else:
        raan = np.arccos(n[0]/norm(n))

        if n[1] < 0:
            raan = 2*np.pi - raan

        aop = np.arccos(np.dot(n, e)/(norm(n)*norm(e)))

        if e[2] < 0:
            aop = 2*np.pi - aop

        th = np.arccos(np.dot(e, r)/(norm(e)*norm(r)))
    
        if np.dot(r, v) < 0:
            th = 2*np.pi - th
    
    if Angle == 'Deg':
        i = rad2deg(i)
        raan = rad2deg(raan)
        aop = rad2deg(aop)
        th = rad2deg(th)

    return p, norm(a), norm(e), i, raan, aop, th

def norm(v):

    """
    Returns the norm of the given vector
    
    Args:
        v (array, float): vector 
    
    Outputs:
       |v| (float): norm of the vector
        
    """

    return np.linalg.norm(v)

def rad2deg(a):

    """
    Conversion from radians to degrees
    
    Args:
        a (float): angle [rad]
    
    Outputs:
        a (float): angle [deg]
        
    """

    a = a*180/np.pi
    return a


r1 = np.array([-5920.3995, -2605.6070, -684.7419])
r2 = np.array([4105.8403, -5440.4387, -2702.8755])
r3 = np.array([5247.8183, 4826.6112, 1710.5731])

mu = 398600.441

r1_r2 = np.cross(r1, r2)
r2_r3 = np.cross(r2, r3)
r3_r1 = np.cross(r3, r1)

D = r2_r3 + r3_r1 + r1_r2

N = np.dot(r2_r3, norm(r1)) + np.dot(r3_r1, norm(r2)) + np.dot(r1_r2, norm(r3)) 

S = -(np.dot(r2 - r3, norm(r1)) + np.dot(r3 - r1, norm(r2)) + np.dot(r1 - r2, norm(r3)))

v2 = np.dot((1/norm(r2))*np.sqrt(mu/(norm(N)*norm(D))), np.cross(D, r2)) + np.dot(np.sqrt(mu/(norm(N)*norm(D))), S)

'''p, a, e, i, raan, aop, th = rv2coe(r2, v2, Angle = 'Deg', mu = 398600.441)

print('a', a)
print('e', e)
print('i', i)
print('raan', raan)
print('aop', aop)
print('th', th)'''


p, a, e, i, raan, aop, th = rv2coe(r3, v2, Angle = 'Deg', mu = 398600.441)
print(th)

