"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np
from datetime import datetime, timedelta

def KepEqtnE(M, e, abstol = 1e-8, itlim = 1e8):
    """
    Kepler equation to convert mean anomaly and eccentricity to eccentric anomaly
    
    Args:
        M (float): mean anomaly [rad]
        
        e (float): eccentricity [-]

        abstol (float): maximum tolerance for E value [rad]
        
        itlim (int): maximum iterations
    
    Outputs:
        E (float): eccentric anomaly [rad]
        
        convergence (int): 1 if calculation converged within itlim else 0
    """
    E = M - e if np.sin(M) < 0 else M + e
    E_ = E + 2*abstol
    i = 0
    convergence = 1
    while abs(E-E_) > abstol and i < itlim:
        E_ = E; i += 1
        if i >= itlim:
            print('Calulation stopped, iteration limit reached')
            convergence = 0
            break
        E = E_ + (M - E_ + e*np.sin(E_)) / (1 - e*np.cos(E_))
    return E, convergence

def KepEqtnP(dt, p, mu = 3.986e14):
    """
    Kepler equation to convert time of flight since periapsis to parabolic anomaly
    
    Args:
        dt (float): time of flight since periapsis [s]
        
        p (float): semilatus rectum [m]

        mu (float): central body gravitational parameter [m^3/s^2]
    
    Outputs:
        B (float): parabolic anomaly [rad]
    """
    s = (np.arctan(1/(3*(np.sqrt(mu/(p**3)))*dt)))/2
    w = np.arctan(np.cbrt(np.tan(s)))
    B = 2*(1/np.tan(2*w))
    return B

def KepEqtnH(M, e, abstol = 1e-8, itlim = 1e8):
    """
    Kepler equation to convert mean anomaly and eccentricity to hyperbolic anomaly
    
    Args:
        M (float): mean anomaly [rad]
        
        e (float): eccentricity [-]

        abstol (float): maximum tolerance for H value [rad]
        
        itlim (int): maximum iterations
    
    Outputs:
        H (float): hyperbolic anomaly [rad]
        
        convergence (int): 1 if calculation converged within itlim else 0
    """
    if e < 1.6:
        if -np.pi<M<0 or M>np.pi:
            H = M - e
        else:
            H = M + e
    else:
        if e<3.6 and np.absolute(M)>np.pi:
            H = M - np.sign(M)*e
        else:
            H = M/(e-1)
    H_ = H + 2*abstol
    i = 0
    convergence = 1
    while abs(H-H_) > abstol and i < itlim:
        H_ = H; i += 1
        if i >= itlim:
            print('Calulation stopped, iteration limit reached')
            convergence = 0
            break
        H = H_ + (M + H_ - e*np.sinh(H_)) / (e*np.cosh(H_)-1)
    return H, convergence

def ROT1(a, positive = True): # por default crece positivamente
    if positive:
        ROT1 = np.array([[1, 0, 0], [0, np.cos(a), np.sin(a)], [0, -np.sin(a), np.cos(a)]])
    else:
        ROT1 = np.array([[1, 0, 0], [0, np.cos(a), -np.sin(a)], [0, np.sin(a), np.cos(a)]])
    return ROT1

def ROT2(a, positive = True): # por default crece positivamente
    if positive:
        ROT2 = np.array([[np.cos(a), 0, -np.sin(a)], [0, 1, 0], [np.sin(a), 0, np.cos(a)]])
    else:
        ROT2 = np.array([[np.cos(a), 0, np.sin(a)], [0, 1, 0], [-np.sin(a), 0, np.cos(a)]])
    return ROT2

def ROT3(a, positive = True): # por default crece positivamente
    if positive:
        ROT3 = np.array([[np.cos(a), np.sin(a), 0], [-np.sin(a), np.cos(a), 0], [0, 0, 1]])
    else:
        ROT3 = np.array([[np.cos(a), -np.sin(a), 0], [np.sin(a), np.cos(a), 0], [0, 0, 1]])
    return ROT3

def COE_2_RV(p, e, i, o = None, w = None, theta = None, u = None, lt = None, wt = None, mu = 3.986e14, in_angle = 'Rad'):

    if in_angle != 'Rad':
        i = i*np.pi/180
        if o != None:
            o = o*np.pi/180
        if w != None:
            w = w*np.pi/180
        if theta != None:
            theta = theta*np.pi/180
        if u != None:
            u = u*np.pi/180
        if lt != None:
            lt = lt*np.pi/180
        if wt != None:
            wt = wt*np.pi/180
    if i == 0.0 and e == 0.0:
        o = 0.0
        w = 0.0
        theta = lt
    if i != 0.0 and e == 0.0:
        w = 0.0
        theta = u
    if i == 0.0 and e < 1:
        o = 0.0
        w = wt

    r_vector_PQW_i = (p*np.cos(theta))/(1+e*np.cos(theta))
    r_vector_PQW_j = (p*np.sin(theta))/(1+e*np.cos(theta))
    r_vector_PQW = np.array([[r_vector_PQW_i], [r_vector_PQW_j], [0]])

    v_vector_PQW_i = -np.sqrt(mu/p)*np.sin(theta)
    v_vector_PQW_j = np.sqrt(mu/p)*(e+np.cos(theta))
    v_vector_PQW = np.array([[v_vector_PQW_i], [v_vector_PQW_j], [0]])
    PQW2ECI = np.dot(np.dot(ROT3(o, False), ROT1(i, False)),ROT3(w, False))
    r_vector_ECI = np.dot(PQW2ECI, r_vector_PQW)
    v_vector_ECI = np.dot(PQW2ECI, v_vector_PQW)

    return r_vector_ECI, v_vector_ECI

def RV_2_COE(r_vector, v_vector, mu = 3.986e14, out_angle = 'Rad'):
    '''
    State vectors to orbital elements.
    
    Args:
        r_vector (array of floats [1x3]): vector position [m]

        v_vector (array of floats [1x3]): velocity vector [m]

        mu (float): central body gravitational parameter [m^3/s^2]

    '''
    r = np.linalg.norm(r_vector)
    v = np.linalg.norm(v_vector)

    h_vector = np.cross(r_vector, v_vector)
    h = np.linalg.norm(h_vector)
    
    n_vector = np.cross([0,0,1], h_vector)
    n = np.linalg.norm(n_vector)
    
    e_vector =(np.multiply((v**2-mu/r),r_vector)-np.multiply(np.dot(r_vector, v_vector),v_vector))/mu
    e = np.linalg.norm(e_vector)
    
    EN = v**2/2 - mu/r

    if e != 1.0:
        a = -mu/(2*EN)
        p = a*(1-e**2)
    else:
        p = h**2/mu
        a = 'Inf'
    
    i = np.arccos(h_vector[2]/h)
    o = np.arccos(n_vector[0]/n)
    
    if n_vector[1] < 0:
        o = 2*np.pi - o
    
    w = np.arccos(np.dot(n_vector, e_vector)/(n*e))
    if e_vector[2] < 0:
        w = 2*np.pi - w
    
    theta = np.arccos(np.dot(e_vector, r_vector)/(e*r))
    if np.dot(r_vector, v_vector) < 0:
        theta = 2*np.pi - theta
    
    if i == 0 and e < 1.0:
        wt = np.arccos(e_vector[0]/e)
        if e_vector[1] < 0:
            wt = 2*np.pi - wt
    else:
        wt = None
    
    if i != 0 and e == 0:
        u = np.arccos(np.dot(n_vector, r_vector)/(n*r))
        if r_vector[2] < 0:
            u = 2*np.pi - u
    else:
        u = None
    
    if i == 0 and e == 0:
        lt = np.arccos(r_vector[0]/r)
        if r_vector[1] < 0:
            lt = 2*np.pi - lt
    else:
        lt = None
    
    if out_angle != 'Rad':
        i = i*180/np.pi
        o = o*180/np.pi
        w = w*180/np.pi
        theta = theta*180/np.pi
        if u != None:    
            u = u*180/np.pi
        if lt != None:
            lt = lt*180/np.pi
        if wt != None:
            wt = wt*180/np.pi

    return p, a, e, i, o, w, theta, u, lt, wt

def theta_2_E_B_H(theta, e):
    '''
    True anomaly to eccentric anomaly or parabolic anomaly or hyperbolic anomaly
    
    Args:
        theta (float): true anomaly [rad]
            - eliptical 0 < theta < 2pi
            - parabolic -pi < theta < pi
            - hyperbolic -pi + arcos(1/e) < theta < pi - arcos(1/e)

        e (float): eccentricity [-]

    Outputs:
        E (float): eccentric anomaly [rad] 0 < E < 2pi

        B (float): parabolic anomaly [rad] -pi< theta < pi

        H (float): hyperbolic anomaly [rad] 
    '''
    if e < 1.0:
        sin_E = (np.sin(theta)*np.sqrt(1-e**2))/(1+e*np.cos(theta))
        cos_E = (np.cos(theta)+e)/(1+e*np.cos(theta))
        E = np.arctan2(sin_E, cos_E)
        return E
    if e == 1:
        B = np.tan(theta/2)
        return B
    if e > 1.0:
        H = np.arcsinh((np.sin(theta)*np.sqrt(e**2-1))/(1+e*np.cos(theta))) 
        return H

def E_B_H_2_theta(e, E = None, B = None, H = None, p = None, r = None):
    '''
    Eccentric anomaly or parabolic anomaly or hyperbolic anomaly to true anomaly
    
    Args:
        e (float): eccentricity [-]

        E (float): eccentric anomaly [rad]

        B (float): parabolic anomaly [rad]

        H (float): hyperbolic anomaly [rad]

        p (float): semilatus rectum [m] (add for parabolic case)

        r (float): ratio [m] (add for parabolic case)

    Outputs:
        theta (float): true anomaly [rad]
    '''
    if e < 1.0:
        sin_theta = (np.sin(E)*np.sqrt(1-e**2))/(1-e*np.cos(E))
        cos_theta = (np.cos(E)-e)/(1-e*np.cos(E))
    if e == 1:
        sin_theta = (p*B/r)
        cos_theta = ((p-r)/r)
    if e > 1.0:
        sin_theta = (-np.sinh(E)*np.sqrt(e**2-1))/(1-e*np.cosh(E))
        cos_theta = (np.cosh(E)-e)/(1-e*np.cosh(E))
    theta = np.arctan2(sin_theta, cos_theta)
    return theta

def propagate(r_0, v_0, dt, mu = 3.986e14):

    p, a, e, i, o, w, theta, u, lt, wt = RV_2_COE(r_0, v_0, mu)
    E1 = theta_2_E_B_H(theta, e)
    n = np.sqrt(mu/a**3)
    
    if e < 1.0:                                               
        M1 = E1 - e*np.sin(E1)
        M2 = M1 + n*dt
        E2, convergence = KepEqtnE(M2, e)
        theta = E_B_H_2_theta(E2, e)
    
    if e == 1.0:
        r = np.linalg.norm(r_0)                                           
        M1 = E1 + E1**3/3
        E2 = KepEqtnP(dt, p, mu)
        theta = E_B_H_2_theta(e, B = E2, p = p, r = r)

    if e > 1.0:                                               
        M1 = e*np.sinh(E1) - E1
        M2 = M1 + n*dt
        E2, convergence = KepEqtnH(M2, e)
        theta = E_B_H_2_theta(e, H = E2)
        
    r_vector, v_vector = COE_2_RV(p, e, i, o, w, theta, mu)

    return r_vector, v_vector

def JulianDate(yr, mo, d, h, min, s):

    """
    Finds the Julian date (based on UT1) from the entered date and time (gregorian)

    Args:
        yr (int): year

        mo (int): month

        d (int): day

        h (int): hours

        min (int): minutes

        s (float): seconds
    
    Outputs:
        JD (float): Julian date

    """

    JD = 367*(yr) - int(7*(yr + int(((mo + 9)/12)))/4) + int(275*mo/9) + d + 1721013.5 + (((s/60 + min)/60) + h)/24

    return JD

def ConvTime(yr, mo, d, h, min, s, DAT):

    """
    UTC time to the respective Julian date (J2000) 

    Args:
        yr (int): year

        mo (int): month

        d (int): day

        h (int): hours

        min (int): minutes

        s (float): seconds

        DUT1 (float): delta of UT1 - UTC [s]

        DAT (float): delta of TAI - UTC [s]

    Outputs:
        TTT (float): Julian date in centuries (J2000)

    """
    UTC = datetime(yr, mo, d, h, min, s)
    DAT = timedelta(seconds=DAT)
    TAI = UTC + DAT
    TT = TAI + timedelta(seconds=32.184)
    JDTT = JulianDate(TT.year, TT.month, TT.day, TT.hour, TT.minute, TT.second)
    TTT = (JDTT - 2451545)/36525

    return TTT

def thetaGMST(yr, mo, d, h, min, s, DUT1, out_angle = 'Deg'):

    """
    Finds the GMST (Greenwich Mean Sidereal Time) angle given the UTC time. 

    Args:
        yr (int): year

        mo (int): month

        d (int): day

        h (int): hours

        min (int): minutes

        s (float): seconds

        DUT1 (float): delta of UT1 - UTC [s]

    Outputs:
        th_GMST (float): GMST angle [deg][rad]

    """

    UTC = datetime(yr, mo, d, h, min, s)
    DUT1 = timedelta(seconds=DUT1)
    UT1 = UTC + DUT1
    UT1_seconds = timedelta(hours=UT1.hour, minutes=UT1.minute, seconds=UT1.second).total_seconds()

    JDUT1 = JulianDate(UT1.year, UT1.month, UT1.day, UT1.hour, UT1.minute, UT1.second)
    TUT1 = (JDUT1 - 2451545)/36525
    
    thetaGMST = 100.4606184 + 36000.77005361*TUT1  + 0.00038793*(TUT1**2) - (2.6e-8)*(TUT1**3)
    w = (1.002737909350795*360)/(24*3600)
    thetaGMST = thetaGMST + w*UT1_seconds
    thetaGMST = np.mod(thetaGMST, 360)

    if out_angle != 'Deg':
        thetaGMST = thetaGMST*np.pi/180
        
    return thetaGMST

# Condiciones iniciales FOCUS
a = 6.90418
e = 0.0011
i = 1.7017
o = 3.2004
w = 1.5708
p = a*(1-e**2)
theta = np.pi

rFOCUS_ECI, vFOCUS_ECI = COE_2_RV(p, e, i, o, w, theta)

# Fecha Inicial
initial_date = datetime(2022, 2, 22, 0, 0, 0)

# Fecha Final
final_date = datetime(2022, 2, 27, 0, 0, 0)

DUT1 = -0.2
DAT = 37

# Posicion del Centro Teofilo Tabanera en ECEF (solucion de los problemas 1 a 4)
rCTT_ECEF = np.array([[2346.179148384711], [-4910.818252388238], [-3315.9631023923976]])

# Tiempo de vuelo (segundos) entre las dos fechas (son exactamente 5 dias)
TOF = int((final_date - initial_date).total_seconds())

for c in range(TOF):

    rFOCUS_ECI, vFOCUS_ECI = propagate(rFOCUS_ECI, vFOCUS_ECI, c)

    (p, a, e, i, o, w, theta) = RV_2_COE(rFOCUS_ECI, vFOCUS_ECI)

    ECI2PQW = np.dot(np.dot(ROT3(w), ROT1(i)), ROT3(o))

    PQW2RSW = ROT3(theta)

    thetaGMST = thetaGMST(initial_date.year, initial_date.month, initial_date.day, initial_date.hour, initial_date.minute, c, DUT1, 'Rad')

    ECEF2ECI = ROT3(thetaGMST, False)

    rCTT_ECI = np.dot(ECEF2ECI , rCTT_ECEF)

    rCTT_PQW = np.dot(ECI2PQW, rCTT_ECI)

    rCTT_RSW = np.dot(PQW2RSW, rCTT_PQW)
    
    if np.linalg.norm(rFOCUS_ECI - rCTT_ECI) < 700 and 195 < rCTT_RSW[2] < 421:
        break

initial_date = datetime(2022, 2, 22, 0, 0, 0)
capture_date = initial_date + timedelta(seconds=c)
TTT = ConvTime(capture_date.year, capture_date.month, capture_date.day, capture_date.hour, capture_date.minute, capture_date.second, DUT1, DAT)