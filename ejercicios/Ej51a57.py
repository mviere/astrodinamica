"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def RV2COE(r_ECI, v_ECI, mu = 398600.441, out_angle = 'Deg'):
    '''
    Position vector (r_ECI) and velocity vector (v_ECI), both in ECI,
    to orbital elements (a, e, i, raan, w, theta, u, lt, wt) and semilatus rectum (p).
    Algorithm 9 Vallado.

    Args:
        r_ECI (vector): position vector [km]

        v_ECI (vector): velocity vector [km/s]

        mu (float): central body gravitational parameter [km^3/s^2]

        out_angle (string): angle unit of i, raan, w, theta, u, lt and wt, 'Rad' or 'Deg'

    Outputs:
        p (float): semilatus rectum [km]

        a (float): semi-major axis [m] or 'Inf' for parabolas

        e (float): eccentricity [-]

        i (float): inclination [deg]

        raan (float): right ascension of ascending node [deg]

        w (float): argument of perigee [deg]

        theta (float): true anomaly [deg]

        u (float): argument of latitud [deg]

        lt (float): true longitud [deg]

        wt (float): true longitud of periapsis [deg]
    '''
    r = np.linalg.norm(r_ECI)
    v = np.linalg.norm(v_ECI)

    h_vector = np.cross(r_ECI, v_ECI)
    h = np.linalg.norm(h_vector)
    
    n_vector = np.cross([0,0,1], h_vector)
    n = np.linalg.norm(n_vector)
    
    e_vector =(np.multiply((v**2-mu/r),r_ECI)-np.multiply(np.dot(r_ECI, v_ECI),v_ECI))/mu
    e = np.linalg.norm(e_vector)
    
    EN = v**2/2 - mu/r

    if e != 1.0:
        a = -mu/(2*EN)
        p = a*(1-e**2)
    else:
        p = h**2/mu
        a = 'Inf'
    
    i = np.arccos(h_vector[2]/h)
    raan = np.arccos(n_vector[0]/n)
    
    if n_vector[1] < 0:
        raan = 2*np.pi - raan
    
    w = np.arccos(np.dot(n_vector, e_vector)/(n*e))
    if e_vector[2] < 0:
        w = 2*np.pi - w
    
    theta = np.arccos(np.dot(e_vector, r_ECI)/(e*r))
    if np.dot(r_ECI, v_ECI) < 0:
        theta = 2*np.pi - theta
    
    if i == 0 and e < 1.0:
        wt = np.arccos(e_vector[0]/e)
        if e_vector[1] < 0:
            wt = 2*np.pi - wt
    else:
        wt = None
    
    if i != 0 and e == 0:
        u = np.arccos(np.dot(n_vector, r_ECI)/(n*r))
        if r_ECI[2] < 0:
            u = 2*np.pi - u
    else:
        u = None
    
    if i == 0 and e == 0:
        lt = np.arccos(r_ECI[0]/r)
        if r_ECI[1] < 0:
            lt = 2*np.pi - lt
    else:
        lt = None
    
    if out_angle == 'Deg':
        i = np.degrees(i)
        raan = np.degrees(raan)
        w = np.degrees(w)
        theta = np.degrees(theta)
        if u != None:    
            u = np.degrees(u)
        if lt != None:
            lt = np.degrees(lt)
        if wt != None:
            wt = np.degrees(wt)

    return p, a, e, i, raan, w, theta, u, lt, wt

def MetodoGibbs(r_1, r_2, r_3, vr, mu = 398600.441, out_angle = 'Deg'): 
    '''
    Devuelve posicion y velocidad del satelite (r_ECI, v_ECI) y los elementos orbitales de la orbita,
    dados tres vectores posiciones en ECI (r_1, r_2, r_3).
    Algorithm 54 Vallado.

    Args:
        r_1 (float): first position vector [km]

        r_2 (float): second position vector [km]

        r_3 (float): third position vector [km]

        vr (string): positicion vector to use with v, 'r_1', 'r_2' or 'r_3'

        out_angle (string): angle unit of coe, 'Rad' or 'Deg'

    Outputs:
        r_ECI (float): position vector [km]

        v_ECI (float): velocity vector [km/s]

        coe (float): list of orbital elements
    '''
    r_12 = np.cross(r_1, r_2)
    r_23 = np.cross(r_2, r_3)
    r_31 = np.cross(r_3, r_1)
    D = r_12 + r_23 + r_31
    N = np.dot(r_23, np.linalg.norm(r_1)) + np.dot(r_31, np.linalg.norm(r_2)) + np.dot(r_12, np.linalg.norm(r_3)) 
    S = -(np.dot(r_2-r_3, np.linalg.norm(r_1)) + np.dot(r_3-r_1, np.linalg.norm(r_2)) + np.dot(r_1-r_2, np.linalg.norm(r_3)))

    if vr == 'r_1':
        v_1 = np.dot((1/np.linalg.norm(r_1))*np.sqrt(mu/(np.linalg.norm(N)*np.linalg.norm(D))), np.cross(D, r_1)) + np.dot(np.sqrt(mu/(np.linalg.norm(N)*np.linalg.norm(D))), S)
        p, a, e, i, raan, w, theta, u, lt, wt = RV2COE(r_1, v_1, mu)        
        r_ECI = r_1
        v_ECI = v_1
        coe = [a, e, i, raan, w, theta, u, lt, wt]

    if vr == 'r_2':
        v_2 = np.dot((1/np.linalg.norm(r_2))*np.sqrt(mu/(np.linalg.norm(N)*np.linalg.norm(D))), np.cross(D, r_2)) + np.dot(np.sqrt(mu/(np.linalg.norm(N)*np.linalg.norm(D))), S)
        p, a, e, i, raan, w, theta, u, lt, wt = RV2COE(r_2, v_2, mu)        
        r_ECI = r_2
        v_ECI = v_2
        coe = [a, e, i, raan, w, theta, u, lt, wt]

    if vr == 'r_3':
        v_3 = np.dot((1/np.linalg.norm(r_3))*np.sqrt(mu/(np.linalg.norm(N)*np.linalg.norm(D))), np.cross(D, r_3)) + np.dot(np.sqrt(mu/(np.linalg.norm(N)*np.linalg.norm(D))), S)
        p, a, e, i, raan, w, theta, u, lt, wt = RV2COE(r_3, v_3, mu)        
        r_ECI = r_3
        v_ECI = v_3
        coe = [a, e, i, raan, w, theta, u, lt, wt]

    return r_ECI, v_ECI, coe

# Defino las tres mediciones de posición (tres vectores posición en ECI).
r_1 = np.array([-213.6154048, -2243.7017158, 8968.9367839])
r_2 = np.array([-8644.2871764, -7716.3468827, 2011.1724123])
r_3 = np.array([-7159.0513040, -3077.3139544, -12728.5694556])

# Método Gibbs: obtengo los vectores estado y los elementos orbitales a partir de las tres mediciones de posición.
vr = 'r_2' # Elijo realizar el método Gibbs sobre la segunda medición.
r_ECI, v_ECI, coe = MetodoGibbs(r_1, r_2, r_3, vr)

# Imprimo los elementos orbitales contenidos en una lista.
for element in coe:
    print(element)