"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np
from sgp4.earth_gravity import wgs84
from sgp4.io import twoline2rv
from matplotlib import pyplot as plt
from datetime import datetime
from sgp4.conveniences import sat_epoch_datetime

def RV2COE(r_ECI, v_ECI, mu = 398600.441, out_angle = 'Deg'):
    '''
    Position vector (r_ECI) and velocity vector (v_ECI), both in ECI,
    to orbital elements (a, e, i, raan, w, theta, u, lt, wt) and semilatus rectum (p).

    Args:
        r_ECI (vector): position vector [km]

        v_ECI (vector): velocity vector [km/s]

        mu (float): central body gravitational parameter [km^3/s^2]

        out_angle (string): angle unit of i, raan, w, theta, u, lt and wt, 'Rad' or 'Deg'

    Outputs:
        p (float): semilatus rectum [km]

        a (float): semi-major axis [m] or 'Inf' for parabolas

        e (float): eccentricity [-]

        i (float): inclination [deg]

        raan (float): right ascension of ascending node [deg]

        w (float): argument of perigee [deg]

        theta (float): true anomaly [deg]

        u (float): argument of latitud [deg]

        lt (float): true longitud [deg]

        wt (float): true longitud of periapsis [deg]
    '''
    r = np.linalg.norm(r_ECI)
    v = np.linalg.norm(v_ECI)

    h_vector = np.cross(r_ECI, v_ECI)
    h = np.linalg.norm(h_vector)
    
    n_vector = np.cross([0,0,1], h_vector)
    n = np.linalg.norm(n_vector)
    
    e_vector =(np.multiply((v**2-mu/r),r_ECI)-np.multiply(np.dot(r_ECI, v_ECI),v_ECI))/mu
    e = np.linalg.norm(e_vector)
    
    EN = v**2/2 - mu/r

    if e != 1.0:
        a = -mu/(2*EN)
        p = a*(1-e**2)
    else:
        p = h**2/mu
        a = 'Inf'
    
    i = np.arccos(h_vector[2]/h)
    raan = np.arccos(n_vector[0]/n)
    
    if n_vector[1] < 0:
        raan = 2*np.pi - raan
    
    w = np.arccos(np.dot(n_vector, e_vector)/(n*e))
    if e_vector[2] < 0:
        w = 2*np.pi - w
    
    theta = np.arccos(np.dot(e_vector, r_ECI)/(e*r))
    if np.dot(r_ECI, v_ECI) < 0:
        theta = 2*np.pi - theta
    
    if i == 0 and e < 1.0:
        wt = np.arccos(e_vector[0]/e)
        if e_vector[1] < 0:
            wt = 2*np.pi - wt
    else:
        wt = None
    
    if i != 0 and e == 0:
        u = np.arccos(np.dot(n_vector, r_ECI)/(n*r))
        if r_ECI[2] < 0:
            u = 2*np.pi - u
    else:
        u = None
    
    if i == 0 and e == 0:
        lt = np.arccos(r_ECI[0]/r)
        if r_ECI[1] < 0:
            lt = 2*np.pi - lt
    else:
        lt = None
    
    if out_angle == 'Deg':
        i = np.degrees(i)
        raan = np.degrees(raan)
        w = np.degrees(w)
        theta = np.degrees(theta)
        if u != None:    
            u = np.degrees(u)
        if lt != None:
            lt = np.degrees(lt)
        if wt != None:
            wt = np.degrees(wt)

    return p, a, e, i, raan, w, theta, u, lt, wt

def simulateMoliniya(TLE_1, TLE_2, n_orbits, t_step, T, coe = False, mu = 398600.441):

    # obtengo fecha inicial sobre la cual luego iterar
    sat = twoline2rv(TLE_1, TLE_2, wgs84)
    yr = sat.epochyr
    initial_date0 = sat_epoch_datetime(sat)
    initial_date = initial_date0.replace(year=yr)
    totsec = n_orbits*T
    N = int(totsec/t_step)                                     
    X = np.zeros([3,N])
    mean_list = []                
    for k in range(N):
        r, v = sat.propagate(initial_date.year, initial_date.month, initial_date.day, initial_date.hour, initial_date.minute, initial_date.second + k*t_step)
        X[:,k] = r
    
    if coe:
        e_list = []
        i_list = []
        w_list = []
        T_list = []
        
        for n in range(N):
            t = n*t_step
            r, v = sat.propagate(initial_date.year, initial_date.month, initial_date.day, initial_date.hour, initial_date.minute, initial_date.second + t)
            p, a, e, i, raan, w, theta, u, lt, wt = RV2COE(r, v)
            T = 2*np.pi*np.sqrt((a**3)/mu)
            e_list.append(e)
            i_list.append(i)
            w_list.append(w)
            T_list.append(T)

        R = 6378.1   
        fig = plt.figure()
        fig.suptitle("3D System")
        plt.style.use('dark_background')
        ax = plt.axes(projection ='3d')
        u2, v = np.mgrid[0:2*np.pi:100j, 0:np.pi:100j]

        x = R*np.cos(u2)*np.sin(v)
        y = R*np.sin(u2)*np.sin(v)
        z = R*np.cos(v)

        ax.plot_wireframe(x, y, z, color='b')
        ax.scatter(X[0],X[1],X[2], c=X[0]+X[1]+X[2], s=10)

        ax.set_xlabel('X [km]')
        ax.set_ylabel('Y [km]')
        ax.set_zlabel('Z [km]')

        minval = X[0:3].min()
        maxval = X[0:3].max()

        ax.set_xlim3d(minval, maxval)
        ax.set_ylim3d(minval, maxval)
        ax.set_zlim3d(minval, maxval)
    
        plt.show()
        e_mean = np.mean(np.array(e_list))
        i_mean = np.mean(np.array(i_list))
        w_mean = np.mean(np.array(w_list))
        T_mean = np.mean(np.array(T_list))
        mean_list = [i_mean, w_mean, T_mean, e_mean]
    
    return X, mean_list

T = 587.5*60 # período Moliniya 2-9 https://www.n2yo.com/satellite/?s=7276
TLE_1 = ('1 07276U 74026A   22100.26558472  .00000035  00000+0  00000+0 0  9991')
TLE_2 = ('2 07276  64.2670 236.7733 6515387 282.2063  16.3032  2.45095939247541')
X, mean_list = simulateMoliniya(TLE_1, TLE_2, 5, 60, T, True)
print('Inclinacion media', mean_list[0])
print('Argumento de periapsis medio', mean_list[1])
print('Período medio', mean_list[2])
print('Excentricidad media', mean_list[3])