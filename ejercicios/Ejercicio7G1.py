"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def a_e_2_EN_h(a, e, mu = 3.986e14, r_periapsis = None):
    """
    Semi-major axis and eccentricity to energy and angular momentum

    Args:
        a (float / string): semi-major axis [m] or 'Inf' for parabolas
        
        e (float): eccentricity [-]
        
        mu (float): central body gravitational parameter [m^3/s^2]
        
        r_periapsis (float): radius of periapsis [m] (add for parabolic case)
    
    Outputs:
        EN (float): orbit energy [m^2/s^2]
        
        h (float): angular momentum [m^2/s]
    
    Examples
        1. CIRCULAR
            a = 1.2756e7 m
            e = 0
            >> EN = -1.5624e7 m^2/s^2
            >> h = 7.1306e10 m^2/s 

        2. ELIPTICAL
            a = 3.189e7 m
            e = 0.6
            >> EN = -6.2496e6 m^2/s^2
            >> h = 9.01957e10 m^2/s
        
        3. HYPERBOLIC
            a = - 3.189e6 m
            e = 5
            >> EN = 6.2496e7 m^2/s^2
            >> h = 1.7466e11 m^2/s
        
        3. PARABOLIC
            a = 'Inf'
            e = 1
            r_periapsis = 1.2756e7 m
            >> EN = 0 m^2/s^2
            >> h = 1.0084e11 m^2/s
    """
    if type(a) == float or type(a) == int:  # circular / elliptical / hyperbolic
        EN = -mu/(2*a)
        p = a*(1-e**2)
    else:                                   # parabolic case
        EN = 0
        e = 1
        p = 2*r_periapsis
    h = np.sqrt(p*mu)
    return EN, h

def EN_h_2_a_e(EN, h, mu = 3.986e14, r_periapsis = None):
    """
    Energy and angular momentum to semi-major axis and eccentricity

    Args:
        EN (float): orbit energy [m^2/s^2]
        
        h (float): angular momentum [m^2/s]
        
        mu (float): central body gravitational parameter [m^3/s^2]
        
        r_periapsis (float): radius of periapsis [m] (add for parabolic case)
    
    Outputs:
        a (float / string): semi-major axis [m] or 'Inf' for parabolas
        
        e (float): eccentricity [-]
    
    Examples
        1. CIRCULAR
            EN = -1.5624e7 m^2/s^2
            h = 7.1306e10 m^2/s
            >> a = 1.2756e7 m
            >> e = 0

        2. ELIPTICAL
            EN = -6.2496e6 m^2/s^2
            h = 9.01957e10 m^2/s
            >> a = 3.189e7 m
            >> e = 0.6
        
        3. HYPERBOLIC
            EN = 6.2496e7 m^2/s^2
            h = 1.7466e11 m^2/s
            >> a = - 3.189e6 m
            >> e = 5
        
        3. PARABOLIC
            EN = 0 m^2/s^2
            h = 1.0084e11 m^2/s
            r_periapsis = 1.2756e7 m
            >> a = 'Inf'
            >> e = 1
    """
    if EN == 0:                              # parabolic case
        a = 'Inf'
        p = 2*r_periapsis
        e = 1
    else:                                   # circular / elliptical / hyperbolic
        a = -mu/(2*EN)
        p = h**2/mu
        e = np.sqrt(1-(p/a))
    return a, e