"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def KepEqtnH(M, e, abstol = 1e-8, itlim = 1e8):
    """
    Kepler equation to convert mean anomaly and eccentricity to hyperbolic anomaly
    
    Args:
        M (float): mean anomaly [deg] -180 < M < 180 (aprox)
        
        e (float): eccentricity [-]

        abstol (float): maximum tolerance for H value [deg]
        
        itlim (int): maximum iterations
    
    Outputs:
        H (float): hyperbolic anomaly [deg] -180 < H < 180 (aprox)
        
        convergence (int): 1 if calculation converged within itlim else 0
    """
    M = np.radians(M)
    if abstol != 1e-8:
        abstol = abstol*np.pi/180
    if e < 1.6:
        if -np.pi<M<0 or M>np.pi:
            H = M - e
        else:
            H = M + e
    else:
        if e<3.6 and np.absolute(M)>np.pi:
            H = M - np.sign(M)*e
        else:
            H = M/(e-1)
    H_ = H + 2*abstol
    i = 0
    convergence = 1
    while abs(H-H_) > abstol and i < itlim:
        H_ = H; i += 1
        if i >= itlim:
            print('Calulation stopped, iteration limit reached')
            convergence = 0
            break
        H = H_ + (M + H_ - e*np.sinh(H_)) / (e*np.cosh(H_)-1)
    H = np.degrees(H)
    return H, convergence

if __name__ == "__main__":
    print('----------------------------------------------------------------------------')
    print('This is an example to verify the functionality of the script.')
    print('Given M = 235.4 deg and e = 2.4, hyperbolic anomaly (H) should be 91.75 deg.')
    print('Let\'s see,')
    H, convergence = KepEqtnH(235.4, 2.4)
    print('>>> hyperbolic anomaly (H): ', H)
    print('----------------------------------------------------------------------------')