"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def E_B_H_2_theta(e, E = None, B = None, H = None, p = None, r = None):
    '''
    Eccentric anomaly or parabolic anomaly or hyperbolic anomaly to true anomaly
    
    Args:
        e (float): eccentricity [-]

        E (float): eccentric anomaly [deg] 0 < E < 360

        B (float): parabolic anomaly [deg] -180 < E < 180

        H (float): hyperbolic anomaly [deg] -180 < H < 180 (aprox)

        p (float): semilatus rectum [m] (add for parabolic case)

        r (float): ratio [m] (add for parabolic case)

    Outputs:
        theta (float): true anomaly [deg]
            - eliptical 0 < theta < 360
            - parabolic -180 < theta < 180
            - hyperbolic -180 < theta < 180 (aprox)
    '''
    if e < 1.0:
        E = E*np.pi/180
        sin_theta = (np.sin(E)*np.sqrt(1-e**2))/(1-e*np.cos(E))
        cos_theta = (np.cos(E)-e)/(1-e*np.cos(E))
        theta = np.arctan2(sin_theta, cos_theta)
        if theta < 0:
            theta += 2*np.pi
    if e == 1:
        B = B*np.pi/180
        sin_theta = (p*B/r)
        cos_theta = ((p-r)/r)
        theta = np.arctan2(sin_theta, cos_theta)
    if e > 1.0:
        H = H*np.pi/180
        sin_theta = (-np.sinh(H)*np.sqrt(e**2-1))/(1-e*np.cosh(H))
        cos_theta = (np.cosh(H)-e)/(1-e*np.cosh(H))
        theta = np.arctan2(sin_theta, cos_theta)
    theta = theta*180/np.pi
    return theta

if __name__ == "__main__":
    print('-----------------------------------------------------------------------------------')
    print('This is an example to verify the functionality of the script.')
    print('Given E = 310.64 deg and e = 0.7411, true anomaly for eliptical orbit should be 260.03 deg.')
    print('Given B = 57.3 deg, p = 6.3756e7 m, r = 6.3756e7 m and e = 1, true anomaly for parabolic orbit should be 90 deg.')
    print('Given H = -138.12 deg and e = 1.3837, true anomaly for hyperbolic orbit should be -128.69 deg.')
    print('Let\'s see,')
    theta_e = E_B_H_2_theta(0.7411, E = 310.64)
    theta_b = E_B_H_2_theta(1, B = 57.3, p = 6.3756e7, r = 6.3756e7)
    theta_h = E_B_H_2_theta(1.3837, H = -138.12)
    print('>>> eliptical true anomaly  (theta): ', theta_e)
    print('>>> parabolic true anomaly  (theta): ', theta_b)
    print('>>> hyperbolic true anomaly  (theta): ', theta_h)
    print('-----------------------------------------------------------------------------------')