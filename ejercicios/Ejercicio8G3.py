"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def ROT1(a, positive = True): # por default crece positivamente
    if positive:
        ROT1 = np.array([[1, 0, 0], [0, np.cos(a), np.sin(a)], [0, -np.sin(a), np.cos(a)]])
    else:
        ROT1 = np.array([[1, 0, 0], [0, np.cos(a), -np.sin(a)], [0, np.sin(a), np.cos(a)]])
    return ROT1

def ROT2(a, positive = True): # por default crece positivamente
    if positive:
        ROT2 = np.array([[np.cos(a), 0, -np.sin(a)], [0, 1, 0], [np.sin(a), 0, np.cos(a)]])
    else:
        ROT2 = np.array([[np.cos(a), 0, np.sin(a)], [0, 1, 0], [-np.sin(a), 0, np.cos(a)]])
    return ROT2

def ROT3(a, positive = True): # por default crece positivamente
    if positive:
        ROT3 = np.array([[np.cos(a), np.sin(a), 0], [-np.sin(a), np.cos(a), 0], [0, 0, 1]])
    else:
        ROT3 = np.array([[np.cos(a), -np.sin(a), 0], [np.sin(a), np.cos(a), 0], [0, 0, 1]])
    return ROT3

def COE_2_RV(p, e, i, o = None, w = None, theta = None, u = None, lt = None, wt = None, mu = 3.986e14, in_angle = 'Rad'):

    if in_angle != 'Rad':
        i = i*np.pi/180
        if o != None:
            o = o*np.pi/180
        if w != None:
            w = w*np.pi/180
        if theta != None:
            theta = theta*np.pi/180
        if u != None:
            u = u*np.pi/180
        if lt != None:
            lt = lt*np.pi/180
        if wt != None:
            wt = wt*np.pi/180
    if i == 0.0 and e == 0.0:
        o = 0.0
        w = 0.0
        theta = lt
    if i != 0.0 and e == 0.0:
        w = 0.0
        theta = u
    if i == 0.0 and e < 1:
        o = 0.0
        w = wt

    r_vector_PQW_i = (p*np.cos(theta))/(1+e*np.cos(theta))
    r_vector_PQW_j = (p*np.sin(theta))/(1+e*np.cos(theta))
    r_vector_PQW = np.array([[r_vector_PQW_i], [r_vector_PQW_j], [0]])

    v_vector_PQW_i = -np.sqrt(mu/p)*np.sin(theta)
    v_vector_PQW_j = np.sqrt(mu/p)*(e+np.cos(theta))
    v_vector_PQW = np.array([[v_vector_PQW_i], [v_vector_PQW_j], [0]])
    PQW2ECI = np.dot(np.dot(ROT3(o, False), ROT1(i, False)),ROT3(w, False))
    r_vector_ECI = np.dot(PQW2ECI, r_vector_PQW)
    v_vector_ECI = np.dot(PQW2ECI, v_vector_PQW)

    return r_vector_ECI, v_vector_ECI

def objectISS(robject_RSW):                             # robject_RSW = [10, 50, -3] m
    
    a = 6795100
    e = 0.0005552
    i = 51.6438
    o = 197.3862
    w = 149.0408
    theta = 120.0
    p = a*(1-e**2)
    rISS_ECI, vISS_ECI = COE2RV(p, e, i, o, w, theta, in_angle = 'Deg')
    
    RSW2PQW = ROT3(theta, False)
    robject_PQW = np.dot(RSW2PQW, robject_RSW)
    print(robject_PQW)
    PQW2ECI = np.dot(np.dot(ROT3(o, False), ROT1(i, False)),ROT3(w, False))
    robject_ECI = np.dot(PQW2ECI, robject_PQW) + rISS_ECI

    return robject_ECI

def r_RSW2r_ECI(r_objeto_RSW, a, e, i, raan, w, theta):                             
    p = a*(1-e**2)
    r_site_ECI, v_site_ECI = COE2RV(p, e, i, raan, w, theta, in_angle = 'Deg')    
    RSW2PQW = ROT3(theta, False)
    r_objeto_PQW = np.dot(RSW2PQW, r_objeto_RSW)
    PQW2ECI = np.dot(np.dot(ROT3(raan, False), ROT1(i, False)),ROT3(w, False))
    r_objeto_ECI = np.dot(PQW2ECI, r_objeto_PQW) + r_site_ECI
    return r_objeto_ECI