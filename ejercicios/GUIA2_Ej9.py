"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np
import matplotlib.pyplot as plt

plt.figure(figsize=(10, 10), dpi=80)                                 
plt.subplot(1, 1, 1)
plt.grid()
plt.suptitle("Mean Anomaly vs. True Anomaly")

plt.xlim(-np.pi, np.pi)
plt.ylim(-np.pi, np.pi)

plt.xticks([-np.pi, -5*np.pi/6, -2*np.pi/3, -np.pi/2, -np.pi/3, -np.pi/6, 0, np.pi/6, np.pi/3, np.pi/2, 2*np.pi/3, 5*np.pi/6, np.pi],
           [r'$-180°$', r'$-150°$', r'$-120°$', r'$-90°$', r'$-60°$', r'$-30°$', r'$0°$', r'$30°$', r'$60°$', r'$90°$', r'$120°$', r'$150°$', r'$180°$'])
plt.yticks([-np.pi, -5*np.pi/6, -2*np.pi/3, -np.pi/2, -np.pi/3, -np.pi/6, 0, np.pi/6, np.pi/3, np.pi/2, 2*np.pi/3, 5*np.pi/6, np.pi],
           [r'$-180°$', r'$-150°$', r'$-120°$', r'$-90°$', r'$-60°$', r'$-30°$', r'$0°$', r'$30°$', r'$60°$', r'$90°$', r'$120°$', r'$150°$', r'$180°$'])

plt.xlabel("Eccentric Anomaly")
plt.ylabel("Mean Anomaly")

th = np.linspace(-np.pi, np.pi, 400)

i = 0
e = 0

while i < 10:
    E = np.arctan(np.tan(th/2)*np.sqrt((1 - e)/(1 + e)))*2
    M = E - e*np.sin(E)
    
    f = "{:.2f}".format(e)
    plt.plot(th, M, linewidth=1.0, linestyle="-", label = f)
    plt.legend(loc='best')
    e += 0.1
    i += 1

plt.show()