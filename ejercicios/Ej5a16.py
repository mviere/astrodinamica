"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def ROT1(a, positive = True): # por default crece positivamente
    if positive:
        ROT1 = np.array([[1, 0, 0], [0, np.cos(a), np.sin(a)], [0, -np.sin(a), np.cos(a)]])
    else:
        ROT1 = np.array([[1, 0, 0], [0, np.cos(a), -np.sin(a)], [0, np.sin(a), np.cos(a)]])
    return ROT1

def ROT2(a, positive = True): # por default crece positivamente
    if positive:
        ROT2 = np.array([[np.cos(a), 0, -np.sin(a)], [0, 1, 0], [np.sin(a), 0, np.cos(a)]])
    else:
        ROT2 = np.array([[np.cos(a), 0, np.sin(a)], [0, 1, 0], [-np.sin(a), 0, np.cos(a)]])
    return ROT2

def ROT3(a, positive = True): # por default crece positivamente
    if positive:
        ROT3 = np.array([[np.cos(a), np.sin(a), 0], [-np.sin(a), np.cos(a), 0], [0, 0, 1]])
    else:
        ROT3 = np.array([[np.cos(a), -np.sin(a), 0], [np.sin(a), np.cos(a), 0], [0, 0, 1]])
    return ROT3

def RV2COE(r_ECI, v_ECI, mu = 398600.441, out_angle = 'Deg'):
    '''
    Position vector (r_ECI) and velocity vector (v_ECI), both in ECI,
    to orbital elements (a, e, i, raan, w, theta, u, lt, wt) and semilatus rectum (p).
    Algorithm 9 Vallado.

    Args:
        r_ECI (vector): position vector [km]

        v_ECI (vector): velocity vector [km/s]

        mu (float): central body gravitational parameter [km^3/s^2]

        out_angle (string): angle unit of i, raan, w, theta, u, lt and wt, 'Rad' or 'Deg'

    Outputs:
        p (float): semilatus rectum [km]

        a (float): semi-major axis [m] or 'Inf' for parabolas

        e (float): eccentricity [-]

        i (float): inclination [deg]

        raan (float): right ascension of ascending node [deg]

        w (float): argument of perigee [deg]

        theta (float): true anomaly [deg]

        u (float): argument of latitud [deg]

        lt (float): true longitud [deg]

        wt (float): true longitud of periapsis [deg]
    '''
    r = np.linalg.norm(r_ECI)
    v = np.linalg.norm(v_ECI)

    h_vector = np.cross(r_ECI, v_ECI)
    h = np.linalg.norm(h_vector)
    
    n_vector = np.cross([0,0,1], h_vector)
    n = np.linalg.norm(n_vector)
    
    e_vector =(np.multiply((v**2-mu/r),r_ECI)-np.multiply(np.dot(r_ECI, v_ECI),v_ECI))/mu
    e = np.linalg.norm(e_vector)
    
    EN = v**2/2 - mu/r

    if e != 1.0:
        a = -mu/(2*EN)
        p = a*(1-e**2)
    else:
        p = h**2/mu
        a = 'Inf'
    
    i = np.arccos(h_vector[2]/h)
    raan = np.arccos(n_vector[0]/n)
    
    if n_vector[1] < 0:
        raan = 2*np.pi - raan
    
    w = np.arccos(np.dot(n_vector, e_vector)/(n*e))
    if e_vector[2] < 0:
        w = 2*np.pi - w
    
    theta = np.arccos(np.dot(e_vector, r_ECI)/(e*r))
    if np.dot(r_ECI, v_ECI) < 0:
        theta = 2*np.pi - theta
    
    if i == 0 and e < 1.0:
        wt = np.arccos(e_vector[0]/e)
        if e_vector[1] < 0:
            wt = 2*np.pi - wt
    else:
        wt = None
    
    if i != 0 and e == 0:
        u = np.arccos(np.dot(n_vector, r_ECI)/(n*r))
        if r_ECI[2] < 0:
            u = 2*np.pi - u
    else:
        u = None
    
    if i == 0 and e == 0:
        lt = np.arccos(r_ECI[0]/r)
        if r_ECI[1] < 0:
            lt = 2*np.pi - lt
    else:
        lt = None
    
    if out_angle == 'Deg':
        i = np.degrees(i)
        raan = np.degrees(raan)
        w = np.degrees(w)
        theta = np.degrees(theta)
        if u != None:    
            u = np.degrees(u)
        if lt != None:
            lt = np.degrees(lt)
        if wt != None:
            wt = np.degrees(wt)

    return p, a, e, i, raan, w, theta, u, lt, wt

def COE2RV(p, e, i, raan = None, w = None, theta = None, u = None, lt = None, wt = None, mu = 398600.441, in_angle = 'Deg'):
    '''
    Some orbital elements (e, i, raan, w, theta, u, lt, wt) and semilatus rectum (p)
    to position vector (r_ECI) and velocity vector (v_ECI), both in ECI.
    Algorithm 10 Vallado.
    
    Args:
        p (float): semilatus rectum [km]

        e (float): eccentricity [-]

        i (float): inclination [deg]

        raan (float): right ascension of ascending node [deg]

        w (float): argument of perigee [deg]

        theta (float): true anomaly [deg]

        u (float): argument of latitud [deg]

        lt (float): true longitud [deg]

        wt (float): true longitud of periapsis [deg]

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of i, raan, w, theta, u, lt and wt, 'Rad' or 'Deg'

    Outputs:
        r_ECI (vector): position vector [km]

        v_ECI (vector): velocity vector [km/s]
    '''
    if in_angle == 'Deg':
        i = np.radians(i)
        if raan != None:
            raan = np.radians(raan)
        if w != None:
            w = np.radians(w)
        if theta != None:
            theta = np.radians(theta)
        if u != None:
            u = np.radians(u)
        if lt != None:
            lt = np.radians(lt)
        if wt != None:
            wt = np.radians(wt)
    if i == 0.0 and e == 0.0:
        raan = 0.0
        w = 0.0
        theta = lt
    if i != 0.0 and e == 0.0:
        w = 0.0
        theta = u
    if i == 0.0 and e < 1:
        raan = 0.0
        w = wt

    r_PQW_i = (p*np.cos(theta))/(1+e*np.cos(theta))
    r_PQW_j = (p*np.sin(theta))/(1+e*np.cos(theta))
    r_PQW = np.array([r_PQW_i, r_PQW_j, 0])

    v_PQW_i = -np.sqrt(mu/p)*np.sin(theta)
    v_PQW_j = np.sqrt(mu/p)*(e+np.cos(theta))
    v_PQW = np.array([v_PQW_i, v_PQW_j, 0])
    
    PQW2ECI = np.dot(np.dot(ROT3(raan, False), ROT1(i, False)),ROT3(w, False))
    r_ECI = np.dot(PQW2ECI, r_PQW)
    v_ECI = np.dot(PQW2ECI, v_PQW)

    return r_ECI, v_ECI

def SME(v, r, mu = 398600.441):
    '''
    Specific Mechanical Energy

    Args:
        v (float): velocity [km/s]

        r (float): ratio [km]
        
        mu (float): central body gravitational parameter [km^3/s^2]
    
    Outputs:
        EN (float): specific mechanical energy [km^2/s^2]
    '''
    EN = (v**2)/2 - mu/r # (Eq. 1-20 del Vallado con c=0)
    return EN

# Datos del problema
r_ECI = np.array([8000, 2000, 6000])
v_ECI = np.array([-4, 3, 4])

# Obtengo el momento angular con los vectores posición y velocidad. Esta ecuación vale para cualquier órbita.
h_vector = np.cross(r_ECI, v_ECI) # (Eq. 1-15 del Vallado)
h = np.linalg.norm(h_vector)
print('Momento angular:', h)

# Obtengo la energía específica de la órbita con los módulos de los vectores posición y velocidad.
r = np.linalg.norm(r_ECI)
v = np.linalg.norm(v_ECI)
EN = SME(v, r)
print('Energía específica:', EN)

# Obtengo el semi-eje mayor y la excentricidad, con ella, defino qué tipo de órbita es.
p, a, e, i, raan, w, theta, u, lt, wt = RV2COE(r_ECI, v_ECI)
if e == 0: 
    print('Orbita circular') # (Tabla 1-1 del Vallado)
if 0 < e < 1:
    print('Orbita eliptica') # (Tabla 1-1 del Vallado)
if e == 1:
    print('Orbita parabolica') # (Tabla 1-1 del Vallado)
if e > 1:
    print('Orbita hyperbolica') # (Tabla 1-1 del Vallado)
print('Semi-eje mayor:', a)
print('Excentricidad:', e)

# Con los elementos orbitales obtenidos y fijando theta = 0, obtengo el vector posición en el perigeo.
r_ECI_peri, v_ECI_peri = COE2RV(p, e, i, raan, w, 0, u, lt, wt)
print('Vector posición en perigeo:')
print(r_ECI_peri)

# Con los elementos orbitales obtenidos y fijando theta = 90, obtengo el vector posición en el semi lactus rectum.
r_ECI_semi, v_ECI_semi = COE2RV(p, e, i, raan, w, 90, u, lt, wt)
print('Vector posición en semi lactus rectum:')
print(r_ECI_semi)