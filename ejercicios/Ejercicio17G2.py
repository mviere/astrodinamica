"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np
def RV_2_COE(r_vector, v_vector, mu = 3.986e14, deg = False):
    '''
    State vectors to orbital elements.
    
    Args:
        r_vector (array of floats [1x3]): vector position [m]

        v_vector (array of floats [1x3]): velocity vector [m]

        mu (float): central body gravitational parameter [m^3/s^2]

    Outputs:
        p

        a

        e

        i

        o

        w

        theta

        u

        lt

        wt
    '''
    r = np.linalg.norm(r_vector)
    v = np.linalg.norm(v_vector)

    h_vector = np.cross(r_vector, v_vector)
    h = np.linalg.norm(h_vector)
    
    n_vector = np.cross([0,0,1], h_vector)
    n = np.linalg.norm(n_vector)
    
    e_vector =(np.multiply((v**2-mu/r),r_vector)-np.multiply(np.dot(r_vector, v_vector),v_vector))/mu
    e = np.linalg.norm(e_vector)
    
    EN = v**2/2 - mu/r

    if e != 1.0:
        a = -mu/(2*EN)
        p = a*(1-e**2)
    else:
        p = h**2/mu
        a = 'Inf'
    
    i = np.arccos(h_vector[2]/h)
    o = np.arccos(n_vector[0]/n)
    
    if n_vector[1] < 0:
        o = 2*np.pi - o
    
    w = np.arccos(np.dot(n_vector, e_vector)/(n*e))
    if e_vector[2] < 0:
        w = 2*np.pi - w
    
    theta = np.arccos(np.dot(e_vector, r_vector)/(e*r))
    if np.dot(r_vector, v_vector) < 0:
        theta = 2*np.pi - theta
    
    if i == 0 and e < 1.0:
        wt = np.arccos(e_vector[0]/e)
        if e_vector[1] < 0:
            wt = 2*np.pi - wt
    else:
        wt = None
    
    if i != 0 and e == 0:
        u = np.arccos(np.dot(n_vector, r_vector)/(n*r))
        if r_vector[2] < 0:
            u = 2*np.pi - u
    else:
        u = None
    
    if i == 0 and e == 0:
        lt = np.arccos(r_vector[0]/r)
        if r_vector[1] < 0:
            lt = 2*np.pi - lt
    else:
        lt = None
    
    if deg:
        i = i*180/np.pi
        o = o*180/np.pi
        w = w*180/np.pi
        theta = theta*180/np.pi
        if u != None:    
            u = u*180/np.pi
        if lt != None:
            lt = lt*180/np.pi
        if wt != None:
            wt = wt*180/np.pi

    return p, a, e, i, o, w, theta, u, lt, wt

if __name__ == "__main__":
    print('-------------------------------------------------------------')
    print('This is an example to verify the functionality of the script.')
    print('Given r_vector = [6524834, 6862875, 6448296] m')
    print('      v_vector = [4901.372, 5533.756, -1976.341] m/s')
    print('Orbital elements should be,')
    print('      p = 11067790 m')
    print('      a = 36127343 m')
    print('      e = 0.832853')
    print('      i = 87.870 deg')
    print('      o = 277.898 deg')
    print('      w = 53.38 deg')
    print('      theta = 92.335 deg')
    print('Let\'s see,')
    r_vector = np.array([6524834, 6862875, 6448296])
    v_vector = np.array([4901.372, 5533.756, -1976.341])
    p, a, e, i, o, w, theta, u, lt, wt = RV_2_COE(r_vector, v_vector, deg = True)
    print('      p = ', p)
    print('      a = ', a)
    print('      e = ', e)
    print('      i = ', i)
    print('      o = ', o)
    print('      w = ', w)
    print('      theta = ', theta)
    print('-------------------------------------------------------------')