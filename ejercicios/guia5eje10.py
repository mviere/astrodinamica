
import numpy as np
from datetime import datetime, timedelta
from astro import theta_gmst2UTC_deltaUT1, TM
import matplotlib.pyplot as plt


def RAZELcorto(r_sez):
    rho = np.linalg.norm(r_sez) #Km
    el = (np.arcsin(r_sez[2] / rho)) * 180 / np.pi #Grados
    if el != 90:
        sin_beta =  r_sez[1] / np.sqrt(r_sez[0]**2 + r_sez[1]**2)
        cos_beta = -r_sez[0] / np.sqrt(r_sez[0]**2 + r_sez[1]**2)
        beta = (np.arctan2(sin_beta, cos_beta)) * 180 / np.pi #Grados
    else:
        beta = "no se pudo calcular, falta la velocidad"
    return rho, el, beta

#DAtos de BA
phi_gd   = -(34 + 36 * 60) * np.pi / 180 #Rad
longitud = -(58 + 26 * 60) * np.pi / 180 #Rad
print(phi_gd)
print(longitud)
e_tierra = 0.08181922

#Datos de sirio
ascensión_recta = (6 * 3600 + 45 * 60 + 8.9173) * 4.848137E-6     #Rad
declinacion     = -(16 + 42 / 60 + 58.017 / 3600) * np.pi / 180   #Rad

#Pasamos los datos de sirio en GCRF a ECI
r_site = 1
r_delta = r_site * np.cos(declinacion)

r_i = r_delta * np.cos(ascensión_recta)
r_j = r_delta * np.sin(ascensión_recta)
r_k = r_site  * np.sin(declinacion)

r_ijk_sirio = [r_i, r_j, r_k]

#Ahora hay que pasar los datos de sirio en ECI a ECEF

deltaUT1 = -0.2
deltaAT  = 37
fecha    = datetime(2022, 2, 22, 0, 0, 0) #Fecha y rango de 1 día

el_array = []
minut_array = []

for minut in range (1440):
    fecha1 = fecha + timedelta(minutes = minut) #Sumo minutos al día
    #Calculo el theta gmst para luego usarlo para pasar de eci a ecef
    theta_gmst_deg = (theta_gmst2UTC_deltaUT1(fecha1, deltaUT1, deltaAT)) * np.pi / 180 #Rad
    TM_eci2ecef = TM(theta_gmst_deg, 3) #Matriz de rotación de ECI a ECEF
    r_ecef_sirio = np.dot(TM_eci2ecef, r_ijk_sirio) #Pasamos de ECI a ECEF
    TM_ecef2sez = np.transpose(np.dot(TM(-longitud, 3),TM(-((np.pi/2) - phi_gd), 2))) #Matriz de rotación de ECEF a SEZ
    r_sez_sirio = np.dot(TM_ecef2sez,r_ecef_sirio) #Pasamos de ECEF a SEZ
    r_sez_sirio_versor = r_sez_sirio / np.linalg.norm(r_sez_sirio)
    rho, el, beta = RAZELcorto(r_sez_sirio_versor) #Convertimos de sez a az-el-r
    el_array.append(el)
    minut_array.append(minut)
'''
plt.plot(minut_array,el_array)
plt.title("Elevación de Sirio desde Buenos Aires durante el día " + str(fecha))
plt.xlabel("Tiempo [minutos]")
plt.ylabel("Elevación [°]")
plt.grid(axis = 'y', color = 'gray', linestyle = 'dashed')
plt.grid(axis = 'x', color = 'gray', linestyle = 'dashed')
plt.ylim(-180,180); plt.xlim(0,1440)
legend=plt.legend(["Elevación de Sirio [°]"]); legend.get_frame().set_facecolor('grey') #Leyenda
plt.show()'''
