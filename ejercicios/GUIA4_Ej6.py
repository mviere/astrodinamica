"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def deg2rad(a):

    """
    Conversion from degrees to radians
    
    Args:
        a (float): angle [deg]
    
    Outputs:
        a (float): angle [rad]
        
    """
    
    a = a*np.pi/180
    return a

def rad2deg(a):

    """
    Conversion from radians to degrees
    
    Args:
        a (float): angle [rad]
    
    Outputs:
        a (float): angle [deg]
        
    """

    a = a*180/np.pi
    return a

def Phasing_same(a_tgt, th, k_tgt, k_int, mu = 398600.441):

    """
    Algorithm for circular phasing maneuvers (same orbits)

    Args:
        a_tgt (float): semimayor axis [km]

        th (float): phase angle [deg]

        k_tgt (int): revolutions on the target orbit [-]

        k_int (int): revolutions on the transfer orbit [-]

    Outputs:
        delta_v (float): total change in velocity [km/s]

        t_phase (float): phasing orbit period [s]

    """
    
    th = deg2rad(th)

    w_tgt = np.sqrt(mu/a_tgt**3)

    t_phase = (2*np.pi*k_tgt + th)/w_tgt
    a_phase = (mu*((t_phase/(2*np.pi*k_int))**2))**(1/3)

    delta_v = 2*abs(np.sqrt((2*mu/a_tgt) - mu/a_phase) - np.sqrt(mu/a_tgt))

    return delta_v, a_phase, t_phase

# Datos
a_tgt = 6378.1363 + 400
th1 = 180
th2 = 90
th3 = 270

delta_v1, a_phase1, t_phase1 = Phasing_same(a_tgt, th1, 1, 1)

delta_v2, a_phase2, t_phase2 = Phasing_same(a_tgt, th2, 1, 1)

delta_v3, a_phase3, t_phase3 = Phasing_same(a_tgt, th3, 1, 1)

print(delta_v1)
print(delta_v2)
print(delta_v3)
print(a_phase1)
print(a_phase2)
print(a_phase3)
print(t_phase1)
print(t_phase2)
print(t_phase3)