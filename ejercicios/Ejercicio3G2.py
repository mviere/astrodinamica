"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def KepEqtnP(dt, p, mu = 398600.441, out_angle = 'Rad'):
    '''
    Kepler equation to convert time of flight since periapsis (dt) to parabolic anomaly (B).
    
    Args:
        dt (float): time of flight since periapsis [s]
        
        p (float): semilatus rectum [km]

        mu (float): central body gravitational parameter [km^3/s^2]

        out_angle (string): angle unit of B, 'Rad' or 'Deg'
    
    Outputs:
        B (float): parabolic anomaly [rad]
    '''
    s = (np.arctan(1/(3*(np.sqrt(mu/(p**3)))*dt)))/2
    w = np.arctan(np.cbrt(np.tan(s)))
    B = 2*(1/np.tan(2*w))
    if out_angle != 'Rad':
        B = np.degrees(B)
    return B

if __name__ == "__main__":
    print('-----------------------------------------------------------------------------------')
    print('This is an example to verify the functionality of the script.')
    print('Given dt = 3227.244 s and p = 25512 km, parabolic anomaly (B) should be 46.85 deg.')
    print('Let\'s see,')
    B = KepEqtnP(3227.244, 25512, out_angle = 'Deg')
    print('>>> parabolic anomaly (B): ', B)
    print('-----------------------------------------------------------------------------------')