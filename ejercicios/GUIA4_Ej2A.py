"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def deg2rad(a):

    """
    Conversion from degrees to radians
    
    Args:
        a (float): angle [deg]
    
    Outputs:
        a (float): angle [rad]
        
    """
    
    a = a*np.pi/180
    return a

def rad2deg(a):

    """
    Conversion from radians to degrees
    
    Args:
        a (float): angle [rad]
    
    Outputs:
        a (float): angle [deg]
        
    """

    a = a*180/np.pi
    return a

def Hohmann(r_initial, r_final, mu = 398600.441 ):

    """
    Algorithm for the Hohmann transfer (circular case). Given the initial and final radii, it returns the total 
    change in velocity and the time of flight for the transfer.

    Args:
        r_initial (float): initial radius [km]

        r_final (float): final radius [km]

    Outputs:
        dv (float): total change in velocity [km/s]

        TOF (float): time of flight [s]

    """

    a_trans = (r_initial + r_final)/2

    v_initial = np.sqrt(mu/r_initial)
    v_final = np.sqrt(mu/r_final)

    v_trans_a = np.sqrt((2*mu/r_initial) - mu/a_trans)
    v_trans_b = np.sqrt((2*mu/r_final) - mu/a_trans)

    delta_va = v_trans_a - v_initial
    delta_vb = v_final- v_trans_b

    delta_v = abs(delta_va) + abs(delta_vb)

    T_trans = np.pi*np.sqrt((a_trans**3)/mu)

    return delta_v, T_trans

def Bi_elliptic(r_initial, r_b, r_final, mu = 398600.441 ):

    """
    Algorithm for the Bi-elliptical transfer (circular case). Given the initial, intermediate and final radii, 
    it returns the total change in velocity and the time of flight for the transfer.

    Args:
        r_initial (float): initial radius [km]

        r_b (float): intermediate radius [km]

        r_final (float): final radius [km]

    Outputs:
        dv (float): total change in velocity [km/s]

        TOF (float): time of flight [s]

    """

    a_trans1 = (r_initial + r_b)/2
    a_trans2 = (r_b + r_final)/2

    v_initial = np.sqrt(mu/r_initial)
    v_final = np.sqrt(mu/r_final)

    v_trans1_a = np.sqrt((2*mu/r_initial) - mu/a_trans1)
    v_trans1_b = np.sqrt((2*mu/r_b) - mu/a_trans1)
    v_trans2_b = np.sqrt((2*mu/r_b) - mu/a_trans2)
    v_trans2_c = np.sqrt((2*mu/r_final) - mu/a_trans2)

    delta_va = v_trans1_a - v_initial
    delta_vb = v_trans2_b - v_trans1_b
    delta_vc = v_final - v_trans2_c

    delta_v = abs(delta_va) + abs(delta_vb) + abs(delta_vc)

    T_trans = np.pi*np.sqrt((a_trans1**3)/mu) + np.pi*np.sqrt((a_trans2**3)/mu)

    return delta_v, T_trans

def OneTangentBurn(r_initial, r_final, nu_trans_b, k = 0, Per = True, mu = 398600.441 ):

    """
    Algorithm for the One-Tangent Burn transfer (circular case). Given the initial and final radii plus, 
    it returns the total change in velocity and the time of flight for the transfer.

    Args:
        r_initial (float): initial radius [km]

        r_b (float): intermediate radius [km]

        r_final (float): final radius [km]

    Outputs:
        dv (float): total change in velocity [km/s]

        TOF (float): time of flight [s]

    """

    nu_trans_b = deg2rad(nu_trans_b)

    R_ = r_initial/r_final

    if Per == True:
        
        e_trans = (R_ - 1)/(np.cos(nu_trans_b) - R_)
        a_trans = r_initial/(1 - e_trans)

    else:
        e_trans = (R_ - 1)/(np.cos(nu_trans_b) + R_)
        a_trans = r_initial/(1 + e_trans)


    v_initial = np.sqrt(mu/r_initial)
    v_final = np.sqrt(mu/r_final)

    v_trans_a = np.sqrt((2*mu/r_initial) - mu/a_trans)
    v_trans_b = np.sqrt((2*mu/r_final) - mu/a_trans)

    delta_va = v_trans_a - v_initial

    phi_trans_b = np.arctan(e_trans*np.sin(nu_trans_b)/(1 + e_trans*np.cos(nu_trans_b)))

    delta_vb = np.sqrt(v_trans_b**2 + v_final**2 - 2*v_trans_b*v_final*np.cos(phi_trans_b))

    delta_v = abs(delta_va) + abs(delta_vb)

    E = np.arccos((e_trans + np.cos(nu_trans_b))/(1 + e_trans*np.cos(nu_trans_b)))

    if Per == True:
        
        T_trans = np.sqrt((a_trans**3)/mu)*(2*k*np.pi + (E - e_trans*np.sin(E)))

    else:
        
        T_trans = np.sqrt((a_trans**3)/mu)*(2*k*np.pi + (E - e_trans*np.sin(E)) - (np.pi - e_trans*np.sin(np.pi)))

    return delta_v, T_trans
    

# Hohmann
'''r_initial = 1.496e8
nu_trans_b = 175
r_final = 2.27987e8
mu = 1.32712440018e11

delta_v, T_trans = OneTangentBurn(r_initial, r_final, nu_trans_b, k = 0, Per = True, mu = 1.32712440018e11)

print('delta v', delta_v)
print('t trans', T_trans)'''

def whatever():
    mu = 1.32712440018e11
    r_initial = 1.496e8
    r_final = 2.27987e8
    T_earth = 2*np.pi*np.sqrt((r_initial**3)/mu)
    T_mars = 2*np.pi*np.sqrt((r_final**3)/mu)
    w_mars = 2*np.pi/T_mars

    th = rad2deg(w_mars*T_trans) - 180

    print('thita', th)

    t_wait = 2*np.pi/abs(2*np.pi/T_mars - 2*np.pi/T_earth)

    print('twait', t_wait)

    return th, t_wait



