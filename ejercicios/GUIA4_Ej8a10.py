"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def deg2rad(a):

    """
    Conversion from degrees to radians
    
    Args:
        a (float): angle [deg]
    
    Outputs:
        a (float): angle [rad]
        
    """
    
    a = a*np.pi/180
    return a

def rad2deg(a):

    """
    Conversion from radians to degrees
    
    Args:
        a (float): angle [rad]
    
    Outputs:
        a (float): angle [deg]
        
    """

    a = a*180/np.pi
    return a


def Inclination_only(delta_i, a, mu = 398600.441):

    """
    Algorithm for the inclination-only maneuvers (circular case)

    Args:
        delta_i (float): inclination change [deg]

        a (float): semimayor axis [km]

    Outputs:
        delta_v (float): total change in velocity [km/s]

    """

    v_initial = np.sqrt(mu/a)

    delta_v = 2*v_initial*np.sin(deg2rad(delta_i)/2)

    return delta_v

def raan_only(delta_raan, i_initial, a, mu = 398600.441):

    """
    Algorithm for changes in the Right Ascension of the Ascending Node (circular case)

    Args:
        delta_raan (float): ascending node change [deg]

        i_initial (float): orbit inclination [deg]

        a (float): semimayor axis [km]

    Outputs:
        delta_v (float): total change in velocity [km/s]

    """

    v_initial = np.sqrt(mu/a)

    th = np.arccos((np.cos(deg2rad(i_initial))**2) + (np.sin(deg2rad(i_initial))**2)*np.cos(deg2rad(delta_raan)))

    delta_v = 2*v_initial*np.sin(th/2)

    return delta_v, rad2deg(th)

def i_raan_change(i_initial, i_final, delta_raan, a, mu = 398600.441):

    """
    Algorithm for combined changes in Ascending Node and Inclination (circular case)

    Args:
        i_initial (float): initial inclination [deg]

        i_final (float): final inclination [deg]

        delta_raan (float): ascending node change [deg]

        a (float): semimayor axis [km]

    Outputs:
        delta_v (float): total change in velocity [km/s]

    """

    v_initial = np.sqrt(mu/a)

    th = np.arccos(np.cos(deg2rad(i_initial))*np.cos(deg2rad(i_final)) + np.sin(deg2rad(i_initial))*np.sin(deg2rad(i_final))*np.cos(deg2rad(delta_raan)))

    delta_v = 2*v_initial*np.sin(th/2)

    return delta_v, rad2deg(th)


# Transferencia combinada

a = 6904
i_initial = 98
i_final = 97.5
delta_raan = 0.4
delta_i = 0.5

delta_v0, th0 = i_raan_change(i_initial, i_final, delta_raan, a)

print(th0)

print(delta_v0)

u_initial = np.arccos((np.sin(i_final*np.pi/180)*np.cos(delta_raan*np.pi/180)-np.cos(th0*np.pi/180)*np.sin(i_initial*np.pi/180))/(np.sin(th0*np.pi/180)*np.cos(i_initial*np.pi/180)))*180/np.pi
u_final = np.arccos((np.cos(i_initial*np.pi/180)*np.sin(i_final*np.pi/180)-np.sin(i_initial*np.pi/180)*np.cos(i_final*np.pi/180)*np.cos(delta_raan*np.pi/180))/np.sin(th0*np.pi/180))*180/np.pi
w = abs(u_final - u_initial)
print('u_initial', u_initial)
print('u_final', u_final)
print('w', w)
# Primero inclinacion luego raan

delta_v1 = Inclination_only(delta_i, a)

delta_v2, th = raan_only(delta_raan, i_final, a)

delta_vt = delta_v1 + delta_v2

print(delta_vt)
u_initial = np.arccos((np.sin(i_final*np.pi/180)*np.cos(delta_raan*np.pi/180)-np.cos(th*np.pi/180)*np.sin(i_initial*np.pi/180))/(np.sin(th0*np.pi/180)*np.cos(i_initial*np.pi/180)))*180/np.pi

print('u_initial_primeroi', u_initial)
# Primero raan luego inclinacion

delta_v3, th = raan_only(delta_raan, i_initial, a)

delta_v4 = Inclination_only(delta_i, a)

delta_vt2 = delta_v3 + delta_v4

print(delta_vt2)
u_initial = np.arccos((np.sin(i_final*np.pi/180)*np.cos(delta_raan*np.pi/180)-np.cos(th*np.pi/180)*np.sin(i_initial*np.pi/180))/(np.sin(th0*np.pi/180)*np.cos(i_initial*np.pi/180)))*180/np.pi

print('u_initial_primeroRAAN', u_initial)