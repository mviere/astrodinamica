"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def Hohmann(radioi, radiof, mu = 398600.4418):
    '''
    Devuelve el delta de velocidad (delta_v) total y el tiempo de vuelo (t_trans) en una transferencia de Hohmann
    entre dos orbitas circulares con radio inicial (radioi) y radio final (radiof).
    Algorithm 36 Vallado.

    Args:
        radioi (float): initial radius [km]

        radiof (float): final radius [km]

        mu (float): central body gravitational parameter [km^3/s^2]

    Outputs:
        delta_v (float): total change in velocity [km/s]

        t_trans (float): time on trasfer orbit [s]
    '''
    a_trans = (radioi+radiof)/2
    
    vi = np.sqrt(mu/radioi)
    v_transa = np.sqrt((2*mu/radioi)-mu/a_trans)
    
    vf = np.sqrt(mu/radiof)
    v_transb = np.sqrt((2*mu/radiof)-mu/a_trans)

    delta_va = v_transa-vi
    delta_vb = vf-v_transb
    delta_v = abs(delta_va)+abs(delta_vb)
    
    t_trans = np.pi*np.sqrt((a_trans**3)/mu)
    
    return delta_v, t_trans

def InclinationOnly(delta_i, e, a = None, p = None, theta = None, mu = 398600.4418, in_angle = 'Deg'):
    '''
    Devuelve el delta de velocidad (delta_v) en la maniobra Inclination Only entre orbitas circulares o elipticas.
    Algorithm 39 Vallado + análisis de menor delta_v.

    Args:
        delta_i (float): inclination change [deg]

        e (float): eccentricity [-]

        a (float): semimayor axis [km] (obligatory for circular case. for eliptical case, enter a instead of p)

        p (float): semilatus rectum [km] (for eliptical case, enter a instead of p)

        theta (float): true anomaly at node [deg] (for eliptical case)

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of delta_i and theta, 'Rad' or 'Deg'

    Outputs:
        delta_v (float): total change in velocity [km/s]
    '''
    if in_angle == 'Deg' and theta != None:
        theta = np.radians(theta)
    
    if in_angle == 'Deg':
        delta_i = np.radians(delta_i)

    if e == 0.0:
        vi = np.sqrt(mu/a)
        delta_v = 2*vi*np.sin(delta_i/2)
    else:
        if p == None and a != None:
            p = a*(1-e**2)
        if p != None and a == None:
            a = p/(1-e**2)
        
        r1 = p/(1+e*np.cos(theta))
        vi1 = np.sqrt((2*mu/r1)-mu/a)
        phi_fpa1 = np.arctan(e*np.sin(theta)/(1+e*np.cos(theta)))
        delta_v1 = 2*np.cos(phi_fpa1)*vi1*np.sin(delta_i/2)
        
        r2 = p/(1+e*np.cos(theta-np.pi))
        vi2 = np.sqrt((2*mu/r2)-mu/a)
        phi_fpa2 = np.arctan(e*np.sin(theta-np.pi)/(1+e*np.cos(theta-np.pi)))
        delta_v2 = 2*np.cos(phi_fpa2)*vi2*np.sin(delta_i/2)

        if delta_v1 <= delta_v2:
            delta_v = delta_v1
        else:
            delta_v = delta_v2
    delta_v = abs(delta_v)
    return delta_v

def SMACombinedPlaneChange(i_initial, i_final, r_initial, r_final, mu = 398600.4418, in_angle = 'Deg', out_angle = 'Deg'):
    '''
    Primero SMA y después maniobra combinada: transferencia de Hohmann entre orbitas circulares con un de inclinacion (puntos final).
    
    Args:
        i_initial (float): initial inclination [deg]

        i_final (float): final inclination [deg]

        r_initial (float): initial orbit ratio [km]

        r_final (float): final orbit ratio [km]

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of i_initial and i_final, 'Rad' or 'Deg'

        out_angle (string): angle unit of delta_i_initial and delta_i_final, 'Rad' or 'Deg'

    Outputs:
        delta_i_initial (float): change in inclination at initial point [deg]
        
        delta_i_final (float): change in inclination at final point [deg]
        
        delta_v (float): total change in velocity [km/s]
    '''
    a_trans = (r_initial+r_final)/2 # (Eq. 6-3 del Vallado)
    v_initial = np.sqrt(mu/r_initial) # (Eq. dentro del Algoritm 36 del Vallado)
    v_final = np.sqrt(mu/r_final) # (Eq. dentro del Algoritm 36 del Vallado)
    v_trans_a = np.sqrt((2*mu/r_initial)-mu/a_trans) # (Eq. dentro del Algoritm 36 del Vallado)
    v_trans_b = np.sqrt((2*mu/r_final)-mu/a_trans) # (Eq. dentro del Algoritm 36 del Vallado)
    
    if in_angle == 'Deg':
        i_initial = np.radians(i_initial)
        i_final = np.radians(i_final)
    delta_i = i_final - i_initial
        
    delta_v_initial = v_trans_a - v_initial # (Eq. dentro del Algoritm 36 del Vallado)
    delta_v_final = np.sqrt((v_final)**2+(v_trans_b)**2-2*v_final*v_trans_b*np.cos(delta_i)) # (Eq. 6-31 del Vallado)
    delta_v = abs(delta_v_initial) + abs(delta_v_final) # (Eq. dentro del Algoritm 36 del Vallado)

    return delta_v

def MinimumCombinedPlaneChange(i_initial, i_final, r_initial, r_final, mu = 398600.4418, in_angle = 'Deg', out_angle = 'Deg'):
    '''
    Maniobra combinada: transferencia de Hohmann entre orbitas circulares con dos cambios de inclinacion (puntos inicial y final).
    Optimiza el cambio de velocidad (delta_v) total realizando dos cambios de inclinación (delta_i_initial, delta_i_final) en lugar de uno.
    Algorithm 42 Vallado.

    Args:
        i_initial (float): initial inclination [deg]

        i_final (float): final inclination [deg]

        r_initial (float): initial orbit ratio [km]

        r_final (float): final orbit ratio [km]

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of i_initial and i_final, 'Rad' or 'Deg'

        out_angle (string): angle unit of delta_i_initial and delta_i_final, 'Rad' or 'Deg'

    Outputs:
        delta_i_initial (float): change in inclination at initial point [deg]
        
        delta_i_final (float): change in inclination at final point [deg]
        
        delta_v (float): total change in velocity [km/s]
    '''
    a_trans = (r_initial+r_final)/2
    v_initial = np.sqrt(mu/r_initial)
    v_final = np.sqrt(mu/r_final)
    v_trans_a = np.sqrt((2*mu/r_initial)-mu/a_trans)
    v_trans_b = np.sqrt((2*mu/r_final)-mu/a_trans)
    
    if in_angle == 'Deg':
        i_initial = np.radians(i_initial)
        i_final = np.radians(i_final)
    delta_i = i_final - i_initial
    
    R = r_final/r_initial
    s = (1/delta_i)*np.arctan(np.sin(delta_i)/(R**(3/2)+np.cos(delta_i)))
    
    delta_i_initial = s*delta_i
    delta_i_final = (1-s)*delta_i
    
    delta_v_initial = np.sqrt((v_initial)**2+(v_trans_a)**2-2*v_initial*v_trans_a*np.cos(delta_i_initial))
    delta_v_final = np.sqrt((v_final)**2+(v_trans_b)**2-2*v_final*v_trans_b*np.cos(delta_i_final))
    delta_v = abs(delta_v_initial) + abs(delta_v_final)
    
    delta_i_initial = abs(delta_i_initial)
    delta_i_final = abs(delta_i_final)

    if out_angle == 'Deg':
        delta_i_initial = np.degrees(delta_i_initial)
        delta_i_final = np.degrees(delta_i_final)

    return delta_i_initial, delta_i_final, delta_v

# Datos de las órbitas inicial y final
radioi = 6378.137 + 280
radiof = 42164
i_initial = 28.5
i_final = 0
delta_i = i_final - i_initial

# Primera estrategia
'''Realizo una transferencia de Hohmann (primer y segundo impulso).
Luego, realizo todo el delta_i en una maniobra (tercer impulso).
Obtengo el delta de velocidad total de la estrategia.'''
delta_v11, t_trans = Hohmann(radioi, radiof)
a = radiof # El semi-eje mayor de la siguiente maniobra es el de la órbita GEO.
delta_v12 = InclinationOnly(delta_i, 0.0, a)
delta_v1 = delta_v11 + delta_v12
print('Delta_v primer estrategia:', delta_v1)

# Segunda estrategia
'''Inicio transferencia de Hohmann (primer impulso).
Luego, realizo todo el delta_i y termino transferencia (segundo impulso).
Obtengo el delta de velocidad total de la estrategia.'''
delta_v2 = SMACombinedPlaneChange(i_initial, i_final, radioi, radiof)
print('Delta_v segunda estrategia:', delta_v2)

# Tercer estrategia
'''Realizo parte del delta_i e inicio transferencia de Hohmann (primer impulso).
Luego, termino el delta_i y la transferencia (segundo impulso).
Obtengo el delta de velocidad total de la estrategia y los delta_i en cada impulso.'''
delta_i_initial, delta_i_final, delta_v3 = MinimumCombinedPlaneChange(i_initial, i_final, radioi, radiof)
print('Delta_v tercer estrategia:', delta_v3)
print('delta i en LEO:', delta_i_initial)
print('delta i en GEO:', delta_i_final)