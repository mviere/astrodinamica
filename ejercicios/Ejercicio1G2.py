"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def KepEqtnE(M, e, abstol = 1e-8, itlim = 1e8):
    """
    Kepler equation to convert mean anomaly M and eccentricity e to eccentric anomaly E.
    
    Args:
        M (float): mean anomaly [deg]
        
        e (float): eccentricity [-]

        abstol (float): maximum tolerance for E value [deg]
        
        itlim (int): maximum iterations
    
    Outputs:
        E (float): eccentric anomaly [deg]
        
        convergence (int): 1 if calculation converged within itlim else 0
    """
    M = np.radians(M)
    if abstol != 1e-8:
        abstol = np.radians(abstol)
    E = M - e if np.sin(M) < 0 else M + e
    E_ = E + 2*abstol
    i = 0
    convergence = 1
    while abs(E-E_) > abstol and i < itlim:
        E_ = E; i += 1
        if i >= itlim:
            print('Calulation stopped, iteration limit reached')
            convergence = 0
            break
        E = E_ + (M - E_ + e*np.sin(E_)) / (1 - e*np.cos(E_))
    E = np.degrees(E)
    return E, convergence

if __name__ == "__main__":
    print('----------------------------------------------------------------------------')
    print('This is an example to verify the functionality of the script.')
    print('Given M = 235.4 deg and e = 0.4, eccentric anomaly (E) should be 220.51 deg.')
    print('Let\'s see,')
    E, convergence = KepEqtnE(235.4, 0.4)
    print('>>> eccentric anomaly (E): ', E)
    print('----------------------------------------------------------------------------')