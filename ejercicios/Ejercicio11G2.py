"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np
from matplotlib import pyplot as plt
import random

def KepEqtnH(M, e, abstol = 1e-8, itlim = 1e8, in_angle = 'Rad', out_angle = 'Rad'):
    '''
    Kepler equation to convert mean anomaly (M) and eccentricity (e) to hyperbolic anomaly (H).
    
    Args:
        M (float): mean anomaly [rad] 
        
        e (float): eccentricity [-]

        abstol (float): maximum tolerance for H value [rad]
        
        itlim (int): maximum iterations

        in_angle (string): angle unit of M and abstol, 'Rad' or 'Deg'

        out_angle (string): angle unit of H, 'Rad' or 'Deg'
    
    Outputs:
        H (float): hyperbolic anomaly [rad]
        
        convergence (int): 1 if calculation converged within itlim else 0
    '''
    if in_angle != 'Rad':
        M = np.radians(M)
        if abstol != 1e-8:
            abstol = np.radians(abstol)
    if abstol != 1e-8:
        abstol = abstol*np.pi/180
    if e < 1.6:
        if -np.pi<M<0 or M>np.pi:
            H = M - e
        else:
            H = M + e
    else:
        if e<3.6 and np.absolute(M)>np.pi:
            H = M - np.sign(M)*e
        else:
            H = M/(e-1)
    H_ = H + 2*abstol
    i = 0
    convergence = 1
    while abs(H-H_) > abstol and i < itlim:
        H_ = H; i += 1
        if i >= itlim:
            print('Calulation stopped, iteration limit reached')
            convergence = 0
            break
        H = H_ + (M + H_ - e*np.sinh(H_)) / (e*np.cosh(H_)-1)
    if out_angle != 'Rad':
        H = np.degrees(H)
    return H, convergence

def EBH2theta(e, E = None, B = None, H = None, p = None, r = None, in_angle = 'Rad', out_angle = 'Rad'):
    '''
    Eccentric anomaly or parabolic anomaly or hyperbolic anomaly to true anomaly
    
    Args:
        e (float): eccentricity [-]

        E (float): eccentric anomaly [rad]

        B (float): parabolic anomaly [rad]

        H (float): hyperbolic anomaly [rad]

        p (float): semilatus rectum [km] (add for parabolic case)

        r (float): ratio [km] (add for parabolic case)

        in_angle (string): angle unit of E, B or H, 'Rad' or 'Deg'

        out_angle (string): angle unit of theta, 'Rad' or 'Deg'

    Outputs:
        theta (float): true anomaly [rad]
    '''
    if e < 1.0:
        if in_angle != 'Rad':
            E = np.radians(E)
        sin_theta = (np.sin(E)*np.sqrt(1-e**2))/(1-e*np.cos(E))
        cos_theta = (np.cos(E)-e)/(1-e*np.cos(E))
        theta = np.arctan2(sin_theta, cos_theta)
        if theta < 0:
            theta += 2*np.pi # no seeee
    if e == 1:
        if in_angle != 'Rad':
            B = np.radians(B)
        sin_theta = (p*B/r)
        cos_theta = ((p-r)/r)
        theta = np.arctan2(sin_theta, cos_theta)
    if e > 1.0:
        if in_angle != 'Rad':
            H = np.radians(H)
        sin_theta = (-np.sinh(H)*np.sqrt(e**2-1))/(1-e*np.cosh(H))
        cos_theta = (np.cosh(H)-e)/(1-e*np.cosh(H))
        theta = np.arctan2(sin_theta, cos_theta)
    if out_angle != 'Rad':
        theta = np.degrees(theta)
    return theta

e_list = []
for k in range(10):
    e_list.append(random.uniform(1.01, 10.0))


plt.figure(figsize=(10, 10), dpi=80)                                 
plt.grid()
plt.title("Mean Anomaly vs True Anomaly")
plt.xlabel("True Anomaly [deg]")
plt.ylabel("Mean Anomaly [deg]")
plt.xlim(-180, 180)
plt.ylim(-180, 180)
plt.xticks([-180, -150, -120, -90, -60, -30, 0, 30, 60, 90, 120, 150, 180],
           [r'$-180°$', r'$-150°$', r'$-120°$', r'$-90°$', r'$-60°$', r'$-30°$', r'$0°$', r'$30°$', r'$60°$', r'$90°$', r'$120°$', r'$150°$', r'$180°$'])
plt.yticks([-180, -150, -120, -90, -60, -30, 0, 30, 60, 90, 120, 150, 180],
           [r'$-180°$', r'$-150°$', r'$-120°$', r'$-90°$', r'$-60°$', r'$-30°$', r'$0°$', r'$30°$', r'$60°$', r'$90°$', r'$120°$', r'$150°$', r'$180°$'])

for e in e_list:
    M_list = []
    theta_list = []
    for M in range(-180, 181):
        M_list.append(M)
        H, convergence = KepEqtnH(M, e, in_angle = 'Deg', out_angle = 'Rad')
        theta = EBH2theta(e, 0, 0, H, in_angle = 'Rad', out_angle = 'Deg')
        theta_list.append(theta)
    f = "{:.2f}".format(e)  
    plt.plot(theta_list, M_list, linewidth=1.0, linestyle="-", label = f)
    plt.legend(loc='best')
plt.show()
