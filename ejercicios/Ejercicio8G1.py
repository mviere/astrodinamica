"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rcParams['lines.color'] = 'k'
mpl.rcParams['axes.prop_cycle'] = mpl.cycler('color', ['k'])

x = np.linspace(-6e7, 6e7, 400)
y = np.linspace(-6e7, 6e7, 400)
x, y = np.meshgrid(x, y)

def axes():
    plt.axhline(0, alpha=.1)
    plt.axvline(0, alpha=.1)

def a_e_2_EN_h(a, e, mu = 3.986e14, r_periapsis = None):
    """
    Semi-major axis and eccentricity to energy and angular momentum

    Args:
        a (float / string): semi-major axis [m] or 'Inf' for parabolas
        
        e (float): eccentricity [-]
        
        mu (float): central body gravitational parameter [m^3/s^2]
        
        r_periapsis (float): radius of periapsis [m] (add for parabolic case)
    
    Outputs:
        EN (float): orbit energy [m^2/s^2]
        
        h (float): angular momentum [m^2/s]
    
    Examples
        1. CIRCULAR
            a = 1.2756e7 m
            e = 0
            >> EN = -1.5624e7 m^2/s^2
            >> h = 7.1306e10 m^2/s 

        2. ELIPTICAL
            a = 3.189e7 m
            e = 0.6
            >> EN = -6.2496e6 m^2/s^2
            >> h = 9.01957e10 m^2/s
        
        3. HYPERBOLIC
            a = - 3.189e6 m
            e = 5
            >> EN = 6.2496e7 m^2/s^2
            >> h = 1.7466e11 m^2/s
        
        3. PARABOLIC
            a = 'Inf'
            e = 1
            r_periapsis = 1.2756e7 m
            >> EN = 0 m^2/s^2
            >> h = 1.0084e11 m^2/s
    """
    if type(a) == float or type(a) == int:  # circular / elliptical / hyperbolic
        EN = -mu/(2*a)
        p = a*(1-e**2)
    else:                                   # parabolic case
        EN = 0
        e = 1
        p = 2*r_periapsis
    h = np.sqrt(p*mu)
    return EN, h

def EN_h_2_a_e(EN, h, mu = 3.986e14, r_periapsis = None):
    """
    Energy and angular momentum to semi-major axis and eccentricity

    Args:
        EN (float): orbit energy [m^2/s^2]
        
        h (float): angular momentum [m^2/s]
        
        mu (float): central body gravitational parameter [m^3/s^2]
        
        r_periapsis (float): radius of periapsis [m] (add for parabolic case)
    
    Outputs:
        a (float / string): semi-major axis [m] or 'Inf' for parabolas
        
        e (float): eccentricity [-]
    
    Examples
        1. CIRCULAR
            EN = -1.5624e7 m^2/s^2
            h = 7.1306e10 m^2/s
            >> a = 1.2756e7 m
            >> e = 0

        2. ELIPTICAL
            EN = -6.2496e6 m^2/s^2
            h = 9.01957e10 m^2/s
            >> a = 3.189e7 m
            >> e = 0.6
        
        3. HYPERBOLIC
            EN = 6.2496e7 m^2/s^2
            h = 1.7466e11 m^2/s
            >> a = - 3.189e6 m
            >> e = 5
        
        3. PARABOLIC
            EN = 0 m^2/s^2
            h = 1.0084e11 m^2/s
            r_periapsis = 1.2756e7 m
            >> a = 'Inf'
            >> e = 1
    """
    if EN == 0:                              # parabolic case
        a = 'Inf'
        p = 2*r_periapsis
        e = 1
    else:                                   # circular / elliptical / hyperbolic
        a = -mu/(2*EN)
        p = h**2/mu
        e = np.sqrt(1-(p/a))
    return a, e

# Tomado de https://mmas.github.io/conics-matplotlib

def plotcon(a = None, e = None, EN = None, h = None, mu = 3.986e14, r_periapsis = None):
    """
    Plotting of conic functions: circular, elliptical, parabolic and hyperbola,
    given two parameters: semi-major axis and eccentricity or specific energy and angular momentum.

    Important: when giving the arguments specify the variable. For example, plotcon(EN = 6.2496e7 m^2/s^2, h = 1.7466e11 m^2/s).

    Args:
        EN (float): orbit energy [m^2/s^2]
        
        h (float): angular momentum [m^2/s]
        
        mu (float): central body gravitational parameter [m^3/s^2]
        
        r_periapsis (float): radius of periapsis [m] (add for parabolic case. if so, add also mu)
    
        a (float / string): semi-major axis [m] or 'Inf' for parabolas
        
        e (float): eccentricity [-]
    
    Outputs:
        plot 
    """
    if EN != None and h != None:
        EN_ = EN
        h_ = h
        if mu != 3.986e14:
            mu_ = mu
        else:
            mu_ = 3.986e14
        if r_periapsis != None:
            r_periapsis1 = r_periapsis
        else:
            r_periapsis1 = None
        a, e = EN_h_2_a_e(EN = EN_, h = h_, mu = mu_, r_periapsis = r_periapsis1)
    
    if 0 <= e < 1:                                      # circular / elliptical
        b = a*np.sqrt(1-e**2)
        axes()
        plt.contour(x, y,(x**2/a**2 + y**2/b**2), [1], colors='k')
        plt.show()
    
    elif e > 1:                                         # hyperbolic
        axes()                                         
        b = a*np.sqrt((e**2)-1)
        plt.contour(x, y,(x**2/a**2 - y**2/b**2), [1], colors='k')
        plt.show()

    else:                                               # parabolic case
        axes()
        plt.contour(x, y, (y**2 - 4*r_periapsis*x), [0], colors='k')
        plt.show()
    return
    
