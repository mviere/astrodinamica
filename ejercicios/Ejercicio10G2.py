"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np
from matplotlib import pyplot as plt

theta = np.linspace(-np.pi, np.pi, 400)
B = np.tan(theta/2)
M = B + (B**3)/3

plt.figure(figsize=(10, 10), dpi=80)                                 
plt.grid()
plt.xlabel("True Anomaly")
plt.ylabel("Mean Anomaly")
plt.xlim(-np.pi, np.pi)
plt.ylim(-np.pi, np.pi)
plt.xticks([-np.pi, -5*np.pi/6, -2*np.pi/3, -np.pi/2, -np.pi/3, -np.pi/6, 0, np.pi/6, np.pi/3, np.pi/2, 2*np.pi/3, 5*np.pi/6, np.pi],
           [r'$-180°$', r'$-150°$', r'$-120°$', r'$-90°$', r'$-60°$', r'$-30°$', r'$0°$', r'$30°$', r'$60°$', r'$90°$', r'$120°$', r'$150°$', r'$180°$'])
plt.yticks([-np.pi, -5*np.pi/6, -2*np.pi/3, -np.pi/2, -np.pi/3, -np.pi/6, 0, np.pi/6, np.pi/3, np.pi/2, 2*np.pi/3, 5*np.pi/6, np.pi],
           [r'$-180°$', r'$-150°$', r'$-120°$', r'$-90°$', r'$-60°$', r'$-30°$', r'$0°$', r'$30°$', r'$60°$', r'$90°$', r'$120°$', r'$150°$', r'$180°$'])
plt.plot(theta, M, color="blue", linewidth=1.0, linestyle="-")
plt.show()