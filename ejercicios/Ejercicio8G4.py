"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def InclinationOnly(delta_i, e, a = None, p = None, theta = None, mu = 398600.441, in_angle = 'Deg'):
    '''
    Devuelve el delta de velocidad (delta_v) en la maniobra Inclination Only entre orbitas circulares o elipticas.

    Args:
        delta_i (float): inclination change [deg]

        e (float): eccentricity [-]

        a (float): semimayor axis [km] (obligatory for circular case. for eliptical case, enter a instead of p)

        p (float): semilatus rectum [km] (for eliptical case, enter a instead of p)

        theta (float): true anomaly at node [deg] (for eliptical case)

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of delta_i and theta, 'Rad' or 'Deg'

    Outputs:
        delta_v (float): total change in velocity [km/s]
    '''
    if in_angle == 'Deg' and theta != None:
        theta = np.radians(theta)
        delta_i = np.radians(delta_i)
    
    if e == 0.0:
        vi = np.sqrt(mu/a)
        delta_v = 2*vi*np.sin(delta_i/2)
    else:
        if p == None and a != None:
            p = a*(1-e**2)
        if p != None and a == None:
            a = p/(1-e**2)
        
        r1 = p/(1+e*np.cos(theta))
        vi1 = np.sqrt((2*mu/r1)-mu/a)
        phi_fpa1 = np.arctan(e*np.sin(theta)/(1+e*np.cos(theta)))
        delta_v1 = 2*np.cos(phi_fpa1)*vi1*np.sin(delta_i/2)
        
        r2 = p/(1+e*np.cos(theta-np.pi))
        vi2 = np.sqrt((2*mu/r2)-mu/a)
        phi_fpa2 = np.arctan(e*np.sin(theta-np.pi)/(1+e*np.cos(theta-np.pi)))
        delta_v2 = 2*np.cos(phi_fpa2)*vi2*np.sin(delta_i/2)

        if delta_v1 <= delta_v2:
            delta_v = delta_v1
        else:
            delta_v = delta_v2
    return delta_v

def RAANOnly(i_initial, delta_raan, a, mu = 398600.441, in_angle = 'Deg', out_angle = 'Deg'):
    '''
    Devuelve el delta de velocidad (delta_v) y los argumentos de latitud final e inicial
    en la maniobra RAAN Only entre orbitas circulares.

    Args:
        i_initial (float): initial inclination [deg]

        delta_raan (float): delta RAAN [deg]

        a (float): semimayor axis [km]

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of i_initial and delta_raan, 'Rad' or 'Deg'

        out_angle (string): angle unit of u_initial and u_final, 'Rad' or 'Deg'

    Outputs:
        delta_v (float): total change in velocity [km/s]

        u_initial (float): initial argument of latitud [deg]

        u_final (float): final argument of latitud [deg]
    '''
    if in_angle == 'Deg':
        i_initial = np.radians(i_initial)
        delta_raan = np.radians(delta_raan)
    vi = np.sqrt(mu/a)
    theta = np.arccos((np.cos(i_initial)**2)+(np.sin(i_initial)**2)*np.cos(delta_raan))
    delta_v = 2*vi*np.sin(theta/2)
    # u_initial es el u donde iniciar la maniobra
    u_initial = np.arccos(np.tan(i_initial)*(np.cos(delta_raan)-np.cos(theta))/np.sin(theta))
    # u_final es el u que queda cuando ya se realizo la maniobra, toma un tiempo la maniobra, va a estar en otro u
    u_final = np.arccos(np.cos(i_initial)*np.sin(i_initial)*((1-np.cos(delta_raan))/np.sin(theta)))
    if out_angle == 'Deg':
        u_initial = np.degrees(u_initial)
        u_final = np.degrees(u_final)
    return delta_v, u_initial, u_final

def Combinada(i_initial, i_final, delta_raan, a, mu = 398600.441, in_angle = 'Deg', out_angle = 'Deg'):
    '''
    Devuelve el delta de velocidad (delta_v) y los argumentos de latitud final e inicial
    en la maniobra combinada (Incination and RAAN) entre orbitas circulares.

    Args:
        i_initial (float): initial inclination [deg]

        i_final (float): final inclination [deg]

        delta_raan (float): ascending node change [deg]

        a (float): semimayor axis [km]

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of i_initial, i_final and delta_raan, 'Rad' or 'Deg'

        out_angle (string): angle unit of u_initial and u_final, 'Rad' or 'Deg'

    Outputs:
        delta_v (float): total change in velocity [km/s]

        u_initial (float): initial argument of latitud [deg]

        u_final (float): final argument of latitud [deg]
    '''
    if in_angle == 'Deg':
        i_initial = np.radians(i_initial)
        i_final = np.radians(i_final)
        delta_raan = np.radians(delta_raan)
    vi = np.sqrt(mu/a)
    theta = np.arccos(np.cos(i_initial)*np.cos(i_final)+np.sin(i_initial)*np.sin(i_final)*np.cos(delta_raan))
    delta_v = 2*vi*np.sin(theta/2)
    u_initial = np.arccos((np.sin(i_final)*np.cos(delta_raan)-np.cos(theta)*np.sin(i_initial))/(np.sin(theta)*np.cos(i_initial)))
    u_final = np.arccos((np.cos(i_initial)*np.sin(i_final)-np.sin(i_initial)*np.cos(i_final)*np.cos(delta_raan))/np.sin(theta))
    if out_angle == 'Deg':
        u_initial = np.degrees(u_initial)
        u_final = np.degrees(u_final)
    return delta_v, u_initial, u_final

def InclinationRAAN(i_initial, i_final, delta_raan, a, mu = 398600.441, in_angle = 'Deg', out_angle = 'Deg'):
    '''
    Devuelve el delta de velocidad (delta_v) y los argumentos de latitud inicial, donde iniciar el RAAN,
    y final, de la maniobra Inclination, luego, RAAN entre orbitas circulares.

    Args:
        i_initial (float): initial inclination [deg]

        i_final (float): final inclination [deg]

        delta_raan (float): ascending node change [deg]

        a (float): semimayor axis [km]

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of i_initial, i_final and delta_raan, 'Rad' or 'Deg'

        out_angle (string): angle unit of u_initial and u_final, 'Rad' or 'Deg'

    Outputs:
        delta_v (float): total change in velocity [km/s]

        u_initial (float): initial argument of latitud [deg]

        u_final (float): final argument of latitud [deg]
    '''
    if in_angle == 'Deg':
        i_initial = np.radians(i_initial)
        i_final = np.radians(i_final)
        delta_raan = np.radians(delta_raan)
    
    delta_i = abs(i_final-i_initial)
    delta_vI = InclinationOnly(delta_i, 0, a, 0, 0, mu, in_angle = 'Rad')
    delta_vRAAN, u_initial, u_final = RAANOnly(i_final, delta_raan, a, mu, in_angle = 'Rad')
    delta_v = delta_vI + delta_vRAAN
    if out_angle != 'Deg':
        u_initial = np.radians(u_initial)
        u_final = np.radians(u_final)
    return delta_v, u_initial, u_final

def RAANInclination(i_initial, i_final, delta_raan, a, mu = 398600.441, in_angle = 'Deg', out_angle = 'Deg'):
    '''
    Devuelve el delta de velocidad (delta_v) y los argumentos de latitud inicial, donde iniciar el RAAN,
    y final, de la maniobra RAAN, luego, Inclination entre orbitas circulares.

    Args:
        i_initial (float): initial inclination [deg]

        i_final (float): final inclination [deg]

        delta_raan (float): ascending node change [deg]

        a (float): semimayor axis [km]

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of i_initial, i_final and delta_raan, 'Rad' or 'Deg'

        out_angle (string): angle unit of u_initial and u_final, 'Rad' or 'Deg'

    Outputs:
        delta_v (float): total change in velocity [km/s]

        u_initial (float): initial argument of latitud [deg]

        u_final (float): final argument of latitud [deg]
    '''
    if in_angle == 'Deg':
        i_initial = np.radians(i_initial)
        i_final = np.radians(i_final)
        delta_raan = np.radians(delta_raan)
    
    delta_vRAAN, u_initial, u_final = RAANOnly(i_initial, delta_raan, a, mu, in_angle = 'Rad')
    delta_i = abs(i_final-i_initial)
    delta_vI = InclinationOnly(delta_i, 0, a, 0, 0, mu, in_angle = 'Rad')
    delta_v = delta_vI + delta_vRAAN
    if out_angle != 'Deg':
        u_initial = np.radians(u_initial)
        u_final = np.radians(u_final)
    return delta_v, u_initial, u_final


a = 6904
i_initial = 98
i_final = 97.5
delta_raan = 0.4
delta_i = 0.5

# Combinada
delta_v_combinada, u_i_combinada, u_f_combinada = Combinada(i_initial, i_final, delta_raan, a)
print('delta_v combinada: ', delta_v_combinada)
print('u donde inicia la maniobra: ', u_i_combinada)
# InclinationRAAN
delta_v_IRAAN, u_i_IRAAN, u_f_IRAAN = InclinationRAAN(i_initial, i_final, delta_raan, a)
print('delta_v IRAAN: ', delta_v_IRAAN)
print('u donde inicia la maniobra: ', u_i_IRAAN)
# RAANInclination
delta_v_RAANI, u_i_RAANI, u_f_RAANI = RAANInclination(i_initial, i_final, delta_raan, a)
print('delta_v RAANI: ', delta_v_RAANI)
print('u donde inicia la maniobra: ', u_i_RAANI)