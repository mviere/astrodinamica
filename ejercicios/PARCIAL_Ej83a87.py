"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

'''
Funciones utilizadas
'''
def deg2rad(a):

    """
    Conversion from degrees to radians
    
    Args:
        a (float): angle [deg]
    
    Outputs:
        a (float): angle [rad]
        
    """
    
    a = a*np.pi/180
    return a

def rad2deg(a):

    """
    Conversion from radians to degrees
    
    Args:
        a (float): angle [rad]
    
    Outputs:
        a (float): angle [deg]
        
    """

    a = a*180/np.pi
    return a


'''
Calculos
'''
# Datos
RT = 6371

r_LEO = RT + 550
r_GPS = RT + 20200

mu = 398600.441

# Obtengo el angulo de cobertura (con respecto a la tierra) del satelite LEO
th_a = np.arccos(RT/r_LEO)
th_b = np.arccos(RT/r_GPS)

th_sight = 2*(th_a + th_b)

print('Ángulo de Cobertura =', rad2deg(th_sight))

# El ángulo de cobertura es es de 198.2458727057338°, como la separacion entre satelite es de 60°, se puede inferir que;
n_min = 3
n_max = 4

print('Número míninimo de satélites:', n_min)
print('Número máximo de satélites:', n_max)

# Obtengo el angulo (con respecto a la tierra) para el cual el satelite LEO no tiene cobertura
th_no_sight = 2*np.pi - th_sight

# Obtengo la velocidad angular relativa de LEO con respecto a GPS
w_LEO = np.sqrt(mu/r_LEO**3)
w_GPS = np.sqrt(mu/r_GPS**3)
print(w_LEO)
print(w_GPS)

delta_w = w_LEO - w_GPS
print(delta_w)

# Considerando el instante para el cual el satelite LEO empieza a observar el numero maximo de satelites GPS, podemos calcular el angulo que debe recorrer para volver a ver el minimo;
th_max = np.pi - th_no_sight
print('th_max', th_max)
# A partir de este angulo y la velocidad angular relativa hallada previamente, obtenemos el tiempo seguido que ve el numero maximo de satelites
t_max = th_max/delta_w

print('t (máximo) =', t_max)

# Considerando el instante para el cual el satelite LEO deja de observar el numero maximo de satelites GPS, podemos calcular el angulo que debe recorrer para volver a ver dicho maximo;
th_min = np.pi/3 - th_max

# A partir de este angulo y la velocidad angular relativa hallada previamente, obtenemos el tiempo seguido que ve el numero minimo de satelites
t_min = th_min/delta_w

print('t (mínimo) =', t_min)




