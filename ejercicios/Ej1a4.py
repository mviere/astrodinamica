"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def SME(v, r, mu = 398600.441):
    '''
    Specific Mechanical Energy

    Args:
        v (float): velocity [km/s]

        r (float): ratio [km]
        
        mu (float): central body gravitational parameter [km^3/s^2]
    
    Outputs:
        EN (float): specific mechanical energy [km^2/s^2]
    '''
    EN = (v**2)/2 - mu/r
    return EN

def ENh2ae(EN, h, r_periapsis = None, mu = 398600.441):
    '''
    Energy and angular momentum to semi-major axis and eccentricity

    Args:
        EN (float): orbit energy [km^2/s^2]
        
        h (float): angular momentum [km^2/s]

        r_periapsis (float): radius of periapsis [km] (add for parabolic case)
        
        mu (float): central body gravitational parameter [km^3/s^2]
    
    Outputs:
        a (float / string): semi-major axis [km] or 'Inf' for parabolas
        
        e (float): eccentricity [-]
    '''
    if EN == 0:                              # parabolic case
        a = 'Inf' # (Table 1-1 Vallado)
        p = 2*r_periapsis # (Table 1-1 Vallado)
        e = 1 # (Table 1-1 Vallado)
    else:                                   # circular / elliptical / hyperbolic
        a = -mu/(2*EN) # (Eq. 1-21 Vallado)
        p = h**2/mu # (Eq. 1-19 del Vallado)
        e = np.sqrt(1-(p/a)) # (Eq. 1-19 del Vallado)
    return a, e

def ae2p(a, e, theta = None, h = None, mu = 398600.441, in_angle = 'Rad'):
    '''
    Semi-major axis and eccentricity to semilatus rectum

    Args:
        a (float): semi-major axis [km]
        
        e (float): eccentricity [-]

        theta (float): true anomaly [rad] (add for eliptical and hyperbolic case)

        h (float): angular momentum [km^2/s] (add for parabolic case)

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of theta, 'Rad' or 'Deg'
    
    Outputs:
        p (float): semilatus rectum [m]
    '''
    if in_angle != 'Rad' and theta != None:
        theta = np.radians(theta)
    if theta == None and h == None:              # circular
        p = a # (Table 1-1 Vallado)
    if theta == None and h != None:             # parabolic
        p = -h**2/mu # (Eq. 1-19 del Vallado)
    else:                                       # eliptical / hyperbolic
        p = a*(1-e**2) # (Eq. 1-19 del Vallado)
    return p

# Datos del problema
radio_terrestre = 6378.137
h = 662
v_tangencial = 7
v_radial = 3.72

# Energía específica
v = np.sqrt((v_tangencial)**2+(v_radial)**2) # Cálculo trigonométrico.
r = radio_terrestre + h
EN = SME(v, r)
print('Energía específica:', EN) # Como la energía es negativa, sabemos que la órbita es elíptica.

# Excentricidad y semi-eje mayor
h = v_tangencial*r # h = r^2 * nu_dot (Eq. 1-18 del Vallado), como r * nu_dot = v_tangencial, h = v_tangencial * r
a, e = ENh2ae(EN, h) # El imput de esta función para órbitas elípticas es EN y h.
print('Excentricidad:', e) 

# Anomalía verdadera
p = ae2p(a, e) # El imput de esta función para órbitas elípticas es a y e.
theta = np.arccos(((p/r)-1)*(1/e)) # Ecuación de trayectoria, primera ley de Kepler (Eq. 1-24 del Vallado).
theta = np.degrees(theta)
print('Anomalía verdadera:', theta)