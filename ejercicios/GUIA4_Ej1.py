"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

# Datos
delta_v = 0.2
rt = 6378.1363
h1 = 250
mu = 398600.441 

# Obtengo la velocidad inicial
v1 = np.sqrt(mu/(rt + h1))

# Obtengo la velocidad final
v2 = abs(delta_v + v1)

# Obtengo el nuevo semieje mayor
a2 = mu/(2*mu/(rt + h1) - (v2**2))

# Obtengo la altura del nuevo apogeo
h2 = 2*a2 - 2*rt - h1

print(h2)