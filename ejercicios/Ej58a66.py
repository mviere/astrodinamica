"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np
import math

def Hohmann(radioi, radiof, mu = 398600.441):
    '''
    Devuelve el delta de velocidad (delta_v) total y el tiempo de vuelo (t_trans) en una transferencia de Hohmann
    entre dos orbitas circulares con radio inicial (radioi) y radio final (radiof).
    Algorithm 36 Vallado.

    Args:
        radioi (float): initial radius [km]

        radiof (float): final radius [km]

        mu (float): central body gravitational parameter [km^3/s^2]

    Outputs:
        delta_v (float): total change in velocity [km/s]

        t_trans (float): time on trasfer orbit [s]
    '''
    a_trans = (radioi+radiof)/2
    
    vi = np.sqrt(mu/radioi)
    v_transa = np.sqrt((2*mu/radioi)-mu/a_trans)
    
    vf = np.sqrt(mu/radiof)
    v_transb = np.sqrt((2*mu/radiof)-mu/a_trans)

    delta_va = v_transa-vi
    delta_vb = vf-v_transb
    delta_v = abs(delta_va)+abs(delta_vb)
    
    t_trans = np.pi*np.sqrt((a_trans**3)/mu)
    
    return delta_v, t_trans, a_trans, delta_va, delta_vb

def PropelentMass(delta_v, mi, isp, g = 0.00980665):
    '''
    Devuelve la masa de propelente requerida en un impulso delta_v, a partir de la ecuación de cohetes ideal.
    Pag. 373 Vallado.

    Args:
        delta_v (float): total change in velocity [km/s]

        mi (float): vehicle's initial mass [kg]

        isp (float): specific impulse [s]

        g (float): central body gravity [km/s^2]

    Outputs:
        mp (float): propelent mass [kg]
    '''
    mp = mi - mi/(math.exp(delta_v/(g*isp)))
    return mp

# Radio inicial y final de la transferencia de Hohmann
radioi = 280 + 6378.137
radiof = 42164

# Obtengo los delta de velocidad (total y en cada impulso) requerido para la transferencia y el tiempo que toma.
delta_v, t_trans, a_trans, delta_va, delta_vb = Hohmann(radioi, radiof)
print('SMA órbita de transferencia:', a_trans)
print('Delta_v en el primer impulso:', delta_va)
print('Delta_v en el segundo impulso:', delta_vb)
print('Tiempo de transferencia:', t_trans)

# La transferencia ocurre desde el periapsis al apoapsis de la órbita de transferencia.
r_perigeo = radioi
e_trans =  1 - r_perigeo/a_trans # r_perigeo = a*(1-e)
print('Excentricidad órbita de transferencia:', e_trans)

# La masa del propelente para cada impulso específico
mpf = PropelentMass(delta_v, 500, 250)
mpg = PropelentMass(delta_v, 500, 350)
mph = PropelentMass(delta_v, 500, 450)
print('Masa de propelente con isp = 250 s:', mpf)
print('Masa de propelente con isp = 350 s:',mpg)
print('Masa de propelente con isp = 450 s:',mph)