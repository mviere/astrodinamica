"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def r_2_b_el_ro(r, out_angle = 'Rad'):
    ro = np.sqrt(r[0]**2+r[1]**2+r[2]**2)
    phi = np.arccos(r[2]/ro)
    theta = np.arcsin(r[1]/(ro*np.sin(phi)))
    el = np.pi/2 - phi
    b = np.pi - theta
    if out_angle != 'Rad':
        b = b*180/np.pi
        el = el*180/np.pi
    return b, el, ro

b = 38
el = 0.6
ro = 4.5
b_ = 0
el = 0
ro_ = -0.3

def b_el_ro_2_rSEZ_vSEZ(b, ro, el, b_, ro_, el_, in_angle = 'Rad'):

    if in_angle != 'Rad':
        b = np.radianes(b)
        el = np.radianes(el)
        b_ = np.radianes(b_)
        el_ = np.radianes(el_)

    x = -ro*np.cos(el)*np.cos(b)
    y = ro*np.cos(el)*np.sin(b)
    z = ro*np.sin(el)

    x_ = -ro_*np.cos(el)*np.cos(b) + ro*np.sin(el)*np.cos(b)*el_ + ro*np.cos(el)*np.sin(b)*b_
    y_ = ro_*np.cos(el)*np.sin(b) - ro*np.sin(el)*np.sin(b)*el_ + ro*np.cos(el)*np.cos(b)*b_
    z_ = ro_*np.sin(el) + ro*np.cos(el)*el_

    r = np.array([x, y, z])
    v = np.array([x_, y_, z_])
    return r, v
