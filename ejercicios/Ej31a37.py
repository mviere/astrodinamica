"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def ROT1(a, positive = True): # por default crece positivamente
    if positive:
        ROT1 = np.array([[1, 0, 0], [0, np.cos(a), np.sin(a)], [0, -np.sin(a), np.cos(a)]])
    else:
        ROT1 = np.array([[1, 0, 0], [0, np.cos(a), -np.sin(a)], [0, np.sin(a), np.cos(a)]])
    return ROT1

def ROT2(a, positive = True): # por default crece positivamente
    if positive:
        ROT2 = np.array([[np.cos(a), 0, -np.sin(a)], [0, 1, 0], [np.sin(a), 0, np.cos(a)]])
    else:
        ROT2 = np.array([[np.cos(a), 0, np.sin(a)], [0, 1, 0], [-np.sin(a), 0, np.cos(a)]])
    return ROT2

def ROT3(a, positive = True): # por default crece positivamente
    if positive:
        ROT3 = np.array([[np.cos(a), np.sin(a), 0], [-np.sin(a), np.cos(a), 0], [0, 0, 1]])
    else:
        ROT3 = np.array([[np.cos(a), -np.sin(a), 0], [np.sin(a), np.cos(a), 0], [0, 0, 1]])
    return ROT3

def COE2RV(p, e, i, raan = None, w = None, theta = None, u = None, lt = None, wt = None, mu = 398600.441, in_angle = 'Deg'):
    '''
    Some orbital elements (e, i, raan, w, theta, u, lt, wt) and semilatus rectum (p)
    to position vector (r_ECI) and velocity vector (v_ECI), both in ECI.
    Algorithm 10 Vallado.
    
    Args:
        p (float): semilatus rectum [km]

        e (float): eccentricity [-]

        i (float): inclination [deg]

        raan (float): right ascension of ascending node [deg]

        w (float): argument of perigee [deg]

        theta (float): true anomaly [deg]

        u (float): argument of latitud [deg]

        lt (float): true longitud [deg]

        wt (float): true longitud of periapsis [deg]

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of i, raan, w, theta, u, lt and wt, 'Rad' or 'Deg'

    Outputs:
        r_ECI (vector): position vector [km]

        v_ECI (vector): velocity vector [km/s]
    '''
    if in_angle == 'Deg':
        i = np.radians(i)
        if raan != None:
            raan = np.radians(raan)
        if w != None:
            w = np.radians(w)
        if theta != None:
            theta = np.radians(theta)
        if u != None:
            u = np.radians(u)
        if lt != None:
            lt = np.radians(lt)
        if wt != None:
            wt = np.radians(wt)
    if i == 0.0 and e == 0.0:
        raan = 0.0
        w = 0.0
        theta = lt
    if i != 0.0 and e == 0.0:
        w = 0.0
        theta = u
    if i == 0.0 and e < 1:
        raan = 0.0
        w = wt

    r_PQW_i = (p*np.cos(theta))/(1+e*np.cos(theta))
    r_PQW_j = (p*np.sin(theta))/(1+e*np.cos(theta))
    r_PQW = np.array([r_PQW_i, r_PQW_j, 0])

    v_PQW_i = -np.sqrt(mu/p)*np.sin(theta)
    v_PQW_j = np.sqrt(mu/p)*(e+np.cos(theta))
    v_PQW = np.array([v_PQW_i, v_PQW_j, 0])

    PQW2ECI = np.dot(np.dot(ROT3(raan, False), ROT1(i, False)),ROT3(w, False))
    r_ECI = np.dot(PQW2ECI, r_PQW)
    v_ECI = np.dot(PQW2ECI, v_PQW)

    return r_ECI, v_ECI

# Datos del problema
a = 8000
e = 0.1 # Órbita elíptica
i = 53
raan = 120
w = 270
theta = 30

# Obtengo los vectores posición y velocidad en sistema ECI
p = a*(1-e**2) # Ecuación de semilatus rectum para órbita elíptica (Eq. 1-19, Table 1-1 del Vallado)
r_ECI, v_ECI = COE2RV(p, e, i, raan, w, theta)
print('Vector posición:')
print(r_ECI)
print('Vector velocidad:')
print(v_ECI)