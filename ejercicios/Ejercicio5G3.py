"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np
from datetime import datetime, timedelta

def JulianDate(yr, mo, d, h, min, s):

    """
    Finds the Julian date (based on UT1) from the entered date and time (gregorian)

    Args:
        yr (int): year

        mo (int): month

        d (int): day

        h (int): hours

        min (int): minutes

        s (float): seconds
    
    Outputs:
        JD (float): Julian date

    """

    JD = 367*(yr) - int(7*(yr + int(((mo + 9)/12)))/4) + int(275*mo/9) + d + 1721013.5 + (((s/60 + min)/60) + h)/24

    return JD

def ConvTime(yr, mo, d, h, min, s, DAT):

    """
    UTC time to the respective Julian date (J2000) 

    Args:
        yr (int): year

        mo (int): month

        d (int): day

        h (int): hours

        min (int): minutes

        s (float): seconds

        DUT1 (float): delta of UT1 - UTC [s]

        DAT (float): delta of TAI - UTC [s]

    Outputs:
        TTT (float): Julian date in centuries (J2000)

    """
    UTC = datetime(yr, mo, d, h, min, s)
    DAT = timedelta(seconds=DAT)
    TAI = UTC + DAT
    TT = TAI + timedelta(seconds=32.184)
    JDTT = JulianDate(TT.year, TT.month, TT.day, TT.hour, TT.minute, TT.second)
    TTT = (JDTT - 2451545)/36525

    return TTT