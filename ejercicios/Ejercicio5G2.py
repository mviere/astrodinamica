"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def theta_2_E_B_H(theta, e):
    '''
    True anomaly to eccentric anomaly or parabolic anomaly or hyperbolic anomaly
    
    Args:
        theta (float): true anomaly [deg]
            - eliptical 0 < theta < 360
            - parabolic -180 < theta < 180
            - hyperbolic -180 + arcos(1/e) < theta < 180 - arcos(1/e)

        e (float): eccentricity [-]

    Outputs:
        E (float): eccentric anomaly [deg]

        B (float): parabolic anomaly [deg]

        H (float): hyperbolic anomaly [deg]
    '''
    theta = np.radians(theta)
    if e < 1.0:
        sin_E = (np.sin(theta)*np.sqrt(1-e**2))/(1+e*np.cos(theta))
        cos_E = (np.cos(theta)+e)/(1+e*np.cos(theta))
        E = np.arctan2(sin_E, cos_E)
        E = np.degrees(E)
        if E < 0:
            E += 360
        return E
    if e == 1:
        B = np.tan(theta/2)
        B = np.degrees(B)
        return B
    if e > 1.0:
        H = np.arcsinh((np.sin(theta)*np.sqrt(e**2-1))/(1+e*np.cos(theta))) 
        H = np.degrees(H)
        return H

if __name__ == "__main__":
    print('-----------------------------------------------------------------------------------')
    print('This is an example to verify the functionality of the script.')
    print('Given theta = 260 deg and e = 0.7411, eccentric anomaly (E) should be 310.64 deg.')
    print('Given theta = -45 deg and e = 1, parabolic anomaly (B) should be -23.73 deg.')
    print('Given theta = -128.69 deg and e = 1.3837, hyperbolic anomaly (H) should be -138.14 deg.')
    print('Let\'s see,')
    E = theta_2_E_B_H(260, 0.7411)
    B = theta_2_E_B_H(-45, 1)
    H = theta_2_E_B_H(-128.69, 1.3837)
    print('>>> eccentric anomaly (E): ', E)
    print('>>> parabolic anomaly (B): ', B)
    print('>>> hyperbolic anomaly (H): ', H)
    print('-----------------------------------------------------------------------------------')