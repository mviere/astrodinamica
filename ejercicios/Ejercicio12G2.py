"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def theta2EBH(theta, e, in_angle = 'Rad', out_angle = 'Rad'):
    '''
    True anomaly (theta) to eccentric anomaly (E) or parabolic anomaly (B) or hyperbolic anomaly (H).
    
    Args:
        theta (float): true anomaly [rad]

        e (float): eccentricity [-]

    Outputs:
        E (float): eccentric anomaly [rad]

        B (float): parabolic anomaly [rad]

        H (float): hyperbolic anomaly [rad]
    '''
    if in_angle != 'Rad':
        theta = np.radians(theta)
    if e < 1.0:
        sin_E = (np.sin(theta)*np.sqrt(1-e**2))/(1+e*np.cos(theta))
        cos_E = (np.cos(theta)+e)/(1+e*np.cos(theta))
        E = np.arctan2(sin_E, cos_E)
        if E < 0:
            E += 2*np.pi # nooooos ee
        if out_angle != 'Rad':
            E = np.degrees(E)
        return E
    if e == 1:
        B = np.tan(theta/2)
        if out_angle != 'Rad':
            B = np.degrees(B)
        return B
    if e > 1.0:
        sinh_H = (np.sin(theta)*np.sqrt(e**2-1))/(1+e*np.cos(theta))    
        cosh_H = (e+np.cos(theta))/(1+e*np.cos(theta))
        H = np.arctanh(sinh_H/cosh_H)
        if out_angle != 'Rad':
            H = np.degrees(H)
        return H

def findTOF(thetai, thetaf, e, a, p = None, mu = 398600.441, in_angle = 'Rad'):
    '''
    Time of flight (TOF) between two true anomalies (thetai, thetaf). 
    
    Args:
        thetai (float): initial true anomaly [rad]

        thetaf (float): final true anomaly [rad]

        e (float): eccentricity [-]

        a (float): semi-major axis [km]

        p (float): semilatus rectum [km] (add for parabolic case)

        mu (float): central body gravitational parameter [km^3/s^2]

    Outputs:
        TOF (float): time of flight [s]
    '''
    if in_angle != 'Rad':
        thetai = np.radians(thetai)
        thetaf = np.radians(thetaf)
    Ei = theta2EBH(thetai, e)
    Ef = theta2EBH(thetaf, e)
    if e < 1.0:
        n = np.sqrt(mu/a**3)
        Mi = Ei - e*np.sin(Ei)
        Mf = Ef - e*np.sin(Ef)
        if Mi > Mf:
            Mi = Mi - 2*np.pi
    if e == 1.0:
        n = 2*np.sqrt(mu/p**3)
        Mi = Ei + (Ei**3)/3
        Mf = Ef + (Ef**3)/3
    if e > 1.0:
        n = np.sqrt(mu/abs(a)**3)
        Mi = e*np.sinh(Ei) - Ei
        Mf = e*np.sinh(Ef) - Ef

    TOF = abs(Mf-Mi)/n
    return TOF

if __name__ == "__main__":
    print('-----------------------------------------------------------------------------------------------------------------------------------------------')
    print('This is an example to verify the functionality of the script.')
    print('Given theta initial = 260 deg, theta final = 64.97 deg, a = 26564.5 km and e = 0.7411, time of flight (TOF) should be 3000 s.')
    print('Given theta initial = 315 deg, theta final = 90 deg, a = Inf, p = 63756 km and e = 1, time of flight (TOF) should be 22581.8 s.')
    print('Given theta initial = -128.69, theta final = -19.78 deg, a = -8861.9 km, mu = 42828 km^3/s^2 and e = 1.3837, time of flight (TOF) should be 20886 s.')
    print('Let\'s see,')
    TOF_e = findTOF(4.5379, 1.1339, 0.7411, 26564.5)
    TOF_b = findTOF(5.4978, 1.5708, 1, 'Inf', 63756)
    TOF_h = findTOF(4.0371, 5.9379, 1.3837, -8861.9, mu = 42828)
    print('>>> time of flight (TOF): ', TOF_e)
    print('>>> time of flight (TOF): ', TOF_b)
    print('>>> time of flight (TOF): ', TOF_h)
    print('-----------------------------------------------------------------------------------------------------------------------------------------------')