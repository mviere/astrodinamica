"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

# Datos de las órbitas
r_terrestre = 6371
radio_GPS = 20200 + 6371
radio_LEO = 550 + 6371
mu = 398600.441
w_LEO = np.sqrt(mu/radio_LEO**3)
w_GPS = np.sqrt(mu/radio_GPS**3)

# Ángulo de visión
'''
En la sección 3.3.4. del siguiente link,
https://e-archivo.uc3m.es/bitstream/handle/10016/26329/TFG_Roberto_Perez_Perez_2014.pdf;jsessionid=EF70D744FB3733EFBDE440223EA51EEA?sequence=1
se presenta el ángulo de visión gamma entre una estación terrena y el satélite.
Para que la estación pueda ver al satélite la elevación debe ser positiva.
De allí que, gamma <= arccos(r_terrestre/r_sat). El ángulo de visión total es 2*gamma.
Este ángulo gamma es el ángulo entre el radio a la estación terrena y el radio al satélite.
Si de esta estación terrena, tomo el ángulo gamma_LEO al satélite en la órbita LEO y el ángulo gamma_GPS a alguno de los satélites GPS,
el ángulo de visión final es la suma de los dos. Sobre cuál satélite GPS se calcula este ángulo, es igual, porque la ecuación involucra únicamente a los radios.
'''

# Ángulo de visión del satélite en LEO desde la Tierra:
gamma_LEO = 2*np.arccos(r_terrestre/radio_LEO)
# Ángulo de visión del satélite GPS desde la Tierra:
gamma_GPS = 2*np.arccos(r_terrestre/radio_GPS)
# Ángulo de visión del satélite GPS desde la órbita LEO:
gamma = gamma_LEO + gamma_GPS
# Cantidad mínima de satélites que ve LEO:
n_min = np.floor(np.degrees(gamma)/60)
n_max = np.ceil(np.degrees(gamma)/60)
print('Mínima cantidad de satélites: ', n_min)
print('Máxima cantidad de satélites: ', n_max)

'''
Como en ambas órbitas los satélites se mueven a distintas velocidades angulares.
Es decir, están en movimiento relativo. Requiero pasar a un sistema en donde alguno de ellos esté detenido.
Para ello, obtengo la velocidad angular de LEO respecto a GPS -> w_LEO_GPW.

El ángulo de visión de LEO gira con él, y es aproximadamente 198.
Si el ángulo fuera igual a 180 deg, vería siempre tres satélites.
Entonces, la diferencia entre ver tres o cuatro está dada por el ángulo auxiliar Aux1 = 198-180
y el ángulo axuliar Aux2 = 60 - Aux1.

Planteo la siguiente situación:
(1) El ángulo de visión ve 4 satélites
Recorre una trayectoria radio_GPS*Aux1 -> El tiempo que demora en recorrer esa trayectoria es t1 = Aux1/w_LEO_GPS
(2) El ángulo de visión ve 3 satélites
Recorre una trayectoria radio_GPS*Aux2 -> El tiempo que demora en recorrer esa trayectoria es t2 = Aux2/w_LEO_GPS
(3) El ángulo de visión ve 4 satélites

'''
# Para analizar tiempos considero que el satélite GPS está quieto respecto al satélite en órbita LEO (w_LEO > w_GPS).
# Velocidad angular de LEO con respecto a GPS:
w_LEO_GPS = w_LEO - w_GPS
# Defino ángulos auxiliares
Aux1 = gamma - np.pi
Aux2 = np.pi/3 - Aux1
# Obtengo los tiempos
t1 = Aux1/w_LEO_GPS
t2 = Aux2/w_LEO_GPS
print('Tiempo de máxima cantidad de satélites:', t1)
print('Tiempo de mínima cantidad de satélites:', t2)