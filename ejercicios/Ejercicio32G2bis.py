"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np
def KepEqtnE(M, e, abstol = 1e-8, itlim = 1e8):
    """
    Kepler equation to convert mean anomaly and eccentricity to eccentric anomaly
    
    Args:
        M (float): mean anomaly [rad]
        
        e (float): eccentricity [-]

        abstol (float): maximum tolerance for E value [rad]
        
        itlim (int): maximum iterations
    
    Outputs:
        E (float): eccentric anomaly [rad]
        
        convergence (int): 1 if calculation converged within itlim else 0
    """
    E = M - e if np.sin(M) < 0 else M + e
    E_ = E + 2*abstol
    i = 0
    convergence = 1
    while abs(E-E_) > abstol and i < itlim:
        E_ = E; i += 1
        if i >= itlim:
            print('Calulation stopped, iteration limit reached')
            convergence = 0
            break
        E = E_ + (M - E_ + e*np.sin(E_)) / (1 - e*np.cos(E_))
    return E, convergence

def KepEqtnP(dt, p, mu = 3.986e14):
    """
    Kepler equation to convert time of flight since periapsis to parabolic anomaly
    
    Args:
        dt (float): time of flight since periapsis [s]
        
        p (float): semilatus rectum [m]

        mu (float): central body gravitational parameter [m^3/s^2]
    
    Outputs:
        B (float): parabolic anomaly [rad]
    """
    s = (np.arctan(1/(3*(np.sqrt(mu/(p**3)))*dt)))/2
    w = np.arctan(np.cbrt(np.tan(s)))
    B = 2*(1/np.tan(2*w))
    return B

def KepEqtnH(M, e, abstol = 1e-8, itlim = 1e8):
    """
    Kepler equation to convert mean anomaly and eccentricity to hyperbolic anomaly
    
    Args:
        M (float): mean anomaly [rad]
        
        e (float): eccentricity [-]

        abstol (float): maximum tolerance for H value [rad]
        
        itlim (int): maximum iterations
    
    Outputs:
        H (float): hyperbolic anomaly [rad]
        
        convergence (int): 1 if calculation converged within itlim else 0
    """
    if e < 1.6:
        if -np.pi<M<0 or M>np.pi:
            H = M - e
        else:
            H = M + e
    else:
        if e<3.6 and np.absolute(M)>np.pi:
            H = M - np.sign(M)*e
        else:
            H = M/(e-1)
    H_ = H + 2*abstol
    i = 0
    convergence = 1
    while abs(H-H_) > abstol and i < itlim:
        H_ = H; i += 1
        if i >= itlim:
            print('Calulation stopped, iteration limit reached')
            convergence = 0
            break
        H = H_ + (M + H_ - e*np.sinh(H_)) / (e*np.cosh(H_)-1)
    return H, convergence

def theta_2_E_B_H(theta, e):
    '''
    True anomaly to eccentric anomaly or parabolic anomaly or hyperbolic anomaly
    
    Args:
        theta (float): true anomaly [rad]
            - eliptical 0 < theta < 2pi
            - parabolic -pi < theta < pi
            - hyperbolic -pi + arcos(1/e) < theta < pi - arcos(1/e)

        e (float): eccentricity [-]

    Outputs:
        E (float): eccentric anomaly [rad] 0 < E < 2pi

        B (float): parabolic anomaly [rad] -pi< theta < pi

        H (float): hyperbolic anomaly [rad] 
    '''
    if e < 1.0:
        sin_E = (np.sin(theta)*np.sqrt(1-e**2))/(1+e*np.cos(theta))
        cos_E = (np.cos(theta)+e)/(1+e*np.cos(theta))
        E = np.arctan2(sin_E, cos_E)
        return E
    if e == 1:
        B = np.tan(theta/2)
        return B
    if e > 1.0:
        H = np.arcsinh((np.sin(theta)*np.sqrt(e**2-1))/(1+e*np.cos(theta))) 
        return H

def E_B_H_2_theta(e, E = None, B = None, H = None, p = None, r = None):
    '''
    Eccentric anomaly or parabolic anomaly or hyperbolic anomaly to true anomaly
    
    Args:
        e (float): eccentricity [-]

        E (float): eccentric anomaly [rad] 0 < theta < 360

        B (float): parabolic anomaly [rad] -180 < theta < 180

        H (float): hyperbolic anomaly [rad]

        p (float): semilatus rectum [m] (add for parabolic case)

        r (float): ratio [m] (add for parabolic case)

    Outputs:
        theta (float): true anomaly [deg]
            - eliptical 0 < theta < 360
            - parabolic -180 < theta < 180
            - hyperbolic -180 + arcos(1/e) < theta < 180 - arcos(1/e)
    '''
    if e < 1.0:
        sin_theta = (np.sin(E)*np.sqrt(1-e**2))/(1-e*np.cos(E))
        cos_theta = (np.cos(E)-e)/(1-e*np.cos(E))
    if e == 1:
        sin_theta = (p*B/r)
        cos_theta = ((p-r)/r)
    if e > 1.0:
        sin_theta = (-np.sinh(E)*np.sqrt(e**2-1))/(1-e*np.cosh(E))
        cos_theta = (np.cosh(E)-e)/(1-e*np.cosh(E))
    theta = np.arctan2(sin_theta, cos_theta)
    return theta

def ROT1(a, clockwise = True):
    if clockwise == True:
        ROT1 = np.array([[1, 0, 0], [0, np.cos(a), np.sin(a)], [0, -np.sin(a), np.cos(a)]])
    else:
        ROT1 = np.array([[1, 0, 0], [0, np.cos(a), -np.sin(a)], [0, np.sin(a), np.cos(a)]])
    return ROT1

def ROT2(a, clockwise = True):
    if clockwise == True:
        ROT2 = np.array([[np.cos(a), 0, -np.sin(a)], [0, 1, 0], [np.sin(a), 0, np.cos(a)]])
    else:
        ROT2 = np.array([[np.cos(a), 0, np.sin(a)], [0, 1, 0], [-np.sin(a), 0, np.cos(a)]])
    return ROT2

def ROT3(a, clockwise = True):
    if clockwise == True:
        ROT3 = np.array([[np.cos(a), np.sin(a), 0], [-np.sin(a), np.cos(a), 0], [0, 0, 1]])
    else:
        ROT3 = np.array([[np.cos(a), -np.sin(a), 0], [np.sin(a), np.cos(a), 0], [0, 0, 1]])
    return ROT3

def COE_2_RV(p, e, i, o = None, w = None, theta = None, u = None, lt = None, wt = None, mu = 3.986e14):
    if i == 0.0 and e == 0.0:
        o = 0.0
        w = 0.0
        theta = lt
    if i != 0.0 and e == 0.0:
        w = 0.0
        theta = u
    if i == 0.0 and e < 1.0:
        o = 0.0
        w = wt

    r_vector_PQW_i = (p*np.cos(theta))/(1+e*np.cos(theta))
    r_vector_PQW_j = (p*np.sin(theta))/(1+e*np.cos(theta))
    r_vector_PQW = np.array([[r_vector_PQW_i], [r_vector_PQW_j], [0]])

    v_vector_PQW_i = -np.sqrt(mu/p)*np.sin(theta)
    v_vector_PQW_j = np.sqrt(mu/p)*(e+np.cos(theta))
    v_vector_PQW = np.array([[v_vector_PQW_i], [v_vector_PQW_j], [0]])
    MAT1 = ROT3(o, False)
    MAT2 = ROT1(i, False)
    MAT3 = ROT3(w, False)
    MAT1MAT2 = np.dot(MAT1, MAT2)
    T = np.dot(MAT1MAT2,MAT3)
    r_vector_ECI = np.dot(T, r_vector_PQW)
    v_vector_ECI = np.dot(T, v_vector_PQW)

    return r_vector_ECI, v_vector_ECI

def RV_2_COE(r_vector, v_vector, mu = 3.986e14, deg = False):
    '''
    State vectors to orbital elements.
    
    Args:
        r_vector (array of floats [1x3]): vector position [m]

        v_vector (array of floats [1x3]): velocity vector [m]

        mu (float): central body gravitational parameter [m^3/s^2]

    Outputs:
        p

        a

        e

        i

        o

        w

        theta

        u

        lt

        wt
    '''
    r = np.linalg.norm(r_vector)
    v = np.linalg.norm(v_vector)

    h_vector = np.cross(r_vector, v_vector)
    h = np.linalg.norm(h_vector)
    
    n_vector = np.cross([0,0,1], h_vector)
    n = np.linalg.norm(n_vector)
    
    e_vector =(np.multiply((v**2-mu/r),r_vector)-np.multiply(np.dot(r_vector, v_vector),v_vector))/mu
    e = np.linalg.norm(e_vector)
    
    EN = v**2/2 - mu/r

    if e != 1.0:
        a = -mu/(2*EN)
        p = a*(1-e**2)
    else:
        p = h**2/mu
        a = 'Inf'
    
    i = np.arccos(h_vector[2]/h)
    o = np.arccos(n_vector[0]/n)
    
    if n_vector[1] < 0:
        o = 2*np.pi - o
    
    w = np.arccos(np.dot(n_vector, e_vector)/(n*e))
    if e_vector[2] < 0:
        w = 2*np.pi - w
    
    theta = np.arccos(np.dot(e_vector, r_vector)/(e*r))
    if np.dot(r_vector, v_vector) < 0:
        theta = 2*np.pi - theta
    
    if i == 0 and e < 1.0:
        wt = np.arccos(e_vector[0]/e)
        if e_vector[1] < 0:
            wt = 2*np.pi - wt
    else:
        wt = None
    
    if i != 0 and e == 0:
        u = np.arccos(np.dot(n_vector, r_vector)/(n*r))
        if r_vector[2] < 0:
            u = 2*np.pi - u
    else:
        u = None
    
    if i == 0 and e == 0:
        lt = np.arccos(r_vector[0]/r)
        if r_vector[1] < 0:
            lt = 2*np.pi - lt
    else:
        lt = None
    
    if deg:
        i = i*180/np.pi
        o = o*180/np.pi
        w = w*180/np.pi
        theta = theta*180/np.pi
        if u != None:    
            u = u*180/np.pi
        if lt != None:
            lt = lt*180/np.pi
        if wt != None:
            wt = wt*180/np.pi

    return p, a, e, i, o, w, theta, u, lt, wt

def propagate(r_0, v_0, dt, mu_ = 3.986e14):

    p_, a_, e_, i_, o_, w_, theta_, u, lt, wt = RV_2_COE(r_0, v_0, mu = mu_)
    E1 = theta_2_E_B_H(theta_, e_)
    n = np.sqrt(mu_/a_**3)
    
    if e_ < 1.0:                                               
        M1 = E1 - e_*np.sin(E1)
        M2 = M1 + n*dt
        E2, convergence = KepEqtnE(M2, e_)
        theta_ = E_B_H_2_theta(E2, e_)
    
    if e_ == 1.0:
        r_ = np.linalg.norm(r_0)                                           
        M1 = E1 + E1**3/3
        E2 = KepEqtnP(dt, p_, mu_)
        theta_ = E_B_H_2_theta(e_, B = E2, p = p_, r = r_)

    if e_ > 1.0:                                               
        M1 = e_*np.sinh(E1) - E1
        M2 = M1 + n*dt
        E2, convergence = KepEqtnH(M2, e_)
        theta_ = E_B_H_2_theta(e_, H = E2)
        
    r_vector, v_vector = COE_2_RV(p_, e_, i_, o_, w_, theta_, mu = mu_)

    return r_vector, v_vector

def simular(r_0, v_0, t, mu_ = 3.986e14, coe = False):

    X = np.zeros([6,t])
    X[:,0] = np.concatenate([r_0, v_0])
    p, a, e, i, o, w, theta, u, lt, wt = RV_2_COE(r_0, v_0, mu_)

    for c in range(1, t):
        r, v =  propagate(r_0, v_0, c)
        p, a, e, i, o, w, theta, u, lt, wt = RV_2_COE(r, v, mu_)
        X[:,c] = COE_2_RV(p, e, i, o, w, theta, mu = mu_)
    if coe:
        o = o*180/np.pi
        i = i*180/np.pi
        w = w*180/np.pi
        return a, e, o, i, w
    else:
        return X

if __name__ == "__main__":
    print('-------------------------------------------------------------')
    print('This is an example to verify the functionality of the script.')
    print('Given r_vector = [-6*(10**6),6*(10**6),6*(10**6)] m')
    print('      v_vector = [-3000,-4000,3000] m/s')
    print('Orbital elements should be,')
    print('      a =  m')
    print('      e = ')
    print('      o = ')
    print('      i = ')
    print('      w = ')
    print('      theta =')
    X = simular([-6000000,6000000,6000000],[-3000,-4000,3000], 86400)
    print('Let\'s see,')
    print(X)
    '''print('      a =', a_)
    print('      e = ', e_)
    print('      o = ', o_)
    print('      i = ', i_)
    print('      w = ', w_)
    print('      theta =', theta1)
    p_, a_, e_, i_, o_, w_, theta1, u_, lt_, wt_= RV_2_COE([-6*(10**6),6*(10**6),6*(10**6)],[-3000,-4000,3000])
    theta2 = theta1_TOF_to_theta2_eliptical(theta1, 86400, e_, a_, abstol = 0.1)
    print(theta2)
    r_vector2, v_vector2 = COE_2_RV(p = p_, e = e_, i = i_, o = o_, w = w_, theta = theta2, u = u_, lt = lt_, wt = wt_)
    print(r_vector2)
    print(v_vector2)'''
    print('-------------------------------------------------------------')