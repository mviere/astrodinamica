"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def KepEqtnE(M, e, abstol = 1e-8, itlim = 1e8, in_angle = 'Rad', out_angle = 'Rad'):
    '''
    Kepler equation to convert mean anomaly (M) and eccentricity (e) to eccentric anomaly (E).
    Algorithm 2 Vallado.
    
    Args:
        M (float): mean anomaly [rad]
        
        e (float): eccentricity [-]

        abstol (float): maximum tolerance for E value [rad]
        
        itlim (int): maximum iterations

        in_angle (string): angle unit of M and abstol, 'Rad' or 'Deg'

        out_angle (string): angle unit of E, 'Rad' or 'Deg'
    
    Outputs:
        E (float): eccentric anomaly [deg]
        
        convergence (int): 1 if calculation converged within itlim else 0
    '''
    if in_angle != 'Rad':
        M = np.radians(M)
        if abstol != 1e-8:
            abstol = np.radians(abstol)
    E = M - e if np.sin(M) < 0 else M + e
    E_ = E + 2*abstol
    i = 0
    convergence = 1
    while abs(E-E_) > abstol and i < itlim:
        E_ = E; i += 1
        if i >= itlim:
            print('Calulation stopped, iteration limit reached')
            convergence = 0
            break
        E = E_ + (M - E_ + e*np.sin(E_)) / (1 - e*np.cos(E_))
    if out_angle != 'Rad':
        E = np.degrees(E)
    return E, convergence

def KepEqtnP(dt, p, mu = 398600.441, out_angle = 'Rad'):
    '''
    Kepler equation to convert time of flight since periapsis (dt) to parabolic anomaly (B).
    Algorithm 3 Vallado.
    
    Args:
        dt (float): time of flight since periapsis [s]
        
        p (float): semilatus rectum [km]

        mu (float): central body gravitational parameter [km^3/s^2]

        out_angle (string): angle unit of B, 'Rad' or 'Deg'
    
    Outputs:
        B (float): parabolic anomaly [rad]
    '''
    s = (np.arctan(1/(3*(np.sqrt(mu/(p**3)))*dt)))/2
    w = np.arctan(np.cbrt(np.tan(s)))
    B = 2*(1/np.tan(2*w))
    if out_angle != 'Rad':
        B = np.degrees(B)
    return B

def KepEqtnH(M, e, abstol = 1e-8, itlim = 1e8, in_angle = 'Rad', out_angle = 'Rad'):
    '''
    Kepler equation to convert mean anomaly (M) and eccentricity (e) to hyperbolic anomaly (H).
    Algorithm 4 Vallado.
    
    Args:
        M (float): mean anomaly [rad] 
        
        e (float): eccentricity [-]

        abstol (float): maximum tolerance for H value [rad]
        
        itlim (int): maximum iterations

        in_angle (string): angle unit of M and abstol, 'Rad' or 'Deg'

        out_angle (string): angle unit of H, 'Rad' or 'Deg'
    
    Outputs:
        H (float): hyperbolic anomaly [rad]
        
        convergence (int): 1 if calculation converged within itlim else 0
    '''
    if in_angle != 'Rad':
        M = np.radians(M)
        if abstol != 1e-8:
            abstol = np.radians(abstol)
    if abstol != 1e-8:
        abstol = abstol*np.pi/180
    if e < 1.6:
        if -np.pi<M<0 or M>np.pi:
            H = M - e
        else:
            H = M + e
    else:
        if e<3.6 and np.absolute(M)>np.pi:
            H = M - np.sign(M)*e
        else:
            H = M/(e-1)
    H_ = H + 2*abstol
    i = 0
    convergence = 1
    while abs(H-H_) > abstol and i < itlim:
        H_ = H; i += 1
        if i >= itlim:
            print('Calulation stopped, iteration limit reached')
            convergence = 0
            break
        H = H_ + (M + H_ - e*np.sinh(H_)) / (e*np.cosh(H_)-1)
    if out_angle != 'Rad':
        H = np.degrees(H)
    return H, convergence

def ROT1(a, positive = True): # por default crece positivamente
    if positive:
        ROT1 = np.array([[1, 0, 0], [0, np.cos(a), np.sin(a)], [0, -np.sin(a), np.cos(a)]])
    else:
        ROT1 = np.array([[1, 0, 0], [0, np.cos(a), -np.sin(a)], [0, np.sin(a), np.cos(a)]])
    return ROT1

def ROT2(a, positive = True): # por default crece positivamente
    if positive:
        ROT2 = np.array([[np.cos(a), 0, -np.sin(a)], [0, 1, 0], [np.sin(a), 0, np.cos(a)]])
    else:
        ROT2 = np.array([[np.cos(a), 0, np.sin(a)], [0, 1, 0], [-np.sin(a), 0, np.cos(a)]])
    return ROT2

def ROT3(a, positive = True): # por default crece positivamente
    if positive:
        ROT3 = np.array([[np.cos(a), np.sin(a), 0], [-np.sin(a), np.cos(a), 0], [0, 0, 1]])
    else:
        ROT3 = np.array([[np.cos(a), -np.sin(a), 0], [np.sin(a), np.cos(a), 0], [0, 0, 1]])
    return ROT3

def RV2COE(r_ECI, v_ECI, mu = 398600.441, out_angle = 'Deg'):
    '''
    Position vector (r_ECI) and velocity vector (v_ECI), both in ECI,
    to orbital elements (a, e, i, raan, w, theta, u, lt, wt) and semilatus rectum (p).
    Algorithm 9 Vallado.

    Args:
        r_ECI (vector): position vector [km]

        v_ECI (vector): velocity vector [km/s]

        mu (float): central body gravitational parameter [km^3/s^2]

        out_angle (string): angle unit of i, raan, w, theta, u, lt and wt, 'Rad' or 'Deg'

    Outputs:
        p (float): semilatus rectum [km]

        a (float): semi-major axis [m] or 'Inf' for parabolas

        e (float): eccentricity [-]

        i (float): inclination [deg]

        raan (float): right ascension of ascending node [deg]

        w (float): argument of perigee [deg]

        theta (float): true anomaly [deg]

        u (float): argument of latitud [deg]

        lt (float): true longitud [deg]

        wt (float): true longitud of periapsis [deg]
    '''
    r = np.linalg.norm(r_ECI)
    v = np.linalg.norm(v_ECI)

    h_vector = np.cross(r_ECI, v_ECI)
    h = np.linalg.norm(h_vector)
    
    n_vector = np.cross([0,0,1], h_vector)
    n = np.linalg.norm(n_vector)
    
    e_vector =(np.multiply((v**2-mu/r),r_ECI)-np.multiply(np.dot(r_ECI, v_ECI),v_ECI))/mu
    e = np.linalg.norm(e_vector)
    
    EN = v**2/2 - mu/r

    if e != 1.0:
        a = -mu/(2*EN)
        p = a*(1-e**2)
    else:
        p = h**2/mu
        a = 'Inf'
    
    i = np.arccos(h_vector[2]/h)
    raan = np.arccos(n_vector[0]/n)
    
    if n_vector[1] < 0:
        raan = 2*np.pi - raan
    
    w = np.arccos(np.dot(n_vector, e_vector)/(n*e))
    if e_vector[2] < 0:
        w = 2*np.pi - w
    
    theta = np.arccos(np.dot(e_vector, r_ECI)/(e*r))
    if np.dot(r_ECI, v_ECI) < 0:
        theta = 2*np.pi - theta
    
    if i == 0 and e < 1.0:
        wt = np.arccos(e_vector[0]/e)
        if e_vector[1] < 0:
            wt = 2*np.pi - wt
    else:
        wt = None
    
    if i != 0 and e == 0:
        u = np.arccos(np.dot(n_vector, r_ECI)/(n*r))
        if r_ECI[2] < 0:
            u = 2*np.pi - u
    else:
        u = None
    
    if i == 0 and e == 0:
        lt = np.arccos(r_ECI[0]/r)
        if r_ECI[1] < 0:
            lt = 2*np.pi - lt
    else:
        lt = None
    
    if out_angle == 'Deg':
        i = np.degrees(i)
        raan = np.degrees(raan)
        w = np.degrees(w)
        theta = np.degrees(theta)
        if u != None:    
            u = np.degrees(u)
        if lt != None:
            lt = np.degrees(lt)
        if wt != None:
            wt = np.degrees(wt)

    return p, a, e, i, raan, w, theta, u, lt, wt

def COE2RV(p, e, i, raan = None, w = None, theta = None, u = None, lt = None, wt = None, mu = 398600.441, in_angle = 'Deg'):
    '''
    Some orbital elements (e, i, raan, w, theta, u, lt, wt) and semilatus rectum (p)
    to position vector (r_ECI) and velocity vector (v_ECI), both in ECI.
    Algorithm 10 Vallado.
    
    Args:
        p (float): semilatus rectum [km]

        e (float): eccentricity [-]

        i (float): inclination [deg]

        raan (float): right ascension of ascending node [deg]

        w (float): argument of perigee [deg]

        theta (float): true anomaly [deg]

        u (float): argument of latitud [deg]

        lt (float): true longitud [deg]

        wt (float): true longitud of periapsis [deg]

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of i, raan, w, theta, u, lt and wt, 'Rad' or 'Deg'

    Outputs:
        r_ECI (vector): position vector [km]

        v_ECI (vector): velocity vector [km/s]
    '''
    if in_angle == 'Deg':
        i = np.radians(i)
        if raan != None:
            raan = np.radians(raan)
        if w != None:
            w = np.radians(w)
        if theta != None:
            theta = np.radians(theta)
        if u != None:
            u = np.radians(u)
        if lt != None:
            lt = np.radians(lt)
        if wt != None:
            wt = np.radians(wt)
    if i == 0.0 and e == 0.0:
        raan = 0.0
        w = 0.0
        theta = lt
    if i != 0.0 and e == 0.0:
        w = 0.0
        theta = u
    if i == 0.0 and e < 1:
        raan = 0.0
        w = wt

    r_PQW_i = (p*np.cos(theta))/(1+e*np.cos(theta))
    r_PQW_j = (p*np.sin(theta))/(1+e*np.cos(theta))
    r_PQW = np.array([r_PQW_i, r_PQW_j, 0])

    v_PQW_i = -np.sqrt(mu/p)*np.sin(theta)
    v_PQW_j = np.sqrt(mu/p)*(e+np.cos(theta))
    v_PQW = np.array([v_PQW_i, v_PQW_j, 0])
    
    PQW2ECI = np.dot(np.dot(ROT3(raan, False), ROT1(i, False)),ROT3(w, False))
    r_ECI = np.dot(PQW2ECI, r_PQW)
    v_ECI = np.dot(PQW2ECI, v_PQW)

    return r_ECI, v_ECI

def theta2EBH(theta, e, in_angle = 'Rad', out_angle = 'Rad'):
    '''
    True anomaly (theta) to eccentric anomaly (E) or parabolic anomaly (B) or hyperbolic anomaly (H).
    Algorithm 5 Vallado.
    
    Args:
        theta (float): true anomaly [rad]

        e (float): eccentricity [-]

        in_angle (string): angle unit of theta, 'Rad' or 'Deg'

        out_angle (string): angle unit of E, B or H, 'Rad' or 'Deg'

    Outputs:
        E (float): eccentric anomaly [rad]

        B (float): parabolic anomaly [rad]

        H (float): hyperbolic anomaly [rad]
    '''
    if in_angle != 'Rad':
        theta = np.radians(theta)
    if e < 1.0:
        sin_E = (np.sin(theta)*np.sqrt(1-e**2))/(1+e*np.cos(theta))
        cos_E = (np.cos(theta)+e)/(1+e*np.cos(theta))
        E = np.arctan2(sin_E, cos_E)
        if E < 0:
            E += 2*np.pi
        if out_angle != 'Rad':
            E = np.degrees(E)
        return E
    if e == 1:
        B = np.tan(theta/2)
        if out_angle != 'Rad':
            B = np.degrees(B)
        return B
    if e > 1.0:
        sinh_H = (np.sin(theta)*np.sqrt(e**2-1))/(1+e*np.cos(theta))    
        cosh_H = (e+np.cos(theta))/(1+e*np.cos(theta))
        H = np.arctanh(sinh_H/cosh_H)
        if out_angle != 'Rad':
            H = np.degrees(H)
        return H

def EBH2theta(e, E = None, B = None, H = None, p = None, r = None, in_angle = 'Rad', out_angle = 'Rad'):
    '''
    Eccentric anomaly or parabolic anomaly or hyperbolic anomaly to true anomaly.
    Algorithm 6 Vallado.
    
    Args:
        e (float): eccentricity [-]

        E (float): eccentric anomaly [rad]

        B (float): parabolic anomaly [rad]

        H (float): hyperbolic anomaly [rad]

        p (float): semilatus rectum [km] (add for parabolic case)

        r (float): ratio [km] (add for parabolic case)

        in_angle (string): angle unit of E, B or H, 'Rad' or 'Deg'

        out_angle (string): angle unit of theta, 'Rad' or 'Deg'

    Outputs:
        theta (float): true anomaly [rad]
    '''
    if e < 1.0:
        if in_angle != 'Rad':
            E = np.radians(E)
        sin_theta = (np.sin(E)*np.sqrt(1-e**2))/(1-e*np.cos(E))
        cos_theta = (np.cos(E)-e)/(1-e*np.cos(E))
        theta = np.arctan2(sin_theta, cos_theta)
        if theta < 0:
            theta += 2*np.pi
    if e == 1:
        if in_angle != 'Rad':
            B = np.radians(B)
        sin_theta = (p*B/r)
        cos_theta = ((p-r)/r)
        theta = np.arctan2(sin_theta, cos_theta)
    if e > 1.0:
        if in_angle != 'Rad':
            H = np.radians(H)
        sin_theta = (-np.sinh(H)*np.sqrt(e**2-1))/(1-e*np.cosh(H))
        cos_theta = (np.cosh(H)-e)/(1-e*np.cosh(H))
        theta = np.arctan2(sin_theta, cos_theta)
    if out_angle != 'Rad':
        theta = np.degrees(theta)
    return theta

def propagate(r_ECI_0, v_ECI_0, dt, mu = 398600.441):
    '''
    This function propagates from initial position and velocity vectors to final position and velocity vectors in a time lapse.
    
    Args:
        r_ECI_0 (vector): initial position vector [km]

        v_ECI_0 (vector): initial velocity vector [km/s]

        dt (int): time lapse [s]

        mu (float): central body gravitational parameter [km^3/s^2]
    Outputs:
        r_ECI (vector): final position vector [km]

        v_ECI (vector): final velocity vector [km/s]
    '''
    p, a, e, i, raan, w, theta, u, lt, wt = RV2COE(r_ECI_0, v_ECI_0, mu, out_angle = 'Rad')
    
    if e < 1.0:
        n = np.sqrt(mu/a**3)
        Ei = theta2EBH(theta, e)                                               
        Mi = Ei - e*np.sin(Ei) # (Eq. 2-7 del Vallado)
        Mf = Mi + n*dt # (Eq. 2-7 del Vallado)
        Ef, convergence = KepEqtnE(Mf, e)
        theta = EBH2theta(e, Ef)
    
    if e == 1.0:
        n = np.sqrt(mu/p**3)
        Bi = theta2EBH(theta, e)
        r = np.linalg.norm(r_ECI_0)                                           
        Mi = Bi + Bi**3/3 # (Eq. 2-23 del Vallado)
        Bf = KepEqtnP(dt, p, mu)
        theta = EBH2theta(e, 0, Bf, 0, p, r)

    if e > 1.0:
        n = np.sqrt(mu/a**3)
        Hi = theta2EBH(theta, e)                                             
        Mi = e*np.sinh(Hi) - Hi # (Eq. 2-39 del Vallado)
        Mf = Mi + n*dt # (Eq. 2-39 del Vallado)
        Hf, convergence = KepEqtnH(Mf, e)
        theta = EBH2theta(e, 0, 0, Hf)

    r_ECI, v_ECI = COE2RV(p, e, i, raan, w, theta, u, lt, wt, mu, in_angle = 'Rad')

    return r_ECI, v_ECI

# Datos del problema
r_ECI_0 = np.array([100, 8000, 0])
v_ECI_0 = np.array([-6, 0, 3])

# Obtengo elementos orbitales
p, a, e, i, raan, w, theta, u, lt, wt = RV2COE(r_ECI_0, v_ECI_0)
print('Semi-eje mayor:', a)
print('Excentricidad:', e)
print('RAAN:', raan)
print('Inclinacion:', i)
print('Argumento perigeo:', w)
print('Anomalia verdadera:', theta)

# Propago en la órbita hasta 30 minutos después
dt = 30*60
r_ECI, v_ECI = propagate(r_ECI_0, v_ECI_0, dt)
p, a, e, i, raan, w, theta, u, lt, wt = RV2COE(r_ECI, v_ECI)
print('Anomalia verdadera final:', theta)
print('Vector posición:')
print(r_ECI)
print('Vector velocidad:')
print(v_ECI)