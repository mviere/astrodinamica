"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def rad2deg(a):

    """
    Conversion from radians to degrees
    
    Args:
        a (float): angle [rad]
    
    Outputs:
        a (float): angle [deg]
        
    """

    a = a*180/np.pi
    return a

def deg2rad(a):

    """
    Conversion from degrees to radians
    
    Args:
        a (float): angle [deg]
    
    Outputs:
        a (float): angle [rad]
        
    """
    
    a = a*np.pi/180
    return a

def ECEF2LLH(r, out_angle = 'Rad', R = 6378.1363, e = 0.081819221456, abstol=1e-8, itlim=1e8):
    
    r_delsat = np.sqrt((r[0]**2) + (r[1]**2))

    sin_alf = r[1]/r_delsat
    cos_alf = r[0]/r_delsat

    lamb = np.arctan2(sin_alf, cos_alf)
    phi_gd = np.arctan(r[2]/r_delsat)
    r_delta = r_delsat

    phi_gd_ = phi_gd + 2*abstol
    i = 0
    convergence = 1
    
    while abs(phi_gd-phi_gd_) > abstol and i < itlim:
        phi_gd_ = phi_gd
        i += 1
        if i >= itlim:
            print('Calulation stopped, iteration limit reached')
            convergence = 0
            break
        C = R/(np.sqrt(1 - (e**2)*(np.sin(phi_gd_)**2)))
        phi_gd = np.arctan((r[2] + C*(e**2)*np.sin(phi_gd_))/r_delta)

    if np.abs(phi_gd) < 1.55334:                                        # Fuera de los polos
        h_ellp = r_delta/np.cos(phi_gd) - C
    else:                                                               # En los polos
        S = R*(1 - e**2)/np.sqrt(1 - (e*np.sin(phi_gd))**2)
        h_ellp = r[2]/np.sin(phi_gd) - S

    if out_angle == 'Deg':
        lamb = rad2deg(lamb)
        phi_gd = rad2deg(phi_gd)

    return phi_gd, lamb, h_ellp, convergence

def LLH2ECEF(phi_gd, lamb, h_ellp, in_angle = 'Rad', R = 6378.1363, e = 0.081819221456):
    
    if in_angle == 'Deg':
        lamb = deg2rad(lamb)
        phi_gd = deg2rad(phi_gd)
    

    if np.abs(phi_gd) < 1.55334:
        C = R/(np.sqrt(1 - (e**2)*(np.sin(phi_gd)**2)))
        r_delsat= (h_ellp + C)*np.cos(phi_gd)
        rK = np.tan(phi_gd)*r_delsat - C*(e**2)*np.sin(phi_gd)

    else:
        S = R*(1 - e**2)/np.sqrt(1 - (e*np.sin(phi_gd))**2)
        rK = (h_ellp+S)*np.sin(phi_gd)
        r_delsat = (rK + C*(e**2)*np.sin(phi_gd))/np.tan(phi_gd)

    alf = lamb
    rJ = np.sin(alf)*r_delsat
    rI = np.cos(alf)*r_delsat

    r = [rI, rJ, rK]

    return r
