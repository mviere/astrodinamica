"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def KepEqtnE(M, e, abstol = 1e-8, itlim = 1e8, in_angle = 'Rad', out_angle = 'Rad'):
    '''
    Kepler equation to convert mean anomaly (M) and eccentricity (e) to eccentric anomaly (E).
    
    Args:
        M (float): mean anomaly [rad]
        
        e (float): eccentricity [-]

        abstol (float): maximum tolerance for E value [rad]
        
        itlim (int): maximum iterations

        in_angle (string): angle unit of M and abstol, 'Rad' or 'Deg'

        out_angle (string): angle unit of E, 'Rad' or 'Deg'
    
    Outputs:
        E (float): eccentric anomaly [deg]
        
        convergence (int): 1 if calculation converged within itlim else 0
    '''
    if in_angle != 'Rad':
        M = np.radians(M)
        if abstol != 1e-8:
            abstol = np.radians(abstol)
    E = M - e if np.sin(M) < 0 else M + e
    E_ = E + 2*abstol
    i = 0
    convergence = 1
    while abs(E-E_) > abstol and i < itlim:
        E_ = E; i += 1
        if i >= itlim:
            print('Calulation stopped, iteration limit reached')
            convergence = 0
            break
        E = E_ + (M - E_ + e*np.sin(E_)) / (1 - e*np.cos(E_))
    if out_angle != 'Rad':
        E = np.degrees(E)
    return E, convergence

def KepEqtnP(dt, p, mu = 398600.441, out_angle = 'Rad'):
    '''
    Kepler equation to convert time of flight since periapsis (dt) to parabolic anomaly (B).
    
    Args:
        dt (float): time of flight since periapsis [s]
        
        p (float): semilatus rectum [km]

        mu (float): central body gravitational parameter [km^3/s^2]

        out_angle (string): angle unit of B, 'Rad' or 'Deg'
    
    Outputs:
        B (float): parabolic anomaly [rad]
    '''
    s = (np.arctan(1/(3*(np.sqrt(mu/(p**3)))*dt)))/2
    w = np.arctan(np.cbrt(np.tan(s)))
    B = 2*(1/np.tan(2*w))
    if out_angle != 'Rad':
        B = np.degrees(B)
    return B

def KepEqtnH(M, e, abstol = 1e-8, itlim = 1e8, in_angle = 'Rad', out_angle = 'Rad'):
    '''
    Kepler equation to convert mean anomaly (M) and eccentricity (e) to hyperbolic anomaly (H).
    
    Args:
        M (float): mean anomaly [rad] 
        
        e (float): eccentricity [-]

        abstol (float): maximum tolerance for H value [rad]
        
        itlim (int): maximum iterations

        in_angle (string): angle unit of M and abstol, 'Rad' or 'Deg'

        out_angle (string): angle unit of H, 'Rad' or 'Deg'
    
    Outputs:
        H (float): hyperbolic anomaly [rad]
        
        convergence (int): 1 if calculation converged within itlim else 0
    '''
    if in_angle != 'Rad':
        M = np.radians(M)
        if abstol != 1e-8:
            abstol = np.radians(abstol)
    if abstol != 1e-8:
        abstol = abstol*np.pi/180
    if e < 1.6:
        if -np.pi<M<0 or M>np.pi:
            H = M - e
        else:
            H = M + e
    else:
        if e<3.6 and np.absolute(M)>np.pi:
            H = M - np.sign(M)*e
        else:
            H = M/(e-1)
    H_ = H + 2*abstol
    i = 0
    convergence = 1
    while abs(H-H_) > abstol and i < itlim:
        H_ = H; i += 1
        if i >= itlim:
            print('Calulation stopped, iteration limit reached')
            convergence = 0
            break
        H = H_ + (M + H_ - e*np.sinh(H_)) / (e*np.cosh(H_)-1)
    if out_angle != 'Rad':
        H = np.degrees(H)
    return H, convergence

def rperiapsis_rapogeo_to_T_e_a_eliptical(r_periapsis, r_apoapsis, mu = 3.986e14):
    """
    Periapsis and apoapsis ratios to period  only for eliptical case

    Args:
        r_periapsis (float): periapsis ratio [m]

        r_apoapsis (float): apoapsis ratio [m]

        mu (float): central body gravitational parameter [m^3/s^2]
    
    Outputs:
        T (float): period [s]

        a (float): semi-major axis [m]

        e (float): eccentricity [-]
    """
    a = (r_periapsis + r_apoapsis)/2
    e = (r_apoapsis - r_periapsis)/(r_apoapsis + r_periapsis)
    T = 2*np.pi*np.sqrt(a**3/mu)
    return T, a, e

def theta2EBH(theta, e, in_angle = 'Rad', out_angle = 'Rad'):
    '''
    True anomaly (theta) to eccentric anomaly (E) or parabolic anomaly (B) or hyperbolic anomaly (H).
    
    Args:
        theta (float): true anomaly [rad]

        e (float): eccentricity [-]

        in_angle (string): angle unit of theta, 'Rad' or 'Deg'

        out_angle (string): angle unit of E, B or H, 'Rad' or 'Deg'

    Outputs:
        E (float): eccentric anomaly [rad]

        B (float): parabolic anomaly [rad]

        H (float): hyperbolic anomaly [rad]
    '''
    if in_angle != 'Rad':
        theta = np.radians(theta)
    if e < 1.0:
        sin_E = (np.sin(theta)*np.sqrt(1-e**2))/(1+e*np.cos(theta))
        cos_E = (np.cos(theta)+e)/(1+e*np.cos(theta))
        E = np.arctan2(sin_E, cos_E)
        if E < 0:
            E += 2*np.pi # nooooos ee
        if out_angle != 'Rad':
            E = np.degrees(E)
        return E
    if e == 1:
        B = np.tan(theta/2)
        if out_angle != 'Rad':
            B = np.degrees(B)
        return B
    if e > 1.0:
        sinh_H = (np.sin(theta)*np.sqrt(e**2-1))/(1+e*np.cos(theta))    
        cosh_H = (e+np.cos(theta))/(1+e*np.cos(theta))
        H = np.arctanh(sinh_H/cosh_H)
        if out_angle != 'Rad':
            H = np.degrees(H)
        return H

def EBH2theta(e, E = None, B = None, H = None, p = None, r = None, in_angle = 'Rad', out_angle = 'Rad'):
    '''
    Eccentric anomaly or parabolic anomaly or hyperbolic anomaly to true anomaly
    
    Args:
        e (float): eccentricity [-]

        E (float): eccentric anomaly [rad]

        B (float): parabolic anomaly [rad]

        H (float): hyperbolic anomaly [rad]

        p (float): semilatus rectum [km] (add for parabolic case)

        r (float): ratio [km] (add for parabolic case)

        in_angle (string): angle unit of E, B or H, 'Rad' or 'Deg'

        out_angle (string): angle unit of theta, 'Rad' or 'Deg'

    Outputs:
        theta (float): true anomaly [rad]
    '''
    if e < 1.0:
        if in_angle != 'Rad':
            E = np.radians(E)
        sin_theta = (np.sin(E)*np.sqrt(1-e**2))/(1-e*np.cos(E))
        cos_theta = (np.cos(E)-e)/(1-e*np.cos(E))
        theta = np.arctan2(sin_theta, cos_theta)
        if theta < 0:
            theta += 2*np.pi # no seeee
    if e == 1:
        if in_angle != 'Rad':
            B = np.radians(B)
        sin_theta = (p*B/r)
        cos_theta = ((p-r)/r)
        theta = np.arctan2(sin_theta, cos_theta)
    if e > 1.0:
        if in_angle != 'Rad':
            H = np.radians(H)
        sin_theta = (-np.sinh(H)*np.sqrt(e**2-1))/(1-e*np.cosh(H))
        cos_theta = (np.cosh(H)-e)/(1-e*np.cosh(H))
        theta = np.arctan2(sin_theta, cos_theta)
    if out_angle != 'Rad':
        theta = np.degrees(theta)
    return theta

def M2theta(M, e, p = None, r = None, abstol = 1e-8, itlim = 1e8, in_angle = 'Rad', out_angle = 'Rad'):
    '''
    Mean anomaly (M) to true anomaly (theta).
    
    Args:
        M (float): mean anomaly [rad]

        e (float): eccentricity [-]

        p (float): semilatus rectum [km] (add for parabolic case)

        r (float): ratio [km] (add for parabolic case)

        abstol (float): maximum tolerance for E or H value [rad]
        
        itlim (int): maximum iterations

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of M, 'Rad' or 'Deg'

        out_angle (string): angle unit of theta, 'Rad' or 'Deg'

    Outputs:
        theta (float): true anomaly [rad]
    '''
    if in_angle != 'Rad':
        M = np.radians(M)
    if e < 1.0:
        E, convergence = KepEqtnE(M, e, abstol, itlim)
        theta = EBH2theta(e, E)
        if theta < 0:
           theta += 2*np.pi
    if e == 1.0:
        s = (np.pi/2 - np.arctan(3*M/2))/2
        w = np.arctan(np.cbrt(np.tan(s)))
        B_ = 2*(1/np.tan(2*w))
        p_ = p
        r_ = r
        theta = EBH2theta(e, B = B_, p = p_, r = r_)
    if e > 1.0:
        H_, convergence = KepEqtnH(M, e, abstol, itlim)
        theta = EBH2theta(e, H = H_)
    if out_angle != 'Rad':
        theta = np.degrees(theta)
    return theta

def theta2M(theta, e, in_angle = 'Rad', out_angle = 'Rad'):
    '''
    True anomaly (theta) to mean anomaly (M).
    
    Args:
        theta (float): true anomaly [rad]

        e (float): eccentricity [-]

        in_angle (string): angle unit of theta, 'Rad' or 'Deg'

        out_angle (string): angle unit of M, 'Rad' or 'Deg'

    Outputs:
        M (float): mean anomaly [rad]
    '''
    if in_angle != 'Rad':
        theta = np.radians(theta)
    if e < 1.0:
        sin_E = (np.sin(theta)*np.sqrt(1-e**2))/(1+e*np.cos(theta))
        cos_E = (np.cos(theta)+e)/(1+e*np.cos(theta))
        E = np.arctan2(sin_E, cos_E)
        if E < 0:
            E += 2*np.pi
        M = E - e*np.sin(E)
    if e == 1:
        B = np.tan(theta/2)
        M = B + (B**3)/3
    if e > 1.0:
        H = np.arcsinh((np.sin(theta)*np.sqrt(e**2-1))/(1+e*np.cos(theta)))
        M = e*np.sinh(H) - H
    if out_angle != 'Rad':
        M = np.degrees(M)
    return M

def thetaiTOF2thetaf(thetai, TOF, e, a, p = None, r = None, mu = 398600.441, abstol = 1e-8, itlim = 1e8, in_angle = 'Rad', out_angle = 'Rad'):
    '''
    Initial true anomaly (thetai) and time of flight (TOF) to final true anomaly (thetaf).
    
    Args:
        thetai (float): initial true anomaly [rad]

        TOF (float): time of flight [s]

        e (float): eccentricity [-]

        a (float): semi-major axis [m] or 'Inf' for parabolas

        mu (float): central body gravitational parameter [m^3/s^2]

    Outputs:
        thetaf (float): final true anomaly [rad]
    '''
    if in_angle != 'Rad':
        thetai = np.radians(thetai)
    Mi = theta2M(thetai, e)
    if e != 1:
        Mf = np.sqrt(mu/a**3)*TOF + Mi
        abstol_ = abstol
        itlim_ = itlim
        thetaf = M2theta(Mf, e, abstol = abstol_, itlim = itlim_)
    if e == 1:
        Mf = 2*np.sqrt(mu/p**3)*TOF + Mi
        p_ = p
        r_ = r
        thetaf = M2theta(Mf, e, p = p_, r = r_)
    if out_angle != 'Rad':
        thetaf = np.degrees(thetaf)
    return thetaf

def findTOF(thetai, thetaf, e, a, p = None, mu = 398600.441, in_angle = 'Rad'):
    '''
    Time of flight (TOF) between two true anomalies (thetai, thetaf). 
    
    Args:
        thetai (float): initial true anomaly [rad]

        thetaf (float): final true anomaly [rad]

        e (float): eccentricity [-]

        a (float): semi-major axis [km]

        p (float): semilatus rectum [km] (add for parabolic case)

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): angle unit of thetai and thetaf, 'Rad' or 'Deg'

    Outputs:
        TOF (float): time of flight [s]
    '''
    if in_angle != 'Rad':
        thetai = np.radians(thetai)
        thetaf = np.radians(thetaf)
    Ei = theta2EBH(thetai, e)
    Ef = theta2EBH(thetaf, e)
    if e < 1.0:
        n = np.sqrt(mu/a**3)
        Mi = Ei - e*np.sin(Ei)
        Mf = Ef - e*np.sin(Ef)
    if e == 1.0:
        n = 2*np.sqrt(mu/p**3)
        Mi = Ei + (Ei**3)/3
        Mf = Ef + (Ef**3)/3
    if e > 1.0:
        n = np.sqrt(mu/a**3)
        Mi = e*np.sinh(Ei) - Ei
        Mf = e*np.sinh(Ef) - Ef
    TOF = abs(Mf-Mi)/n
    return TOF

if __name__ == "__main__":
    r_periapsis = 300000 + 6378000
    r_apoapsis = 5000000 + 6378000
    T, a, e = rperiapsis_rapogeo_to_T_e_a_eliptical(r_periapsis, r_apoapsis)
    print('-------------------------------------------------------------------')
    print('Period of orbit:',T)
    theta1 = np.pi/2
    theta2 = np.pi
    TOF = findTOF(theta1, theta2, e, a)
    print('TOF from semi lactus rectum to apoapsis:',TOF)
    theta1 = np.pi
    TOF = 80*60
    tolerance = 0.1*np.pi/180
    theta2 = thetaiTOF2thetaf(theta1, TOF, e, a, abstol = tolerance)
    print('True anomaly after 80 minutes of apoapsis:',theta2)
    print('-------------------------------------------------------------------')