"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def Hohmann(radioi, radiof, mu = 398600.441):
    '''
    Devuelve el delta de velocidad (delta_v) total y el tiempo de vuelo (t_trans) en una transferencia de Hohmann
    entre dos orbitas circulares con radio inicial (radioi) y radio final (radiof).
    Algorithm 36 Vallado.

    Args:
        radioi (float): initial radius [km]

        radiof (float): final radius [km]

        mu (float): central body gravitational parameter [km^3/s^2]

    Outputs:
        delta_v (float): total change in velocity [km/s]

        t_trans (float): time on trasfer orbit [s]
    '''
    a_trans = (radioi+radiof)/2
    
    vi = np.sqrt(mu/radioi)
    v_transa = np.sqrt((2*mu/radioi)-mu/a_trans)
    
    vf = np.sqrt(mu/radiof)
    v_transb = np.sqrt((2*mu/radiof)-mu/a_trans)

    delta_va = v_transa-vi
    delta_vb = vf-v_transb
    delta_v = abs(delta_va)+abs(delta_vb)
    
    t_trans = np.pi*np.sqrt((a_trans**3)/mu)
    
    return delta_v, t_trans

def CCPhasingDifOrbit(a_tgt, a_int, thetai, k = 0, mu = 398600.441, in_angle = 'Rad', out_angle = 'Deg'):
    '''
    Circular Coplanar Phasing on Different Orbits
    Devuelve el tiempo en la orbita de transferencia (t_trans), el delta de velocidad (delta_v),
    el semi-eje mayor de la órbita de transferencia (a_trans), el tiempo de espera hasta el inicio de transferencia (t_wait)
    y el periodo en el cuál se repite la geometría.
    Algorithm 45 Vallado + modificaciones:
        theta = w_tgt*t_trans-np.pi
        t_wait = ((thetai-theta)+2*np.pi*k)/(w_int-w_tgt)

    Args:
        a_tgt (float): semimayor axis of target orbit [km]

        a_int (float): semimayor axis of interceptor orbit [km]

        thetai (float): initial phase angle [rad] (positive if interceptor leads target)

        k (int): revolutions in which the geometry is repeated [-]

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): unit angle of thetai, 'Rad' or 'Deg'

        out_angle (string): unit angle of theta, 'Rad' or 'Deg'

    Outputs:
        delta_v (float): total change in velocity [km/s]

        t_trans (float): time on transfer orbit [s]

        a_trans (float): semimayor axis of transfer orbit [km]

        t_wait (float): time until the interceptor and target are in the correct positions [s]

        synodic (float): synodic period [s]

        theta (float): angle between target and interceptor [deg]
    '''
    if in_angle != 'Rad':
        thetai = np.radians(thetai)

    w_tgt = np.sqrt(mu/a_tgt**3)
    w_int = np.sqrt(mu/a_int**3)
    a_trans = (a_int+a_tgt)/2
    t_trans = np.pi*np.sqrt(a_trans**3/mu)
    theta = np.pi - w_tgt*t_trans
    t_wait = ((thetai-theta)+2*np.pi*k)/(w_int-w_tgt)
    synodic = 2*np.pi/(w_int-w_tgt)
    delta_va = abs(np.sqrt(2*mu/a_int-mu/a_trans)-np.sqrt(mu/a_int))
    delta_vb = abs(np.sqrt(2*mu/a_tgt-mu/a_trans)-np.sqrt(mu/a_tgt))
    delta_v = delta_va + delta_vb
    if out_angle == 'Deg':
        theta = np.degrees(theta)
    return delta_v, t_trans, a_trans, t_wait, synodic, theta, delta_va, delta_vb

def CCPhasingSameOrbit(a_tgt, theta, k_tgt = 1, k_int = 1, mu = 398600.441, in_angle = 'Rad'):
    '''
    Circular Coplanar Phasing on Same Orbit
    Devuelve el tiempo en la orbita de fase (t_phase), el delta de velocidad (delta_v)
    y el semi-eje mayor de la órbita de fase (a_phase).
    Algorithm 44 Vallado.

    Args:
        a_tgt (float): semimayor axis of target orbit [km]

        theta (float): phase angle [rad] (positive if interceptor leads target)

        k_tgt (int): revolutions on target orbit [-]

        k_int (int): revolutions on phase orbit [-]

        mu (float): central body gravitational parameter [km^3/s^2]

        in_angle (string): unit angle of theta, 'Rad' or 'Deg'

    Outputs:
        delta_v (float): total change in velocity [km/s]

        t_phase (float): time on phasing orbit [s]

        a_phase (float): semimayor axis of phase orbit [km]
    '''
    if in_angle != 'Rad':
        theta = np.radians(theta)

    w_tgt = np.sqrt(mu/a_tgt**3)
    t_phase = (2*np.pi*k_tgt+theta)/w_tgt
    a_phase = (mu*((t_phase/(2*np.pi*k_int))**2))**(1/3)
    if a_phase < a_tgt:
        r_peri = 2*a_phase - a_tgt
        if r_peri <= 6378.1363:
            print('¡Choca con la Tierra!')
    delta_v = 2*abs(np.sqrt((2*mu/a_tgt)-(mu/a_phase))-np.sqrt(mu/a_tgt))

    return delta_v, t_phase, a_phase

# Datos del problema
'''
Sigo la convención de signos según la figura 6-17 del Vallado -> target adelanta a interceptor, theta > 0.
Además, incluyo las siguientes modificaciones al algoritmo 45 del Vallado:
theta = w_tgt*t_trans-np.pi
t_wait = ((thetai-theta)+2*np.pi*k)/(w_int-w_tgt)
'''
thetai = 60
a_int = 300 + 6378.1363 # Órbita casi circular, a = r
a_tgt = 700 + 6378.1363 # Órbita casi circular, a = r

# Realizo un phasing entre dos órbitas casi circulares de distinto radio
# Obtengo los delta de velocidad, el ángulo de fase y los tiempos de transferencia, espera y sinódico.
delta_v, t_trans, a_trans, t_wait, synodic, theta, delta_va, delta_vb = CCPhasingDifOrbit(a_tgt, a_int, thetai, in_angle = 'Deg', out_angle = 'Deg')
print('Tiempo de transferencia:', t_trans)
print('Ángulo de fase:', theta)
print('Tiempo de espera:', t_wait)
print('Delta_v primer impulso:', delta_va)
print('Delta_v segundo impulso:', delta_vb)
print('Tiempo sinódico:', synodic)

# Ahora realizo un phasing en la misma órbita casi circular.
# Obtengo el período de la órbita, el semi-eje mayor y el delta_v total de la maniobra.
'''
En el ejemplo de uso del Algorithm 44 (Example 6-8 Vallado) establece un theta inicial negativo si interceptor adelanta a target.
Sin embargo, en la corrección de la Fe de Erratas versión número 4, establece un theta inicial negativo si target adelanta a interceptor.
Es por ello que, para que funcione correctamente el algoritmo de Vallado tomé la convención de signos de su Fe de Erratas.
-> target adelanta a interceptor, theta < 0 
'''
theta = -2
delta_v, t_phase, a_phase = CCPhasingSameOrbit(a_tgt, theta, in_angle = 'Deg')
T = 2*np.pi*np.sqrt(a_phase**3/398600.441) # Período de órbitas circulares y elípticas.
print('Período:', T)
print('Eje semi-mayor:', a_phase)
print('Delta_v total:', delta_v)