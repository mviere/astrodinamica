"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np
from sgp4.earth_gravity import wgs84
from sgp4.io import twoline2rv
from matplotlib import pyplot as plt
from datetime import datetime
from sgp4.conveniences import sat_epoch_datetime

def RV2COE(r_ECI, v_ECI, mu = 398600.441, out_angle = 'Deg'):
    '''
    Position vector (r_ECI) and velocity vector (v_ECI), both in ECI,
    to orbital elements (a, e, i, raan, w, theta, u, lt, wt) and semilatus rectum (p).

    Args:
        r_ECI (vector): position vector [km]

        v_ECI (vector): velocity vector [km/s]

        mu (float): central body gravitational parameter [km^3/s^2]

        out_angle (string): angle unit of i, raan, w, theta, u, lt and wt, 'Rad' or 'Deg'

    Outputs:
        p (float): semilatus rectum [km]

        a (float): semi-major axis [m] or 'Inf' for parabolas

        e (float): eccentricity [-]

        i (float): inclination [deg]

        raan (float): right ascension of ascending node [deg]

        w (float): argument of perigee [deg]

        theta (float): true anomaly [deg]

        u (float): argument of latitud [deg]

        lt (float): true longitud [deg]

        wt (float): true longitud of periapsis [deg]
    '''
    r = np.linalg.norm(r_ECI)
    v = np.linalg.norm(v_ECI)

    h_vector = np.cross(r_ECI, v_ECI)
    h = np.linalg.norm(h_vector)
    
    n_vector = np.cross([0,0,1], h_vector)
    n = np.linalg.norm(n_vector)
    
    e_vector =(np.multiply((v**2-mu/r),r_ECI)-np.multiply(np.dot(r_ECI, v_ECI),v_ECI))/mu
    e = np.linalg.norm(e_vector)
    
    EN = v**2/2 - mu/r

    if e != 1.0:
        a = -mu/(2*EN)
        p = a*(1-e**2)
    else:
        p = h**2/mu
        a = 'Inf'
    
    i = np.arccos(h_vector[2]/h)
    raan = np.arccos(n_vector[0]/n)
    
    if n_vector[1] < 0:
        raan = 2*np.pi - raan
    
    w = np.arccos(np.dot(n_vector, e_vector)/(n*e))
    if e_vector[2] < 0:
        w = 2*np.pi - w
    
    theta = np.arccos(np.dot(e_vector, r_ECI)/(e*r))
    if np.dot(r_ECI, v_ECI) < 0:
        theta = 2*np.pi - theta
    
    if i == 0 and e < 1.0:
        wt = np.arccos(e_vector[0]/e)
        if e_vector[1] < 0:
            wt = 2*np.pi - wt
    else:
        wt = None
    
    if i != 0 and e == 0:
        u = np.arccos(np.dot(n_vector, r_ECI)/(n*r))
        if r_ECI[2] < 0:
            u = 2*np.pi - u
    else:
        u = None
    
    if i == 0 and e == 0:
        lt = np.arccos(r_ECI[0]/r)
        if r_ECI[1] < 0:
            lt = 2*np.pi - lt
    else:
        lt = None
    
    if out_angle == 'Deg':
        i = np.degrees(i)
        raan = np.degrees(raan)
        w = np.degrees(w)
        theta = np.degrees(theta)
        if u != None:    
            u = np.degrees(u)
        if lt != None:
            lt = np.degrees(lt)
        if wt != None:
            wt = np.degrees(wt)

    return p, a, e, i, raan, w, theta, u, lt, wt

def simulate(TLE_1, TLE_2, finaldate, t_step, coe = False):

    sat = twoline2rv(TLE_1, TLE_2, wgs84)
    yr = sat.epochyr
    initial_date0 = sat_epoch_datetime(sat)
    initial_date = initial_date0.replace(year=yr)
    final_date = datetime(*finaldate)

    totsec = (final_date - initial_date).total_seconds() 

    N = int(totsec/t_step)                                     
    X = np.zeros([6,N])                       
    
    for k in range(N):
        r, v = sat.propagate(initial_date.year, initial_date.month, initial_date.day, initial_date.hour, initial_date.minute, initial_date.second + k*t_step)
        X[:,k] = np.concatenate([r, v])
    
    if coe:
        fig, f = plt.subplots(2, 3, figsize = (20, 8))
        plt.subplots_adjust(wspace = 0.6, hspace = 0.6)
        for filas in range(2):
            for col in range(3):
                f[filas, col].set_xlim(0, totsec)
                f[filas, col].set_xlabel("Tiempo [s]")
                f[filas, col].xaxis.labelpad = 8
                f[filas, col].yaxis.labelpad = 8
                f[filas, col].grid(color = 'gray', linewidth = 0.2)
        f[0, 0].set_title("Eje semi-mayor vs tiempo", pad = 16)
        f[0, 0].set_ylabel("Eje semi-mayor [km]")
        f[0, 1].set_title("Excentricidad vs tiempo", pad = 16)
        f[0, 1].set_ylabel("Excentricidad [-]")
        f[0, 2].set_title("Inclinación vs tiempo", pad = 16)
        f[0, 2].set_ylabel("Inclinación [deg]")
        f[1, 0].set_title("RAAN vs tiempo", pad = 8)
        f[1, 0].set_ylabel("RAAN [deg]")
        f[1, 1].set_title("Argumento de periapsis vs tiempo", pad = 8)
        f[1, 1].set_ylabel("Argumento de periapsis [deg]")
        f[1, 2].set_title("Anomalía verdadera vs tiempo", pad = 8)
        f[1, 2].set_ylabel("Anomalía verdadera [deg]")
        
        for n in range(N):
            t = n*t_step
            r, v = sat.propagate(initial_date.year, initial_date.month, initial_date.day, initial_date.hour, initial_date.minute, initial_date.second + t)
            p, a, e, i, raan, w, theta, u, lt, wt = RV2COE(r, v)
            
            f[0, 0].plot(t, a, marker = "o", markersize = 0.5, markeredgecolor = "orange", markerfacecolor = "orange")
            f[0, 1].plot(t, e, marker = "o", markersize = 0.5, markeredgecolor = "red", markerfacecolor = "red")
            f[0, 2].plot(t, i, marker = "o", markersize = 0.5, markeredgecolor = "blue", markerfacecolor = "blue")
            f[1, 0].plot(t, raan, marker = "o", markersize = 0.5, markeredgecolor = "violet", markerfacecolor = "violet")
            f[1, 1].plot(t, w, marker = "o", markersize = 0.5, markeredgecolor = "pink", markerfacecolor = "pink")
            f[1, 2].plot(t, theta, marker = "o", markersize = 0.5, markeredgecolor = "green", markerfacecolor = "green")
    return X

def simulateSAOCOM1B(TLE_1, TLE_2, n_orbits, t_step, coe = False):

    # obtengo fecha inicial sobre la cual luego iterar
    sat = twoline2rv(TLE_1, TLE_2, wgs84)
    yr = sat.epochyr
    initial_date0 = sat_epoch_datetime(sat)
    initial_date = initial_date0.replace(year=yr)
    T = 97.2*60 # período SAOCOM https://earth.esa.int/eogateway/missions/saocom
    totsec = n_orbits*T
    N = int(totsec/t_step)                                     
    X = np.zeros([6,N])                       
    
    for k in range(N):
        r, v = sat.propagate(initial_date.year, initial_date.month, initial_date.day, initial_date.hour, initial_date.minute, initial_date.second + k*t_step)
        X[:,k] = np.concatenate([r, v])
    
    if coe:
        fig, f = plt.subplots(2, 3, figsize = (20, 8))
        plt.subplots_adjust(wspace = 0.6, hspace = 0.6)
        for filas in range(2):
            for col in range(3):
                f[filas, col].set_xlim(0, totsec)
                f[filas, col].set_xlabel("Tiempo [s]")
                f[filas, col].xaxis.labelpad = 8
                f[filas, col].yaxis.labelpad = 8
                f[filas, col].grid(color = 'gray', linewidth = 0.2)
        f[0, 0].set_title("Eje semi-mayor vs tiempo", pad = 16)
        f[0, 0].set_ylabel("Eje semi-mayor [km]")
        f[0, 1].set_title("Excentricidad vs tiempo", pad = 16)
        f[0, 1].set_ylabel("Excentricidad [-]")
        f[0, 2].set_title("Inclinación vs tiempo", pad = 16)
        f[0, 2].set_ylabel("Inclinación [deg]")
        f[1, 0].set_title("RAAN vs tiempo", pad = 8)
        f[1, 0].set_ylabel("RAAN [deg]")
        f[1, 1].set_title("Argumento de periapsis vs tiempo", pad = 8)
        f[1, 1].set_ylabel("Argumento de periapsis [deg]")
        f[1, 2].set_title("Anomalía verdadera vs tiempo", pad = 8)
        f[1, 2].set_ylabel("Anomalía verdadera [deg]")
        
        for n in range(N):
            t = n*t_step
            r, v = sat.propagate(initial_date.year, initial_date.month, initial_date.day, initial_date.hour, initial_date.minute, initial_date.second + t)
            p, a, e, i, raan, w, theta, u, lt, wt = RV2COE(r, v)
            
            f[0, 0].plot(t, a, marker = "o", markersize = 0.5, markeredgecolor = "orange", markerfacecolor = "orange")
            f[0, 1].plot(t, e, marker = "o", markersize = 0.5, markeredgecolor = "red", markerfacecolor = "red")
            f[0, 2].plot(t, i, marker = "o", markersize = 0.5, markeredgecolor = "blue", markerfacecolor = "blue")
            f[1, 0].plot(t, raan, marker = "o", markersize = 0.5, markeredgecolor = "violet", markerfacecolor = "violet")
            f[1, 1].plot(t, w, marker = "o", markersize = 0.5, markeredgecolor = "pink", markerfacecolor = "pink")
            f[1, 2].plot(t, theta, marker = "o", markersize = 0.5, markeredgecolor = "green", markerfacecolor = "green")
        
        plt.show()
    
    return X

TLE_1 = ('1 46265U 20059A   22099.85067252  .00000486  00000+0  67612-4 0  9999')
TLE_2 = ('2 46265  97.8895 286.5994 0001483  82.9636 277.1745 14.82163469 86960')
X = simulateSAOCOM1B(TLE_1, TLE_2, 5, 60, True)