"""
\authors María Eugenia Viere

\brief Resolución de ejercicios

\version 0

\date 02/05/2022
"""
import numpy as np

def ROT1(a, clockwise = True):
    if clockwise == True:
        ROT1 = np.array([[1, 0, 0], [0, np.cos(a), np.sin(a)], [0, -np.sin(a), np.cos(a)]])
    else:
        ROT1 = np.array([[1, 0, 0], [0, np.cos(a), -np.sin(a)], [0, np.sin(a), np.cos(a)]])
    return ROT1

def ROT2(a, clockwise = True):
    if clockwise == True:
        ROT2 = np.array([[np.cos(a), 0, -np.sin(a)], [0, 1, 0], [np.sin(a), 0, np.cos(a)]])
    else:
        ROT2 = np.array([[np.cos(a), 0, np.sin(a)], [0, 1, 0], [-np.sin(a), 0, np.cos(a)]])
    return ROT2

def ROT3(a, clockwise = True):
    if clockwise == True:
        ROT3 = np.array([[np.cos(a), np.sin(a), 0], [-np.sin(a), np.cos(a), 0], [0, 0, 1]])
    else:
        ROT3 = np.array([[np.cos(a), -np.sin(a), 0], [np.sin(a), np.cos(a), 0], [0, 0, 1]])
    return ROT3

def COE_2_RV(p, e, i, o = None, w = None, theta = None, u = None, lt = None, wt = None, mu = 3.986e14):
    if i == 0.0 and e == 0.0:
        o = 0.0
        w = 0.0
        theta = lt
    if i != 0.0 and e == 0.0:
        w = 0.0
        theta = u
    if i == 0.0 and e < 1:
        o = 0.0
        w = wt
    i = i*np.pi/180
    if o != None:
        o = o*np.pi/180
    if w != None:
        w = w*np.pi/180
    if theta != None:
        theta = theta*np.pi/180
    if u != None:
        u = u*np.pi/180
    if lt != None:
        lt = lt*np.pi/180
    if wt != None:
        wt = wt*np.pi/180
    r_vector_PQW_i = (p*np.cos(theta))/(1+e*np.cos(theta))
    r_vector_PQW_j = (p*np.sin(theta))/(1+e*np.cos(theta))
    r_vector_PQW = np.array([[r_vector_PQW_i], [r_vector_PQW_j], [0]])

    v_vector_PQW_i = -np.sqrt(mu/p)*np.sin(theta)
    v_vector_PQW_j = np.sqrt(mu/p)*(e+np.cos(theta))
    v_vector_PQW = np.array([[v_vector_PQW_i], [v_vector_PQW_j], [0]])
    MAT1 = ROT3(o, False)
    MAT2 = ROT1(i, False)
    MAT3 = ROT3(w, False)
    MAT1MAT2 = np.dot(MAT1, MAT2)
    T = np.dot(MAT1MAT2,MAT3)
    r_vector_ECI = np.dot(T, r_vector_PQW)
    v_vector_ECI = np.dot(T, v_vector_PQW)

    return r_vector_ECI, v_vector_ECI

if __name__ == "__main__":
    print('-------------------------------------------------------------')
    print('This is an example to verify the functionality of the script.')
    print('Given the orbital elements')
    print('      p = 11067790 m')
    print('      e = 0.83285')
    print('      i = 87.87 deg')
    print('      o = 227.89 deg')
    print('      w = 53.38 deg')
    print('      theta = 92.335 deg')
    print('Position and velocity vectors should be,')
    print('      r_vector = [6525344, 6861535, 6449125] m')
    print('      v_vector = [4902.276, 5533.124, -1975.709] m/s')
    print('Let\'s see,')
    r_vector_ECI, v_vector_ECI = COE_2_RV(11067790, 0.83285, 87.87, o = 227.89, w = 53.38, theta = 92.335)
    print('      r_vector = ', r_vector_ECI)
    print('      v_vector = ', v_vector_ECI)
    print('-------------------------------------------------------------')